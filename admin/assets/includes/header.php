<?php require_once('route.php'); 

$page = basename($_SERVER['PHP_SELF'],".php").'.read';
//privileges check
if(!$current_user->can_see_this($page,$group)) {
	$msg = 'Restricted access! You are not authorized to view this section';
	redirect_to("index.php?edit=fail&msg={$msg}");
	exit();
}

if(isset($_SESSION[$elhash]) && $_SESSION[$elhash] != "") { 
	$random_hash = $_SESSION[$elhash];
} else {
	$random_hash = uniqid();
	$_SESSION[$elhash] = $random_hash;
}

?><!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>iChat | Live Support System</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
	<link rel="stylesheet" href="assets/js/cropper/cropper.css">
    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
	<?php if(file_exists(ADMINPANEL.'/install/index.php')) {
		echo "<div class='col-md-12 ' style='background-color:#b92b27;color:white;'><h4 style='font-weight:normal'><center>Warning! installation folder detected.. You must delete it before using the script</center></h4></div><br><br>";
	}?>