<?php
require_once("Loader.php");

spl_autoload_register(function ($class_name) {
	$path = LIBRARY.DS."{$class_name}.php";
	if (file_exists($path)) {
		require_once($path);
	} else {
		fatal_error("Class Error!","the class '{$class_name}' could not be found ! "); /* MUST CHANGE BEFORE UPLOAD! */
	}
});

function strip_zeros_from_date( $marked_string="" ) {
  $no_zeros = str_replace('*0', '', $marked_string);
  $cleaned_string = str_replace('*', '', $no_zeros);
  return $cleaned_string;
}

function strip_to_numbers_only($string) {
    $pattern = '/[^0-9]/';
    return preg_replace($pattern, '', $string);
}

function redirect_to( $location = NULL ) {
  if ($location != NULL) {
    header("Location: {$location}");
    exit;
  }
}

function redirect_to_opener( $location = NULL) {
  if ($location != NULL) {
	echo "<script type=\"text/javascript\">
		window.opener.location.href = \"{$location}\";
		self.close();
	</script>";
  }
}

function getRealIpAddr() {
    if (!empty($_SERVER['HTTP_CLIENT_IP'])) { //check ip from share internet
      $ip=$_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))  { //to check ip is pass from proxy
      $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
 	} else {
      $ip=$_SERVER['REMOTE_ADDR'];
    }
    return $ip;
}

function date_to_eng($datetime="") {
  $unixdatetime = strtotime($datetime);
  return strftime("%Y-%m-%d at %I:%M %p", $unixdatetime);
}

function now_db() {
  $unixdatetime = time();
  return strftime("%Y-%m-%d %H:%M:%S", $unixdatetime);
}

function date_db() {
  $unixdatetime = time();
  return strftime("%Y-%m-%d", $unixdatetime);
}

function now() {
  $unixdatetime = time();
  return strftime("%Y-%m-%d %I:%M %p", $unixdatetime);
}

function time_only($datetime="") {
  $unixdatetime = strtotime($datetime);
  return strftime("%I:%M %p", $unixdatetime);
}

function date_only($datetime="") {
	$unixdatetime = strtotime($datetime);
	return strftime("%Y-%m-%d", $unixdatetime);
}

function date_descriptive($datetime="") {
  $unixdatetime = strtotime($datetime);
  return strftime("%B %d, %Y", $unixdatetime);
}
function day_name($datetime="") {
  $unixdatetime = strtotime($datetime);
  return strftime("%A", $unixdatetime);
}

function SplitSQL($file, $delimiter = ';') {
    set_time_limit(0);

    if (is_file($file) === true) {
        $file = fopen($file, 'r');

        if (is_resource($file) === true) {
            $query = array();

            while (feof($file) === false) {
                $query[] = fgets($file);

                if (preg_match('~' . preg_quote($delimiter, '~') . '\s*$~iS', end($query)) === 1) {
                    $query = trim(implode('', $query));

                    if (mysql_query($query) === false) {
                        echo '<h3>ERROR: ' . $query . '</h3>' . "\n";
                    }

                    //else
                    //{
                       //echo '<h3>SUCCESS: ' . $query . '</h3>' . "\n";
                    //}

                    /*while (ob_get_level() > 0)
                    {
                        ob_end_flush();
                    }*/

                    flush();
                }

                if (is_string($query) === true) {
                    $query = array();
                }
            }

            return fclose($file);
        }
    }

    return false;
}


function updateSQL($file, $delimiter = ';') {
    set_time_limit(0);
	global $db;
    if (is_file($file) === true)
    {
        $file = fopen($file, 'r');

        if (is_resource($file) === true)
        {
            $query = array();

            while (feof($file) === false)
            {
                $query[] = fgets($file);

                if (preg_match('~' . preg_quote($delimiter, '~') . '\s*$~iS', end($query)) === 1)
                {
                    $query = trim(implode('', $query));

                    //if (mysql_query($query) === false)
                    //{
                       // echo '<h3>ERROR: ' . $query . '</h3>' . "\n";
                    //}

					try {
						if (mysqli_query($db->connection(), $query)) {
							throw new Exception($query ." : Column already exists!<br/>");
						}
					}
					catch (Exception $e) {
						//echo $e->getMessage();
					}

                    //else
                    //{
                       //echo '<h3>SUCCESS: ' . $query . '</h3>' . "\n";
                    //}

                    while (ob_get_level() > 0)
                    {
                        ob_end_flush();
                    }

                    flush();
                }

                if (is_string($query) === true)
                {
                    $query = array();
                }
            }

            return fclose($file);
        }
    }

    return false;
}


function size_as_text($size) {
	if ($size < 1024 ) {
		$size_bytes = $size . " Bytes";
		return $size_bytes;
	} elseif ($size < 1048576 ) {
		$size_kb = round($size / 1024) . " KBs";
		return $size_kb;
	} else {
		$size_mb = round($size / 1048576 , 1 ) . " MBs";
		return $size_mb;

	}
}


function calc_difference($newer_str, $older_str) {
	$older = new DateTime($older_str);
	$newer = new DateTime($newer_str);

  $Y1 = $older->format('Y');
  $Y2 = $newer->format('Y');
  $Y = $Y2 - $Y1;

  $m1 = $older->format('m');
  $m2 = $newer->format('m');
  $m = $m2 - $m1;

  $d1 = $older->format('d');
  $d2 = $newer->format('d');
  $d = $d2 - $d1;

  $H1 = $older->format('H');
  $H2 = $newer->format('H');
  $H = $H2 - $H1;

  $i1 = $older->format('i');
  $i2 = $newer->format('i');
  $i = $i2 - $i1;

  $s1 = $older->format('s');
  $s2 = $newer->format('s');
  $s = $s2 - $s1;

  if($s < 0) {
    $i = $i -1;
    $s = $s + 60;
  }
  if($i < 0) {
    $H = $H - 1;
    $i = $i + 60;
  }
  if($H < 0) {
    $d = $d - 1;
    $H = $H + 24;
  }
  if($d < 0) {
    $m = $m - 1;
    $d = $d + get_days_for_previous_month($m2, $Y2);
  }
  if($m < 0) {
    $Y = $Y - 1;
    $m = $m + 12;
  }
  $timespan_string = create_timespan_string($Y, $m, $d, $H, $i, $s);
  return $timespan_string;
}

function mjencode($str, $salt) {
	return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($salt), $str, MCRYPT_MODE_CBC, md5(md5($salt))));
}

function get_days_for_previous_month($current_month, $current_year) {
  $previous_month = $current_month - 1;
  if($current_month == 1) {
    $current_year = $current_year - 1; //going from January to previous December
    $previous_month = 12;
  }
  if($previous_month == 11 || $previous_month == 9 || $previous_month == 6 || $previous_month == 4) {
    return 30;
  }
  else if($previous_month == 2) {
    if(($current_year % 4) == 0) { //remainder 0 for leap years
      return 29;
    } else {
      return 28;
    }
  } else {
    return 31;
  }
}

function create_timespan_string($Y, $m, $d, $H, $i, $s) {
  $timespan_string = array();
  //$found_first_diff = false;
  $found_first_diff = true;
  if($Y >= 1) {
    $found_first_diff = true;
    $timespan_string['years']= $Y;
  } else {
	$timespan_string['years']= 0;
  }
  if($m >= 1 || $found_first_diff) {
    $found_first_diff = true;
    $timespan_string['months']= $m;
  }
  if($d >= 1 || $found_first_diff) {
    $found_first_diff = true;
    $timespan_string['days'] = $d;
  }
  if($H >= 1 || $found_first_diff) {
    $found_first_diff = true;
    $timespan_string['hours'] = $H;
  }
  if($i >= 1 || $found_first_diff) {
    $found_first_diff = true;
    $timespan_string['minuts'] = $i;
  }

  $timespan_string['seconds']= $s;
  return $timespan_string;
}

function secondsToTime($seconds) {
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');
	$return = '';
	if($dtF->diff($dtT)->format('%a') > 0 ) {
		$return .= $dtF->diff($dtT)->format('%a Days, ');
	}
	if($dtF->diff($dtT)->format('%h') > 0 ) {
		$return .= $dtF->diff($dtT)->format('%h Hours, ');
	}
	if($dtF->diff($dtT)->format('%i') > 0 ) {
		$return .= $dtF->diff($dtT)->format('%i Minutes And ');
	}

	if($dtF->diff($dtT)->format('%s') > 0 ) {
		$return .= $dtF->diff($dtT)->format('%s Seconds');
	}

	return $return;

}

function secondsToArray($seconds) {
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    $dtF->diff($dtT)->format('%a days, %h hours, %i minutes and %s seconds');

	$return = array();

	if($dtF->diff($dtT)->format('%a') > 0 ) {
		$return['days'] = $dtF->diff($dtT)->format('%a');
	}
	if($dtF->diff($dtT)->format('%h') > 0 ) {
		$return['hours'] = $dtF->diff($dtT)->format('%h');
	}
	if($dtF->diff($dtT)->format('%i') > 0 ) {
		$return['minutes'] = $dtF->diff($dtT)->format('%i');
	}

	if($dtF->diff($dtT)->format('%s') > 0 ) {
		$return['seconds'] = $dtF->diff($dtT)->format('%s');
	}

	return $return;

}

function secondsToSM($seconds) {
    $dtF = new \DateTime('@0');
    $dtT = new \DateTime("@$seconds");
    $dtF->diff($dtT)->format('%a days %h hours %i minutes %s seconds');
	$return = '';
	if($dtF->diff($dtT)->format('%a') > 0 ) {
		$return .= $dtF->diff($dtT)->format('%a Days');
	} elseif($dtF->diff($dtT)->format('%h') > 0 ) {
		$return .= $dtF->diff($dtT)->format('%h Hours');
	} elseif($dtF->diff($dtT)->format('%i') > 0 ) {
		$return .= $dtF->diff($dtT)->format('%i Minutes');
	} elseif($dtF->diff($dtT)->format('%s') > 0 ) {
		$return .= $dtF->diff($dtT)->format('%s Seconds');
	}

	return $return;

}

function calc_difference_minutes($date2,$date1) {

	$diff = abs(strtotime($date2) - strtotime($date1));
	$result = array();
	$result['minutes']  = floor($diff / 60);
	return $result;
}

function calc_difference_months($date2,$date1) {

	$diff = abs(strtotime($date2) - strtotime($date1));
	$result = array();
	$result['months']  = floor($diff / (30*60*60*24));
	$result['months']  = floor($diff / 86400 / 30 );
	return $result;
}

function calc_difference_days($newer_str,$older_str) {

	$older = new DateTime($older_str);
	$newer = new DateTime($newer_str);

	$Y1 = $older->format('Y');
	$Y2 = $newer->format('Y');

	$z1 = $older->format('z');
	$z2 = $newer->format('z');
	$z = $z2 - $z1;

	$mnth1= $older->format('m');
	$mnth2= $newer->format('m');

	if($mnth1 == "02" && $mnth2 > $mnth1) {
		$z+= 3;
	}

	if ($Y2 != $Y1) {
		$Y = $Y2 - $Y1;
		for ($i = 1 ; $i <= $Y ; $i++ ) {
			$z += 365;
		}
	}

	$result = array();

	//$result['days'] = $z + 1;
	$result['days'] = $z;
	return $result;
}

function get_random($length=0) {
    $characters = str_shuffle('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ');
    $string = '';
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters) -1)];
    }
    return $string;
}

function get_random_num($length=0) {
    $characters = str_shuffle('0123456789');
    $string = '';
    for ($p = 0; $p < $length; $p++) {
        $string .= $characters[mt_rand(0, strlen($characters) -1)];
    }
    return $string;
}


function strtotimefix($val,$timestamp=0) {
	if($timestamp == 0) { $timestamp = time(); }
	//$nval = $val * 31;
	//$strtotime = strtotime("{$nval} Days",$timestamp);

	$d = strftime("%d", $timestamp);
	$m = strftime("%m" , $timestamp);
	$y = strftime("%Y" , $timestamp);

	if ($m + $val >= 1 && $m + $val <= 12) {
		$new_m = $m + $val;
		$new_y = $y;
	} else {
		if($m + $val > 12) {
			$new_m = ($m + $val) - 12;
			$new_y = $y + 1;
		} elseif ($m + $val <= 1) {
			$new_m = ($m + $val) + 12;
			$new_y = $y - 1;

			if($new_m > 12) {
				$new_m -= 12;
				$new_y = $y;
			}

		} else {
			$new_m = $m;
			$new_y = $y;
		}
	}

		if($d > "28") {
			$new_d = "28";
		} else {
			$new_d = $d;
		}

		$final_str = $new_d . "-" . $new_m . "-" . $new_y;

		$strtotime = strtotime($final_str);

	return $strtotime;
}


function convert_to_k($value) {
	$ident = 'k'; $ident2 = 'm';
	if ($value > 999 && $value <= 999999) {
		$result = floor($value / 1000) . $ident;
	} elseif ($value > 999999) {
		$result = floor($value / 1000000) . $ident2;
	} else {
		$result = $value;
	}

	return $result;
}



function curPageURL() {
 $pageURL = 'http';
 if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
 $pageURL .= "://";
 if ($_SERVER["SERVER_PORT"] != "80") {
  $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
 } else {
  $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
 }
 return $pageURL;
}


/**
 * Get either a Gravatar URL or complete image tag for a specified email address.
 *
 * @param string $email The email address
 * @param string $s Size in pixels, defaults to 80px [ 1 - 2048 ]
 * @param string $d Default imageset to use [ 404 | mm | identicon | monsterid | wavatar ]
 * @param string $r Maximum rating (inclusive) [ g | pg | r | x ]
 * @param boole $img True to return a complete IMG tag False for just the URL
 * @param array $atts Optional, additional key/value attributes to include in the IMG tag
 * @return String containing either just a URL or a complete image tag
 * @source https://gravatar.com/site/implement/images/php/
 */
function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
$url = 'https://www.gravatar.com/avatar/';
$url .= md5( strtolower( trim( $email ) ) );
$url .= "?s=$s&d=$d&r=$r";
if( $img ) {
$url = '<img src="' . $url . '"';
foreach ( $atts as $key => $val )
$url .= ' ' . $key . '="' . $val . '"';
$url .= ' />';
}
return $url;
}

function slugify($text){
  $text = preg_replace('~[^\pL\d]+~u', '-', $text);
  $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
  $text = preg_replace('~[^-\w]+~', '', $text);
  $text = trim($text, '-');
  $text = preg_replace('~-+~', '-', $text);
  $text = strtolower($text);
  if (empty($text)) {
    return 'n-a';
  }
  return $text;
}

function date_ago($date) {
	$arr = calc_difference( strftime("%Y-%m-%d %H:%M:%S" , time()) , $date);
	$str = '';
	if($arr['years']) {
		$str .= $arr['years'] . " Year";
		if($arr['years'] > 1) { $str.= "s"; } $str .= ",";
	}
	if($arr['months']) {
		$str .= $arr['months'] . " Month";
		if($arr['months'] > 1) { $str.= "s"; } $str .= ",";
	}
	if($arr['days']) {
		$str .= $arr['days'] . " Day";
		if($arr['days'] > 1) { $str.= "s"; } $str .= ",";
	}
	if($arr['hours']) {
		$str .= $arr['hours'] . " Hour";
		if($arr['hours'] > 1) { $str.= "s"; }
	} elseif($arr['minuts']) {
		$str .= $arr['minuts'] . " Minute";
		if($arr['minuts'] > 1) { $str.= "s"; }
	}

	if($str) {
		$str = $str . " ago";
	} else {
		$str = 'Right Now';
	}

	return $str;
}


function deleteDir($dirPath) {
$dir = $dirPath;
	if(file_exists($dir)) {
		$it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
		$files = new RecursiveIteratorIterator($it,
					 RecursiveIteratorIterator::CHILD_FIRST);
		foreach($files as $file) {
			if ($file->isDir()){
				rmdir($file->getRealPath());
			} else {
				unlink($file->getRealPath());
			}
		}
		rmdir($dir);
	}
}

function file_upload_max_size() {
  static $max_size = -1;

  if ($max_size < 0) {
    // Start with post_max_size.
    $max_size = parse_size(ini_get('post_max_size'));

    // If upload_max_size is less, then reduce. Except if upload_max_size is
    // zero, which indicates no limit.
    $upload_max = parse_size(ini_get('upload_max_filesize'));
    if ($upload_max > 0 && $upload_max < $max_size) {
      $max_size = $upload_max;
    }
  }
  return $max_size;
}

function parse_size($size) {
  $unit = preg_replace('/[^bkmgtpezy]/i', '', $size); // Remove the non-unit characters from the size.
  $size = preg_replace('/[^0-9\.]/', '', $size); // Remove the non-numeric characters from the size.
  if ($unit) {
    // Find the position of the unit in the ordered string which is the power of magnitude to multiply a kilobyte by.
    return round($size * pow(1024, stripos('bkmgtpezy', $unit[0])));
  }
  else {
    return round($size);
  }
}



function ip_info($ip = NULL, $purpose = "location", $deep_detect = FALSE) {
    $output = NULL;
    if (filter_var($ip, FILTER_VALIDATE_IP) === FALSE) {
        $ip = $_SERVER["REMOTE_ADDR"];
        if ($deep_detect) {
            if (filter_var(@$_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            if (filter_var(@$_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP))
                $ip = $_SERVER['HTTP_CLIENT_IP'];
        }
    }

    $purpose    = str_replace(array("name", "\n", "\t", " ", "-", "_"), NULL, strtolower(trim($purpose)));
    $support    = array("country", "countrycode", "state", "region", "city", "location", "address","ip");
    $continents = array(
        "AF" => "Africa",
        "AN" => "Antarctica",
        "AS" => "Asia",
        "EU" => "Europe",
        "OC" => "Australia (Oceania)",
        "NA" => "North America",
        "SA" => "South America"
    );
    if (filter_var($ip, FILTER_VALIDATE_IP) && in_array($purpose, $support)) {
        $ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" . $ip));
        //$ipdat = @json_decode(file_get_contents("http://www.geoplugin.net/json.gp?ip=" ));
        if (@strlen(trim($ipdat->geoplugin_countryCode)) == 2) {
            switch ($purpose) {
                case "location":
                    $output = array(
                        "city"           => @$ipdat->geoplugin_city,
                        "state"          => @$ipdat->geoplugin_regionName,
                        "country"        => @$ipdat->geoplugin_countryName,
                        "country_code"   => @$ipdat->geoplugin_countryCode,
                        "continent"      => @$continents[strtoupper($ipdat->geoplugin_continentCode)],
                        "continent_code" => @$ipdat->geoplugin_continentCode
                    );
                    break;
                case "address":
                    $address = array($ipdat->geoplugin_countryName);
                    if (@strlen($ipdat->geoplugin_regionName) >= 1)
                        $address[] = $ipdat->geoplugin_regionName;
                    if (@strlen($ipdat->geoplugin_city) >= 1)
                        $address[] = $ipdat->geoplugin_city;
                    $output = implode(", ", array_reverse($address));
                    break;
                case "city":
                    $output = @$ipdat->geoplugin_city;
                    break;
                case "state":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "region":
                    $output = @$ipdat->geoplugin_regionName;
                    break;
                case "country":
                    $output = @$ipdat->geoplugin_countryName;
                    break;
                case "countrycode":
                    $output = @$ipdat->geoplugin_countryCode;
                    break;
				case "ip":
                    $output = @$ipdat->geoplugin_request;
                    break;
            }
        }
    }
    return $output;
}


function regexp_url_search($arr) {
	return sprintf('<a href="%1$s" target="_blank">%1$s</a>', $arr[0]);
}

function greeting() {
	$time = strftime("%H:%M" , time());
	if($time >= "00:00" && $time < "11:59" ) {
		return "Good morning! ";
	} elseif($time >= "12:00" && $time < "17:59") {
		return "Good afternoon! ";
	} else {
		return "Good evening! ";
	}
}
?>