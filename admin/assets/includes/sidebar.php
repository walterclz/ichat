<div class="sidebar" data-active-color="rose" data-background-color="black" data-image="assets/img/sidebar-3.jpg">
<!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"
Tip 2: you can also add an image using data-image tag
-->
<?php
$department = "";
if($current_user->prvlg_group != "1") {
	$department = " AND department = '{$current_user->department_id}' ";
}
$requests_count = Chat::count_everything(" AND offline = 1 AND ended = 0 {$department} ");

$cur_page = basename($_SERVER['PHP_SELF'] , '.php');  
$main_nav = array();
$main_nav[] = array('icon'=>'dashboard', 'privilege' => 'index' , 'label'=>'Dashboard',
							'pages'=> array('index'), 'url'=>'index.php', 'badge'=> false , 'badge_class'=> false ,
							'treeview' => false
							);
$main_nav[] = array('icon'=>'chat', 'privilege' => 'chatroom' , 'label'=>'Chat Room',
							'pages'=> array('chatroom' , 'archive'), 'url'=>'chatroom.php', 'badge'=> false , 'badge_class'=> false ,
							'treeview' => false
							);
$main_nav[] = array('icon'=>'map', 'privilege' => 'map' , 'label'=>'Live Map',
							'pages'=> array('map'), 'url'=>'map.php', 'badge'=> false , 'badge_class'=> false ,
							'treeview' => false
							);
$main_nav[] = array('icon'=>'business_center', 'privilege' => 'departments' , 'label'=>'Departments',
							'pages'=> array('departments'), 'url'=>'departments.php', 'badge'=> false , 'badge_class'=> false ,
							'treeview' => false
							);
$main_nav[] = array('icon'=>'group', 'privilege' => 'agents' , 'label'=>'Agents',
							'pages'=> array('agents'), 'url'=>'agents.php', 'badge'=> false , 'badge_class'=> false ,
							'treeview' => false
							);
$main_nav[] = array('icon'=>'live_help', 'privilege' => 'offline_requests' , 'label'=>'Offline Requests',
							'pages'=> array('offline_requests'), 'url'=>'offline_requests.php', 'badge'=> $requests_count , 'badge_class'=> 'primary' ,
							'treeview' => false
							);
$main_nav[] = array('icon'=>'person', 'privilege' => 'profile' , 'label'=>'My Profile',
							'pages'=> array('profile'), 'url'=>'profile.php', 'badge'=> false , 'badge_class'=> false ,
							'treeview' => false
							);
$main_nav[] = array('icon'=>'exit_to_app', 'privilege' => 'index' , 'label'=>'Logout',
							'pages'=> array('logout'), 'url'=>'logout.php', 'badge'=> false , 'badge_class'=> false ,
							'treeview' => false
							);
?>
	<div class="logo">
		<a href="index.php" class="simple-text">
			iChat Live Support
		</a>
	</div>
	<div class="sidebar-wrapper">
		<ul class="nav">
			<?php foreach($main_nav as $nav) {
				if($current_user->can_see_this($nav['privilege'],$group)) {
					echo "<li class='";
						if(in_array($cur_page , $nav['pages'])) {
							echo ' active ';
						}
					echo "'>";
						echo "<a href='{$nav['url']}'><i class='material-icons'>{$nav['icon']}</i><p>{$nav['label']}";
							if($nav['badge']) { echo "<span class='label label-{$nav['badge_class']} pull-right' >{$nav['badge']}</span>"; }
						echo "</p></a>";
					echo "</li>";
				}
			} ?>
		</ul>
	</div>
</div>