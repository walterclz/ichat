<?php
require_once("route.php");

if ($session->is_logged_in() == true ) {
	$current_user = User::get_specific_id($session->admin_id);
	$current_user->last_seen = "0";
	$current_user->update();
}

	// 2. destroy session vars ..
	$_SESSION = array();
	
	// 3. destroy session cookie ..
	if (isset($_COOKIE[session_name()])) {
	setcookie(session_name() , '' , time()-42000 , '/');		
	}


	// 4. destroy the session ..
	session_destroy();
	
	
	redirect_to("login.php?logout=1");
?>