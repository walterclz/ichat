<?php 
require_once("../../route.php");

ini_set('max_execution_time', 300);

if ($current_user->prvlg_group == "1") {
	
	$db->backup_database();
	Log::log_action($current_user->id , "Database Backup" , "Download a database backup");
	
} else {

	redirect_to("index.php");

}
?>