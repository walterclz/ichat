<?php require_once('assets/includes/header.php');

if (isset($_POST['send-chat'])) {
		if($_POST['hash'] == $_SESSION[$elhash]){
			//unset($_SESSION[$elhash]);
			$chat_id = $db->escape_value($_POST['edit_id']);
			if(Chat::check_id_existance($chat_id)) {
				$receiver= $db->escape_value($_POST['receiver']);
				$message= $db->escape_value($_POST['msg']);
				
				$chat = Chat::get_specific_id($chat_id);
				
				$str= $chat->transcript;
				//$str= str_replace('\\' , '' , $chat->transcript);
				//$str= str_replace('/' , '' , $str);
				
				$msgs = unserialize($str);
				
				$msg = str_replace('\\' , '' , $message);
				if($msg) {
				$msgs[] = Array("sender" => $current_user->id , 
										"receiver" => $receiver, 
										"msg" => nl2br($msg),
										"date" => strftime("%Y-%m-%d %H:%M:%S", time()),
										"viewed" => "0" );
				}
				
				$chat->transcript = serialize($msgs);
				$chat->unread_customer = $chat->unread_customer + 1;
				$chat->ended = "1";
				//$chat->ended_at = strftime("%Y-%m-%d %H:%M:%S", time());
				//$chat->duration = time() - strtotime($chat->started_at);
				if($chat->update()) {
					//EMAIL CHAT TRANSCRIPT ...	
					
					$msg = "Message sent successfully to {$chat->name} ({$chat->email})";
					redirect_to("offline_archive.php?id={$chat->id}&type=view&hash={$random_hash}&edit=success&msg={$msg}");
				}
			}
		}
}

if (isset($_POST['add_obj'])) {
		if(!$current_user->can_see_this("offline_archive.create",$group)) {
			$msg = "You don't have required permissions to do this action, please contact system admins";
			redirect_to("offline_archive.php?edit=fail&msg={$msg}");
		}
		if($_POST['hash'] == $_SESSION[$elhash]){
			//unset($_SESSION[$elhash]);
				
				$name = $db->escape_value($_POST['name']);
				if(isset($_POST['public'])) {
					$public = 1;
				} else {
					$public = 0;
				}
				
				$new = New Department();
				$new->name= $name;
				$new->public= $public;
				
				if($new->create()) {
					$msg = "New department added successfully";
					redirect_to("offline_archive.php?edit=success&msg={$msg}");
				} else {
					$msg = "Unable to save data, please try again";
					redirect_to("offline_archive.php?edit=fail&msg={$msg}");
				}
			
		} else {
			$msg = "Authorization failed, please try again";
			redirect_to("offline_archive.php?edit=fail&msg={$msg}");
		}
}

if (isset($_POST['edit_obj'])) {
		if(!$current_user->can_see_this("offline_archive.update",$group)) {
			$msg = "You don't have required permissions to do this action, please contact system admins";
			redirect_to("offline_archive.php?edit=fail&msg={$msg}");
		}
		if($_POST['hash'] == $_SESSION[$elhash]){
			//unset($_SESSION[$elhash]);
				
				$edit_id = $db->escape_value($_POST['edit_id']);
				$name = $db->escape_value($_POST['name']);
				if(isset($_POST['public'])) {
					$public = 1;
				} else {
					$public = 0;
				}
				if(!Chat::check_id_existance($edit_id)) {
					redirect_to("offline_archive.php");
				}
				$edit = Chat::get_specific_id($edit_id);
				$edit->name= $name;
				$edit->public= $public;
				
				if($edit->update()) {
					$msg = "Conversation updated successfully";
					redirect_to("offline_archive.php?edit=success&msg={$msg}");
				} else {
					$msg = "Unable to save data, please try again";
					redirect_to("offline_archive.php?edit=fail&msg={$msg}");
				}
			
		} else {
			$msg = "Authorization failed, please try again";
			redirect_to("offline_archive.php?edit=fail&msg={$msg}");
		}
}

$view_mode = false;

if (isset($_GET['type']) && isset($_GET['id']) && is_numeric($_GET['id']) && isset($_GET['hash'])) {
			$type = $db->escape_value($_GET['type']);
			$id = $db->escape_value($_GET['id']);
			$recieved_hash = $db->escape_value($_GET['hash']);
			
			if(!Chat::check_id_existance($id)) {
				redirect_to("offline_archive.php");
			}
			
			$this_obj = Chat::get_specific_id($id);
			
			if ($_SESSION[$elhash] == $recieved_hash) {
				//unset($_SESSION[$elhash]);
				switch($type) {
					case 'delete' :
						if(!$current_user->can_see_this("offline_archive.delete",$group)) {
							$msg = "You don't have required permissions to do this action, please contact system admins";
							redirect_to("offline_archive.php?edit=fail&msg={$msg}");
						}
						if($this_obj->delete()) {
							$msg = "Conversation deleted successfully";
							redirect_to("offline_archive.php?edit=success&msg={$msg}");
						} else {
							$msg = 'Unable to delete data, please try again';
							redirect_to("offline_archive.php?edit=fail&msg={$msg}");
						}
					break;
					
					case 'view' :
						if($current_user->prvlg_group != "1" && $this_obj->department != $current_user->department_id ) {
							$msg = "You don't have required permissions to do this action, please contact system admins";
							redirect_to("offline_archive.php?edit=fail&msg={$msg}");
						}
						$view_mode = true;
					break;
				
				}
			} else {
				$msg = "Authorization failed, please try again";
				redirect_to("offline_archive.php?edit=fail&msg={$msg}");
			}
}

require_once('assets/includes/sidebar.php'); ?>
<?php require_once('assets/includes/navbar.php'); ?>
            <div class="content">
                <div class="container-fluid">
                    
					<?php if($view_mode == true) { ?>
					<div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">history</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title">View Conversation #<?php echo $this_obj->id; ?> -
                                        <small class="category"><?php echo $this_obj->name; ?></small>
										<a href="offline_archive.php" class="btn btn-sm btn-flat btn-danger pull-right" style="margin:0"><i class='material-icons'>arrow_left</i> Back</a>
                                    </h4>
									
                                        <div class="row" style="padding:20px">
											<?php $chat_msgs = unserialize($this_obj->transcript); ?>
											<?php foreach($chat_msgs as $msg) {
												$sender = $msg['sender'];
												if(is_numeric($sender) && User::check_id_existance($sender) ) {
													$sender_data = User::get_specific_id($sender);
													$sender_name = $sender_data->name;
													$sender_img = 'operator';
												} else {
													$sender_name = $sender;
													$sender_img = 'customer';
												}
												?>
												<div class="direct-chat-msg <?php if(is_numeric(($sender))) { echo ' right'; } ?>">
													  <div class="direct-chat-info clearfix">
														<span class="direct-chat-name <?php if(is_numeric(($sender))) { echo ' pull-right'; } else { echo ' pull-left';  } ?>"><?php echo $sender_name; ?></span>
														<span class="direct-chat-timestamp <?php if(is_numeric(($sender))) { echo ' pull-left'; } else { echo ' pull-right';  } ?>"><?php echo date_ago($msg['date']); ?></span>
													  </div>
													  <!-- /.direct-chat-info -->
													  <img class="direct-chat-img" src="assets/img/<?php echo $sender_img; ?>.png" alt="Message User Image" style="width:42px"><!-- /.direct-chat-img -->
													  <div class="direct-chat-text <?php if(is_numeric($sender)) { echo ' bg-blue'; } ?>">
														<?php echo $msg['msg']; ?>
													  </div>
													  <?php if($msg['viewed']) {  ?><span class="<?php if(is_numeric($sender)) { echo ' pull-left'; } else { echo ' pull-right';  } ?> text-aqua" style='font-size:10px'><i class="fa fa-check"></i> Seen</span><?php } ?>
													  <!-- /.direct-chat-text -->
													</div>
											<?php } ?>
										</div>
										
										<hr>
										
										<form class="form-horizontal" method="POST" action="offline_archive.php" enctype="multipart/form-data">
										
										<h4>Your Reply:</h4>
										<div class="row">
                                            <div class="col-md-12">
                                                    <div class="form-group">
										                <textarea class="form-control" rows="5" name="msg"></textarea>
                                                    </div>
                                            </div>
                                        </div>
										
											<?php echo "<input type=\"hidden\" name=\"hash\" value=\"".$random_hash."\" readonly/>"; ?> 
											<?php echo "<input type=\"hidden\" name=\"edit_id\" value=\"".$this_obj->id."\" readonly/>"; ?> 
											<?php echo "<input type=\"hidden\" name=\"receiver\" value=\"".$this_obj->name."\" readonly/>"; ?> 
											<button type="submit" class="btn btn-rose pull-right" name="send-chat">Send Reply</button>
										</form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-profile">
                                <div class="card-avatar">
                                    <a href="#me">
                                        <img class="img" src="assets/img/customer.png" />
                                    </a>
                                </div>
                                <div class="card-content">
                                    <h6 class="category text-gray"><?php echo $this_obj->email; ?></h6>
                                    <h4 class="card-title"><?php echo $this_obj->name; ?></h4>
									<?php
										$details = json_decode(file_get_contents("http://ipinfo.io/{$this_obj->ip}/json"));
										if($details) {
											$countries = json_decode(file_get_contents("http://country.io/names.json"), true);
											echo "<br><b>I.P:</b> {$this_obj->ip} <br>";
											if(isset($details->country)) {
												echo "<img src='assets/img/flags/".strtolower($details->country)."_64.png' style='margin-right:10px; width:32px; vertical-align:-5px'>";
												if(isset($details->city) && $details->city != "" ) { echo $details->city .' - '; }
												echo $countries[$details->country];
											}
										}
									?>
                                    <p class="description">
                                        <?php echo  "<hr> Sent: " . date_to_eng($this_obj->started_at);  ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
					
					
					
					<?php } elseif($view_mode == false) {  ?>
							
					<div class="col-md-12">
                            
							
							<div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">history</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Offline Requests Archive</h4>
									
                                        <table class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                                <tr>
													<th>#</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Question</th>
                                                    <th>Send Date</th>
                                                    <th>Reply Date</th>
                                                    <th>Agent</th>
                                                    <th class="disabled-sorting text-right" style='width:60px'>Actions</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Name</th>
                                                    <th>Email</th>
                                                    <th>Question</th>
                                                    <th>Send Date</th>
                                                    <th>Reply Date</th>
                                                    <th>Agent</th>
                                                    <th class="text-right">Actions</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
											<?php 
												$per_page= 20;
												if(isset($_GET['page'])) {
													$page = $db->escape_value($_GET['page']);
												} else {
													$page = 1;
												}
												
												$query = " AND operator_id != 0 ";
												if($current_user->prvlg_group != "1") {
													$query .= " AND department = '{$current_user->department_id}' ";
												}
												
												if(isset($_GET['search']) && $_GET['search'] != '' ) {
													$term = $db->escape_value($_GET['search']);
													$query .= "AND ( name LIKE '%{$term}%' OR email LIKE '%{$term}%' OR question LIKE '%{$term}%' )";
												} else {
													$query .= " AND offline = 1 AND ended = 1 ";
												}
												
												$total_count = Chat::count_everything($query);
												$pagination = new Pagination($page, $per_page, $total_count);
												$chat = Chat::get_everything(" {$query} LIMIT {$per_page} OFFSET {$pagination->offset()} ");
												
												if($chat) {
												$i= (($page-1) * $per_page) + 1;
												
												foreach($chat as $c ) {
													
													if($c->ended_at != "0000-00-00 00:00:00") {
														$duration = secondsToTime($c->duration);
													} else {
														$duration = secondsToTime(time() - strtotime($c->started_at)) . " (Not finished yet!)";
													}
													
													$agent= User::get_specific_id($c->operator_id);
											?>
												<tr>
                                                    <td onclick="document.location='offline_archive.php?id=<?php echo $c->id; ?>&type=view&hash=<?php echo $random_hash; ?>'; return false;" style="cursor:pointer" ><?php echo $i; ?></td>
                                                    <td onclick="document.location='offline_archive.php?id=<?php echo $c->id; ?>&type=view&hash=<?php echo $random_hash; ?>'; return false;" style="cursor:pointer" ><?php echo $c->name; ?></td>
                                                    <td onclick="document.location='offline_archive.php?id=<?php echo $c->id; ?>&type=view&hash=<?php echo $random_hash; ?>'; return false;" style="cursor:pointer" ><?php echo $c->email; ?></td>
                                                    <td onclick="document.location='offline_archive.php?id=<?php echo $c->id; ?>&type=view&hash=<?php echo $random_hash; ?>'; return false;" style="cursor:pointer" ><?php echo strip_tags($c->question); ?></td>
                                                    <td onclick="document.location='offline_archive.php?id=<?php echo $c->id; ?>&type=view&hash=<?php echo $random_hash; ?>'; return false;" style="cursor:pointer" ><?php echo date_to_eng($c->started_at); ?></td>
                                                    <td onclick="document.location='offline_archive.php?id=<?php echo $c->id; ?>&type=view&hash=<?php echo $random_hash; ?>'; return false;" style="cursor:pointer" ><?php echo $duration; ?></td>
                                                    <td onclick="document.location='profile.php?id=<?php echo $agent->id; ?>&type=view&hash=<?php echo $random_hash; ?>'; return false;" style="cursor:pointer" ><?php echo $agent->name; ?></td>
                                                    <td class="text-right">
                                                        <a href="offline_archive.php?id=<?php echo $c->id; ?>&type=view&hash=<?php echo $random_hash; ?>"  class="<?php if($current_user->can_see_this("offline_archive.read",$group)) { echo ' text-success'; } ?> "><i class="material-icons">chat</i></a>
                                                        <a href="offline_archive.php?id=<?php echo $c->id; ?>&type=delete&hash=<?php echo $random_hash; ?>" class="<?php if($current_user->can_see_this("offline_archive.delete",$group)) { echo ' text-danger'; } ?> " onclick="return confirm('Are you sure you want to delete this chat?');"><i class="material-icons">close</i></a>
                                                    </td>
                                                </tr>
												<?php $i++; }} ?>
                                            </tbody>
                                        </table>
										
										<?php
										//if(isset($pagination) && $pagination->total_pages() > 1) {
										?>
										<ul class="pagination pagination-primary">
										
												<?php
												if ($pagination->has_previous_page()) {
													$params = $_GET; $params['page'] = $pagination->previous_page(); $page_param = http_build_query($params);
													echo "<li><a href=\"offline_archive.php?{$page_param}\" class=\"\" ><i class=\"fa fa-chevron-left\"></i></a></li>";
												} else {
												?>
												<li><a href="#me"><i class="fa fa-chevron-left"></i></a></li>
												<?php
												}
												
												for($p=1; $p <= $pagination->total_pages(); $p++) {
													if($p == $page) {
														echo "<li class='active'><a href='#me'>{$p}</a></li>";
													} else {
														$params = $_GET; $params['page'] = $p; $page_param = http_build_query($params);
														echo "<li><a href=\"offline_archive.php?{$page_param}\">{$p}</a></li>";
													}
												}
												if($pagination->has_next_page()) {
													$params = $_GET; $params['page'] = $pagination->next_page(); $page_param = http_build_query($params);
													echo "<li><a href=\"offline_archive.php?{$page_param}\"><i class=\"fa fa-chevron-right\"></i></a></li>";
												} else {
												?>
												<li><a href="#me" ><i class="fa fa-chevron-right"></i></a></li>
												<?php
												}
												?>
													
										</div>
										<?php
										//}
										?>
										
										
										
									
									
                                </div>
								<br style="clear:both"><br style="clear:both"><br style="clear:both">
                            </div>
					<?php } ?>
                        </div>
                </div>
            
            <?php require_once("assets/includes/footer.php"); ?>
	<?php require_once("assets/includes/preloader.php"); ?>

<script type="text/javascript">
$('#datatables').DataTable({
	"pagingType": "full_numbers",
	"lengthMenu": [
		[10, 25, 50, -1],
		[10, 25, 50, "All"]
	],
	responsive: true,
	language: {
		search: "_INPUT_",
		searchPlaceholder: "Search departments",
	}
});
</script>


</html>