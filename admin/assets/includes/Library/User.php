<?php
require_once("Loader.php");

class User Extends OneClass {
	
	public static $table_name = "users";
	public static $db_fields = array( "id" , "name" , "prvlg_group", "username", "password", "reset_hash",  "email", "mobile" , "address", "position" , "about" , "department_id" , "disabled","deleted" ,"last_seen",  "education" , "avatar" );
	
	public $id;
	public $name;
	public $position;
	public $about;
	public $username;
	public $password;
	public $reset_hash;
	public $prvlg_group;
	public $email;
	public $mobile;
	public $address;
	public $disabled;
	public $deleted;
	public $department_id;
	public $last_seen;	
	public $education;	
	public $avatar;
	
	
	public static function hash_authenticate($username="") {
	global $db;
	$username = $db->escape_value($username);
	
	$sql = "SELECT * FROM  " . DBTP . self::$table_name ;
	$sql .= " WHERE username = '{$username}' ";
	$sql .= "AND deleted='0' ";
	$sql .= "LIMIT 1" ;
	$result_array =  self::preform_sql($sql);
	return (!empty($result_array)) ? array_shift($result_array) : false;
	}

	public static function authenticate($username="", $password="") {
	global $db;
	$username = $db->escape_value($username);
	$password = $db->escape_value($password);

	$sql = "SELECT * FROM  " . DBTP . self::$table_name ;
	$sql .= " WHERE username = '{$username}' ";
	$sql .= "AND password = '{$password}' ";
	$sql .= "AND deleted='0' ";
	$sql .= "LIMIT 1" ;
	$result_array =  self::preform_sql($sql);
	return (!empty($result_array)) ? array_shift($result_array) : false;
	}

	
	public static function can_see_this($sent_item=0,$sent_group=0) {
		global $db;
		$item = $db->escape_value($sent_item);
		$group = $db->escape_value($sent_group);
		$group_query = Group::get_specific_id($group);
		$group_privileges = $group_query->privileges;
		
		$pos = strpos($group_privileges,$item);

		if($pos === false) {
			return false;
		}
		else {
			return true;
		}	
	}
	
	public static function get_users_for_group_except($id=0,$query="", $string="") {
	global $db;
	global $current_user;
	return self::preform_sql("SELECT * FROM " . DBTP . self::$table_name ." WHERE id != {$current_user->id} AND prvlg_group = '" . $id . "' AND deleted = 0 " . $query . " ORDER BY name DESC " . $string." ");
	}
	
	public static function get_users_for_group($id=0,$query="", $string="") {
	global $db;
	return self::preform_sql("SELECT * FROM " . DBTP . self::$table_name ." WHERE prvlg_group = '" . $id . "' AND deleted = 0 " . $query . " ORDER BY name DESC " . $string." ");
	}
	
	public static function get_users($query="", $string="") {
	global $db;
	return self::preform_sql("SELECT * FROM " . DBTP . self::$table_name ." WHERE deleted = 0 " . $query . " ORDER BY name ASC " . $string." ");
	}
	
	public static function get_online($query="", $string="") {
	global $db;
	global $current_user;
	$online_diff = strtotime("-5 Minutes" , time());
	return self::preform_sql("SELECT * FROM " . DBTP . self::$table_name ." WHERE id != '{$current_user->id}' AND last_seen >= '{$online_diff}' AND deleted = 0 " . $query . " ORDER BY name ASC " . $string." ");
	}
	
	public static function get_online_agents($department_id="") {
	global $db;
	$online_diff = strtotime("-5 Minutes" , time());
	$result_array = self::preform_sql("SELECT id FROM " . DBTP . self::$table_name ." WHERE last_seen >= '{$online_diff}' AND deleted = 0 AND ( department_id = '{$department_id}'  OR prvlg_group = '1' ) ORDER BY name ASC ");
	return !empty($result_array) ? $result_array : false;
	}
	
	
	public static function check_existance_except($column="" , $value="" , $id="") {
	global $db;
	$column = $db->escape_value($column);
	$value = $db->escape_value($value);
	
	$sql = "SELECT * FROM  " . DBTP . self::$table_name;
	$sql .= " WHERE {$column} = '{$value}' ";
	$sql .= " AND id != '{$id}' ";
	$sql .= " AND deleted= 0 ";
	$sql .= "LIMIT 1" ;
	$result_array =  $db->query($sql);
	return $db->num_rows($result_array) ? true : false;
	}
	
	public function set_online() {
		global $db;
		$this->last_seen = time();
		if ($this->update()) { return true; } else { return false; }
	}
	
	
	public function get_avatar() {
		global $db;
		if($this->avatar) {
			$img = File::get_specific_id($this->avatar);
			$dev_avatar = WEB_LINK."assets/".$img->image_path();
			$dev_avatar_path = UPLOADPATH."/".$img->image_path();
			if (!file_exists($dev_avatar_path)) {
				$dev_avatar = WEB_LINK.'assets/img/operator.png';
			}
		} else {
			$dev_avatar = WEB_LINK.'assets/img/operator.png';
		}
		return $dev_avatar;
	}
	
}
	
?>