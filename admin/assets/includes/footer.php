<footer class="footer">
	<div class="container-fluid">
		<p class="copyright pull-right">
			<span style='color:#d2d6de'>V. 1.6</span>
			&copy;
			<script>
				document.write(new Date().getFullYear())
			</script>
			<a href="http://www.michael-designs.com" target="_blank">Michael Designs</a> <i class="fa fa-heart"></i> Theme by <a href="http://www.creative-tim.com" target="_blank">Creative Tim</a>
		</p>
	</div>
</footer>