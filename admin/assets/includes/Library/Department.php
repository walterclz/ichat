<?php
require_once("Loader.php");

class Department Extends OneClass {
	
	public static $table_name = "departments";
	public static $db_fields = array( "id" , "name" , "public" , "deleted" );
	
	public $id;
	public $name;
	public $public;
	public $deleted;
	
}
	
?>