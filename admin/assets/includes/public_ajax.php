<?php
require_once("Library/Loader.php");

if(isset($_SESSION[$elhash_login]) && $_SESSION[$elhash_login] != "") { 
	$random_hash = $_SESSION[$elhash_login];
} else {
	$random_hash = uniqid();
	$_SESSION[$elhash_login] = $random_hash;
}

if (isset($_GET['type']) && !empty($_GET['type']) && isset($_POST['hash']) && !empty($_POST['hash']) && isset($_POST['data']) && !empty($_POST['data']) && isset($_POST['id']) && is_numeric($_POST['id'])) {
	$id = $db->escape_value($_POST['id']);
	$type = $db->escape_value($_GET['type']);
	$hash = $db->escape_value($_POST['hash']);
	$data = $db->escape_value($_POST['data']);
	if($_POST['hash'] == $_SESSION[$elhash_login]){
	switch($type) {
			case 'chat' :
				if(!Chat::check_id_existance($data)) {
					return false;
					die();
				}
				
				$chat = Chat::get_specific_id($data);
				echo "<div style='padding:20px' class='emoticons' >";
				if($chat) {
					
					$str= $chat->transcript;
					//$str= str_replace('\\' , '' , $chat->transcript);
					//$str= str_replace('/' , '' , $str);
					
					$chat_msgs = unserialize($str);
					
					$upd_chat_msgs = array();
					foreach($chat_msgs as $msg) {
						$sender = $msg['sender'];
						if(is_numeric($sender) && User::check_id_existance($sender) ) {
							$sender_data = User::get_specific_id($sender);
							$sender_name = $sender_data->name;
							$sender_img = $sender_data->get_avatar();
						} else {
							$sender_name = $sender;
							$sender_img = 'admin/assets/img/customer.png';
						}
						?>
						<div class="direct-chat-msg <?php if(is_numeric(($sender))) { echo ' right'; } ?>">
							  <div class="direct-chat-info clearfix">
								<span class="direct-chat-name <?php if(is_numeric(($sender))) { echo ' pull-right'; } else { echo ' pull-left';  } ?>"><?php echo $sender_name; ?></span>
								<span class="direct-chat-timestamp <?php if(is_numeric(($sender))) { echo ' pull-left'; } else { echo ' pull-right';  } ?>"><?php echo date_ago($msg['date']); ?></span>
							  </div>
							  <!-- /.direct-chat-info -->
							  <img class="direct-chat-img" src="<?php echo $sender_img; ?>" alt="Message User Image" style="width:42px"><!-- /.direct-chat-img -->
							  <div class="direct-chat-text <?php if(is_numeric($sender)) { echo ' bg-blue'; } ?>">
								<?php //echo $msg['msg'];
								
								$cmnt= nl2br($msg['msg']);
								echo preg_replace_callback('#(?<!href\=[\'"])(https?|ftp|file)://[-A-Za-z0-9+&@\#/%()?=~_|$!:,.;]*[-A-Za-z0-9+&@\#/%()=~_|$]#', 'regexp_url_search', $cmnt);
								
								?>
							  </div>
							  <?php if($msg['viewed']) {  ?><span class="<?php if(is_numeric($sender)) { echo ' pull-left'; } else { echo ' pull-right';  } ?> text-aqua" style='font-size:10px'><i class="fa fa-check"></i> Seen</span><?php } ?>
							  <!-- /.direct-chat-text -->
							</div>
						<?php
						if($msg['receiver'] == $chat->name && $msg['viewed'] == 0 ) {
							echo '<input type="hidden" value="unread">';
						}
						
						if($msg['receiver'] == $chat->name) {
							$msg['viewed'] = 1;
						}
					$upd_chat_msgs[] = $msg;
					}
					
					
					
					$chat->transcript = serialize($upd_chat_msgs);
					$chat->unread_customer = 0;
					$chat->update();
					
					
					if($chat->operator_id ) { echo '<input type="hidden" value="answered">'; } else {
						echo "<h5><center>Please wait.. one of our agents will join the conversation shortly</center></h5>";
					}
					
					if($chat->transferred_to) { echo '<input type="hidden" value="transferred">';
						echo "<h5><center>Please wait.. one of our agents will join the conversation shortly</center></h5>";
					}
					
					if($chat->ended) {
						echo '<br><h5><center>This chat session is ended .. Thanks for contacting us<br><a href="#me" class="btn btn-sm btn-flat btn-primary close-window">Close Window</a></center></h5>';
					}
				}
				?>
			</div>
			<script type="text/javascript">
				$('.emoticons').emotions();
			</script>
				<?php
			break;
		###############################################################
		case 'reply-box' :
		
		$chat = Chat::get_specific_id($data);
		
		echo "<input type=\"hidden\" name=\"hash\" value=\"".$hash."\" readonly/>";
		echo "<input type=\"hidden\" name=\"chat_id\" value=\"".$chat->id."\" readonly/>";
		echo "<input type=\"hidden\" name=\"receiver\" value=\"".$chat->operator_id."\" readonly/>";
		echo "<input type=\"hidden\" name=\"send-chat\" value=\"true\" readonly/>";
		
		
		
		break;
		###############################################################
		case 'agent-info' :
		
		$chat = Chat::get_specific_id($data);
		
		$agent = User::get_specific_id($chat->operator_id);
		$d = Department::get_specific_id($agent->department_id);
		if($agent) {
			$url = $agent->get_avatar();
			echo "<center style='line-height:1.2'><img src='{$url}' style='width:56px; padding-right:10px; margin-top:-10px' class='pull-left' ><b>".$agent->name."</b><br><small>".$d->name."</small>";
			echo "<br><small>Rate Me:<span id='rate_me'>";
			for($i = 1 ; $i <= 5 ; $i++) {
					echo "<a href='#me' class='rate' data-stars='{$i}'><i class='material-icons text-muted'>star</i></a>";
				}
			echo "</span></small></center>";
		}
		
		break;
		###############################################################
		case 'rate' :
		
		$chat = Chat::get_specific_id($data);
		$stars = $id;
		for($i = 1 ; $i <= 5 ; $i++) {
			echo "<a href='#me' class='rate' data-stars='{$i}'><i class='material-icons ";
				if($i <= $stars) { echo ' text-warning'; } else { echo ' text-muted'; }
			echo "'>star</i></a>";
		}
		
		$chat->rating = $stars;
		$chat->update();
		
		break;
		###############################################################
		case 'end-chat' :
		
		$chat = Chat::get_specific_id($data);
		
		$chat->ended = 1;
		$chat->ended_at = strftime("%Y-%m-%d %H:%M:%S" , time());
		$chat->duration = time() - strtotime($chat->started_at);
		$chat->update();
		
		//if($chat->invite_id) {
			$online_users = Visitor::get_everything(" AND (id = '{$chat->invite_id}' OR chat_id = '{$chat->id}') ");
			if($online_users) {
				foreach($online_users as $online_user) { $online_user->delete(); }
			}
		//}
		
		break;
		###############################################################
		case 'chat-timer' :
		
		echo '<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<small>Chatting for: <span id="stopwatch">00:00:00</span></small>';
		echo '<script src="admin/assets/js/jquery.timer.js"></script>';
		echo '<script src="admin/assets/js/jquery.timer.demo.js"></script>';
		
		
		break;
		###############################################################
		case 'read_msg' :
			
			if(!EMail::check_id_existance($id)) {
				echo "<h4 style=\"color:red; font-family:Century Gothic\" ><center>Error! This page can't be accessed directly! please try again using main program #item-selector</center></h4>";
			}
			
			if(!EMail::check_ownership($id, $current_user->id)) {
				echo "<h4 style=\"color:red; font-family:Century Gothic\" ><center>Error! This page can't be accessed directly! please try again using main program #item-ownership</center></h4>";
			}
			
			$mymsg = 0;
			
			$mail_msg = EMail::get_specific_id($id);
			$last_reply = Reply::get_last_reply_for($id);
			
			if($last_reply) {
				if ($last_reply->sender == $current_user->id) {
					$mymsg = 1;
				}
			}
			
			if ($data =="received" && $mymsg == 0) { $mail_msg->read_msg(); }
			
			
		break;	
		###############################################################
		case 'set_online' :
			
			$session = session_id();
			$time = time();
			$time_check = $time - 300;     //We Have Set Time 5 Minutes
			
			//Current Session
			$currently_online = Visitor::count_everything(" AND session = '{$session}' ");
			
			//If count is 0 , then enter the values
			if($currently_online == "0") {
				$visitor = new Visitor();
				$visitor->session = $session;
				$visitor->time = $time;
				$visitor->ip = ip_info("visitor", "ip");
				$visitor->country = ip_info("visitor", "country");
				$visitor->city = ip_info("visitor", "city");
				$visitor->address = ip_info("visitor", "address");
				$visitor->current_page = $data;
				$visitor->create();
			} else {
				$update_session = Visitor::update_session($session, $data);
			}
			
			// after 5 minutes, session will be deleted 
			$delete_session = Visitor::delete_session($time_check);
			
		break;	
		###############################################################
		case 'check_invitation' :
			
			$session = session_id();
			
			//Current Session
			$currently_online = Visitor::get_everything(" AND session = '{$session}' ORDER BY id DESC LIMIT 1");
			
			//If count is 0 , then enter the values
			if($currently_online) {
				foreach($currently_online as $sess) {
					if($sess->invitation == '1') {
						echo 'invited';
						$sess->invitation = '2';
						$sess->update();
					}
				}
			} else {
				return false;
			}
			
		break;	
		###############################################################
		default : 
			echo "<h4 style=\"color:red; font-family:Century Gothic\" ><center>Error! This page can't be accessed directly! please try again using main program #switch</center></h4>";
			die();
		break;
	}
	}
} else {
	
	echo "<h4 style=\"color:red; font-family:Century Gothic\" ><center>Error! This page can't be accessed directly! please try again using main program #intro</center></h4>";
	die();
}
?>