<?php
require_once("Loader.php");

class Chat Extends OneClass {
	
	public static $table_name = "chat";
	public static $db_fields = array( "id" , "name" , "email" , "department" , "question" , "operator_id" , "invite_id" , "transferred_to" , "transferred_from" , "transcript", "ended", "started_at", "ended_at" , "duration", "rating" , "notified" , "offline" , "unread_client" , "unread_operator" , "ip");
	
	public $id;
	public $name;
	public $email;
	public $department;
	public $question;
	public $operator_id;
	public $invite_id;
	public $transferred_from;
	public $transferred_to;
	public $transcript;
	public $ended;
	public $started_at;
	public $ended_at;
	public $duration;
	public $rating;
	public $notified;
	public $offline;
	public $unread_client;
	public $unread_operator;
	public $ip;
	
	public static function get_pending($user_id = "", $department="") {
		global $db;
		$result_array =  static::preform_sql("SELECT * FROM " .  DBTP. static::$table_name  . " WHERE operator_id = '0' AND offline = '0' {$department} AND notified NOT LIKE '%|{$user_id}|%' ORDER BY id ASC LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	public static function get_transfers($user_id = "") {
		global $db;
		$result_array =  static::preform_sql("SELECT * FROM " .  DBTP. static::$table_name  . " WHERE operator_id = '{$user_id}' AND transferred_to = '{$user_id}' ORDER BY id ASC LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	public static function get_for_invite($invite_id = "") {
		global $db;
		$result_array =  static::preform_sql("SELECT * FROM " .  DBTP. static::$table_name  . " WHERE invite_id = '{$invite_id}' ORDER BY id DESC LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
	}
	
	public static function get_pending_list($department = "") {
		global $db;
		$result_array =  static::preform_sql("SELECT * FROM " .  DBTP. static::$table_name  . " WHERE operator_id = '0' AND offline = '0' {$department} ORDER BY id DESC");
		return $result_array;
	}
	
	public static function get_names($user_id = "" , $ended=" AND ended = '0' ") {
		global $db;
		$result_array =  static::preform_sql("SELECT * FROM " .  DBTP. static::$table_name  . " WHERE operator_id = '{$user_id}' {$ended}  AND offline = '0' ORDER BY id ASC");
		return $result_array;
	}
	
	public static function get_first_chat() {
		global $db;
		$result_array =  $db->query("Select min(started_at),max(ended_at) from " . DBTP. static::$table_name. " WHERE offline = 0 ");
		return $db->fetch_array($result_array);
	}
	
	public static function get_first_offline_chat() {
		global $db;
		$result_array =  $db->query("Select min(started_at),max(ended_at) from " . DBTP. static::$table_name . " WHERE offline = 1 ");
		return $db->fetch_array($result_array);
	}
	
	public function view_transcript() {
		$transcript= $this->transcript;
		$msgs = unserialize($transcript);
		$str = "";
		if(is_array($msgs)) {
			$str .= "Hi, <b>{$this->name}</b>!<br>You've contacted our live support lately, we received your message and gladly one of our support agents replied<br>Here's your conversation summary:<hr>";
			$i=1;
			foreach($msgs as $msg) {
				
						$sender = $msg['sender'];
						if(is_numeric($sender) && User::check_id_existance($sender) ) {
							$sender_data = User::get_specific_id($sender);
							$sender_name = $sender_data->name;
							if($sender_data->department_id) {
								$department = Department::get_specific_id($sender_data->department_id);
								$sender_name .= " (Department: {$department->name})";
							}
						} else {
							$sender_name = $sender;
						}
					
					$str .="<p>- <u>Sender</u>: <b>{$sender_name}</b> at (".date_to_eng($msg['date']).")<br>";
					if($i == 1 ) { $str .="- <u>Question</u>: <b>{$this->question}</b><br>"; }
					$str .="- <u>Message</u>: {$msg['msg']}<br></p>";
				$i++;
			}
		}
		return $str;
	}
	
	
}
	
?>