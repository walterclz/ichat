<?php
require_once("../../route.php");

if(isset($_SESSION[$elhash]) && $_SESSION[$elhash] != "") { 
	$random_hash = $_SESSION[$elhash];
} else {
	$random_hash = uniqid();
	$_SESSION[$elhash] = $random_hash;
}

if (isset($_GET['type']) && !empty($_GET['type']) && isset($_POST['hash']) && !empty($_POST['hash']) && isset($_POST['data']) && !empty($_POST['data']) && isset($_POST['id']) && is_numeric($_POST['id'])) {
	$id = $db->escape_value($_POST['id']);
	$type = $db->escape_value($_GET['type']);
	$hash = $db->escape_value($_POST['hash']);
	$data = $db->escape_value($_POST['data']);
	
	switch($type) {
		###############################################################
			case 'check-invite' :
				
				$pending = Chat::get_for_invite($data);
				if($pending) {
					echo 'accepted';
				} else {
					echo 'pending';
				}
				
			break;
		###############################################################
			case 'pending-chats' :
				$department = "";
				if($current_user->prvlg_group != "1") {
					$department = " AND department = '{$current_user->department_id}' ";
				}
				
				$pending = Chat::get_pending($current_user->id, $department);
				if($pending) {
					echo "<img src='assets/img/phone_calling.gif' style='width:80px'>Chat Request from <b>{$pending->name} ({$pending->email})</b>";
					echo "<div class='btn-group pull-right'><a href='chatroom.php?accept={$pending->id}' class='accept_chat btn btn-success btn-round' style='color:white'  data-chat_id='{$pending->name}'><i class='material-icons'>check</i> Accept</a><a href='#me' class='decline_chat btn btn-danger btn-round'  style='color:white' ><i class='material-icons'>close</i> Decline</a></div>";
					$pending->notified = $pending->notified . "|{$current_user->id}|";
					$pending->update();
				}
			break;
		###############################################################
			case 'transferred-chats' :
				
				$transfers = Chat::get_transfers($current_user->id);
				if($transfers) {
					$prev_agent = User::get_specific_id($transfers->transferred_from);
					echo "<img src='assets/img/phone_calling.gif' style='width:80px'>Conversation from client <b>{$transfers->name} ({$transfers->email})</b> - Transferred to you from <b>{$prev_agent->name}</b>";
					echo "<div class='btn-group pull-right'><a href='chatroom.php?transfer={$transfers->id}' class='accept_chat btn btn-success btn-round' style='color:white'  data-chat_id='{$transfers->name}'><i class='material-icons'>check</i> View Chat</a></div>";
				}
			break;
		###############################################################
			case 'agents-list' :
				$online_diff = strtotime("-5 Minutes" , time());
				$agents = User::get_everything(" AND department_id = '{$data}' AND deleted = 0 AND last_seen >= '{$online_diff}'");
				if($agents) {
					foreach($agents as $agent) {
						$rule = Group::get_specific_id($agent->prvlg_group);
						if($agent->id != $current_user->id) {
							echo "<option value='{$agent->id}'>{$agent->name} [{$rule->name}]</option>";
						}
					}
				} else {
					echo '<option selected disabled>No Online Agents!</option>';
				}
			break;
		###############################################################
			case 'pending-list' :
				$department = "";
				if($current_user->prvlg_group != "1") {
					$department = " AND department = '{$current_user->department_id}' ";
				}
				$chat_list = Chat::get_pending_list($department);
				if($chat_list) { 
					foreach($chat_list as $p) {
						echo "<div><i class='material-icons' style='font-size:16px'>call</i> <b>{$p->name} ({$p->email})";
						echo "<div class='btn-group pull-right'><a href='chatroom.php?accept={$p->id}' class='accept_chat btn btn-success btn-fab btn-fab-mini' style='color:white'  data-chat_id='{$p->id}'><i class='material-icons'>check</i></a><a href='#me' class='decline_chat_list btn btn-danger btn-fab btn-fab-mini'  style='color:white' ><i class='material-icons'>close</i></a></div><br style='clear:both'/><hr></div>";
					}
				} else { ?><h5 class="text-muted text-center"><i class="material-icons" style="font-size:70px">info_outline</i><br>You don't have any pending chat requests</h5><?php }
			break;
		###############################################################
			case 'invite-list' :
				$invite_list = Visitor::get_everything(" AND invited_by = {$current_user->id} AND ( invitation = 1 OR invitation = 2 ) "); ?>
				<?php if($invite_list) { ?>
				<h5><center>Invitations List</center></h5>
				<?php foreach($invite_list as $p) {
						echo "<div class='btn-group pull-right' style='margin-top:0px'><a href='chatroom.php?close-invite={$p->id}' class='decline_invite_list btn btn-danger btn-fab btn-fab-mini' style='color:white'  data-chat_id='{$p->id}'><i class='material-icons'>close</i></a></div><div><i class='fa fa-laptop' style='font-size:16px'></i> <b>{$p->ip} ({$p->city})</b>";
						echo "<br>Waiting customer response<br style='clear:both'/><hr></div>";
					}
				}
			break;
		###############################################################
			case 'names-list' :
				$current_chat = $db->escape_value($_POST['cur']);
				$not = $_POST['notify'];
				$ended = $_POST['ended'];
				
				$chat_list = Chat::get_names($current_user->id , $ended);
				if($chat_list) { 
					foreach($chat_list as $c) {
						echo "<li class=' ";
						if ($c->id == $current_chat) { echo "active"; } 
						if($c->unread_operator) { echo " unread "; }
						echo " ' id='{$c->id}' >";
							echo "<a href='#conv-{$c->id}' data-toggle='tab' aria-expanded='true' class='conv-link'  data-chat_id='{$c->id}'>{$c->name}";
								if($c->unread_operator) { echo "<span class='badge pull-right'>{$c->unread_operator}</span>"; }
							echo "</a>";
						echo "</li>";
						if($c->unread_operator && $c->id != $not ) { echo '<input type="hidden" value="unread">'; }
					}
				}
			break;
		###############################################################
			case 'open-chat' :
				if(!Chat::check_id_existance($data)) {
					return false;
					die();
				}
				$current_chat = $_POST['cur'];
				$chat = Chat::get_specific_id($data);
				echo "<div style='padding:20px' class='emoticons' >";
				if($chat) {
					
					$str= $chat->transcript;
					//$str= str_replace('\\' , '' , $chat->transcript);
					//$str= str_replace('/' , '' , $str);
					
					$chat_msgs = unserialize($str);
					
					$upd_chat_msgs = array();
					foreach($chat_msgs as $msg) {
						$sender = $msg['sender'];
						if(is_numeric($sender) && User::check_id_existance($sender) ) {
							$sender_data = User::get_specific_id($sender);
							$sender_name = $sender_data->name;
							$sender_img = $sender_data->get_avatar();
						} else {
							$sender_name = $sender;
							$sender_img = 'assets/img/customer.png';
						}
						?>
						<div class="direct-chat-msg <?php if(is_numeric(($sender))) { echo ' right'; } ?>">
							  <div class="direct-chat-info clearfix">
								<span class="direct-chat-name <?php if(is_numeric(($sender))) { echo ' pull-right'; } else { echo ' pull-left';  } ?>"><?php echo $sender_name; ?></span>
								<span class="direct-chat-timestamp <?php if(is_numeric(($sender))) { echo ' pull-left'; } else { echo ' pull-right';  } ?>"><?php echo date_ago($msg['date']); ?></span>
							  </div>
							  <!-- /.direct-chat-info -->
							  <img class="direct-chat-img" src="<?php echo $sender_img; ?>" alt="Message User Image" style="width:42px"><!-- /.direct-chat-img -->
							  <div class="direct-chat-text <?php if(is_numeric($sender)) { echo ' bg-blue'; } ?>">
								<?php //echo $msg['msg'];
								
								$cmnt= nl2br($msg['msg']);
								echo preg_replace_callback('#(?<!href\=[\'"])(https?|ftp|file)://[-A-Za-z0-9+&@\#/%()?=~_|$!:,.;]*[-A-Za-z0-9+&@\#/%()=~_|$]#', 'regexp_url_search', $cmnt);
								
								?>
							  </div>
							  <?php if($msg['viewed']) {  ?><span class="<?php if(is_numeric($sender)) { echo ' pull-left'; } else { echo ' pull-right';  } ?> text-aqua" style='font-size:10px'><i class="fa fa-check"></i> Seen</span><?php } ?>
							  <!-- /.direct-chat-text -->
							</div>
						<?php
						if($msg['receiver'] == $current_user->id && $chat->id == $current_chat ) {
							$msg['viewed'] = 1;
						}
					$upd_chat_msgs[] = $msg;
					}
					$chat->transcript = serialize($upd_chat_msgs);
					if($chat->id == $current_chat) { $chat->unread_operator = 0; }
					$chat->update();
					
					if($chat->ended) {
						echo "<h5><center>This chat session is ended";
						if($chat->rating) {
							echo "<br>Customer rated you ({$chat->rating}) Stars!<br>";
							for($i = 1 ; $i <= 5 ; $i++) {
								echo "<i class='material-icons ";
									if($i <= $chat->rating) { echo ' text-warning'; } else { echo ' text-muted'; }
								echo "'>star</i>";
							}
						}
						echo "</center></h5>";
					} else {
						if(isset($msg['date'])) {
							$diff = calc_difference_minutes(strftime("%Y-%m-%d %H:%M:%S", time()) , $msg['date']);
							if($diff['minutes'] >= 10) {
								echo "<h7><center>Customer is away for more than {$diff['minutes']} minutes, <a href='#me' class='end-chat' data-chat_id='{$chat->id}'>Click Here</a> to end this chat session</center></h7>";
							}
						}
					}
				}
				?>
			</div>
			<script type="text/javascript">
				$('.emoticons').emotions();
			</script>
				<?php
			break;
		###############################################################
		case 'end-chat' :
		
		$chat = Chat::get_specific_id($data);
		
		$chat->ended = 1;
		$chat->ended_at = strftime("%Y-%m-%d %H:%M:%S" , time());
		$chat->duration = time() - strtotime($chat->started_at);
		$chat->update();
		
		$online_users = Visitor::get_everything(" AND (id = '{$chat->invite_id}' OR chat_id = '{$chat->id}') ");
		if($online_users) {
			foreach($online_users as $online_user) { $online_user->delete(); }
		}
		
		break;
		###############################################################
		case 'customer_details' :
		$cur = $db->escape_value($_POST['cur']);
		$this_obj = Chat::get_specific_id($cur);
		?>
		
			<div class="card card-profile">
				<div class="card-avatar">
					<a href="#me">
						<img class="img" src="assets/img/customer.png" />
					</a>
				</div>
				<div class="card-content">
					<h6 class="category text-gray"><?php echo $this_obj->email; ?></h6>
					<h4 class="card-title"><?php echo $this_obj->name; ?></h4>
					<?php
						$details = json_decode(file_get_contents("http://ipinfo.io/{$this_obj->ip}/json"));
						if($details) {
							$countries = json_decode(file_get_contents("http://country.io/names.json"), true);
							echo "<br><b>I.P:</b> {$this_obj->ip} <br>";
							if(isset($details->country)) {
								echo "<img src='assets/img/flags/".strtolower($details->country)."_64.png' style='margin-right:10px; width:32px; vertical-align:-5px'>";
								if(isset($details->city) && $details->city != "" ) { echo $details->city .' - '; }
								echo $countries[$details->country];
							}
						}
					?>
					<p class="description">
						<?php echo  "<hr> Sent: " . date_to_eng($this_obj->started_at);  ?>
					</p>
				</div>
			</div>
		
		<?php
		break;
		###############################################################
		case 'read_msg' :
			
			if(!EMail::check_id_existance($id)) {
				echo "<h4 style=\"color:red; font-family:Century Gothic\" ><center>Error! This page can't be accessed directly! please try again using main program #item-selector</center></h4>";
			}
			
			if(!EMail::check_ownership($id, $current_user->id)) {
				echo "<h4 style=\"color:red; font-family:Century Gothic\" ><center>Error! This page can't be accessed directly! please try again using main program #item-ownership</center></h4>";
			}
			
			$mymsg = 0;
			
			$mail_msg = EMail::get_specific_id($id);
			$last_reply = Reply::get_last_reply_for($id);
			
			if($last_reply) {
				if ($last_reply->sender == $current_user->id) {
					$mymsg = 1;
				}
			}
			
			if ($data =="received" && $mymsg == 0) { $mail_msg->read_msg(); }
			
			
		break;
		
		
		###############################################################
		default : 
			echo "<h4 style=\"color:red; font-family:Century Gothic\" ><center>Error! This page can't be accessed directly! please try again using main program #switch</center></h4>";
			die();
		break;
	}
	
} else {
	
	echo "<h4 style=\"color:red; font-family:Century Gothic\" ><center>Error! This page can't be accessed directly! please try again using main program #intro</center></h4>";
	die();
}
?>