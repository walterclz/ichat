<?php require_once('assets/includes/header.php'); ?>
<?php
if (isset($_POST['transfer-chat'])) {
		if($_POST['hash'] == $_SESSION[$elhash]){
			//unset($_SESSION[$elhash]);
			$chat_id = $db->escape_value($_POST['chat_id']);
			if(Chat::check_id_existance($chat_id)) {
				
				$agent_id = $db->escape_value($_POST['agent_id']);
				if($agent_id == "" || !User::check_id_existance($agent_id)) {
					$msg = "Sorry, you must choose an online agent first!";
					redirect_to('chatroom.php?edit=fail&msg=' .$msg);
				}
				
				$chat = Chat::get_specific_id($chat_id);
				$new_agent = User::get_specific_id($agent_id);
				
				$str= $chat->transcript;
				//$str= str_replace('\\' , '' , $chat->transcript);
				//$str= str_replace('/' , '' , $str);
				
				$msgs = unserialize($str);
				
				$msg = "Chat transferred to agent ({$new_agent->name})";
				
				if($msg) {
				$msgs[] = Array("sender" => $current_user->id , 
										"receiver" => $chat->name, 
										"msg" => $msg,
										"date" => strftime("%Y-%m-%d %H:%M:%S", time()),
										"viewed" => "0" );
				}
				
				$chat->transcript = serialize($msgs);
				$chat->unread_customer = $chat->unread_customer + 1;
				$chat->operator_id = $agent_id;
				$chat->transferred_from = $current_user->id;
				$chat->transferred_to = $agent_id;
				
				if($chat->update()) {
					$msg = "Chat transferred to agent ({$new_agent->name}) successfully!";
					redirect_to('chatroom.php?edit=success&msg=' .$msg);
				}
			}
		}
}

if (isset($_POST['send-chat'])) {
		if($_POST['hash'] == $_SESSION[$elhash]){
			//unset($_SESSION[$elhash]);
			$chat_id = $db->escape_value($_POST['chat_id']);
			if(Chat::check_id_existance($chat_id)) {
				$receiver= $db->escape_value($_POST['receiver']);
				$message= $db->escape_value($_POST['message']);
				
				$chat = Chat::get_specific_id($chat_id);
				
				$str= $chat->transcript;
				//$str= str_replace('\\' , '' , $chat->transcript);
				//$str= str_replace('/' , '' , $str);
				
				$msgs = unserialize($str);
				
				$msg = str_replace('\\' , '' , $message);
				if($msg) {
				$msgs[] = Array("sender" => $current_user->id , 
										"receiver" => $receiver, 
										"msg" => $msg,
										"date" => strftime("%Y-%m-%d %H:%M:%S", time()),
										"viewed" => "0" );
				}
				
				$chat->transcript = serialize($msgs);
				$chat->unread_customer = $chat->unread_customer + 1;
				if($chat->ended == "0") { $chat->update(); }
			}
		}
}
?>
<?php require_once('assets/includes/sidebar.php'); ?>
<?php require_once('assets/includes/navbar.php'); ?>
<?php if(isset($_GET['accept']) && is_numeric($_GET['accept']) ) {
	$conv_id = $db->escape_value($_GET['accept']);
	if(Chat::check_id_existance($conv_id)) {
		$conv = Chat::get_specific_id($conv_id);
		if($conv->operator_id == "0") {
			$conv->operator_id = $current_user->id;
			$conv->update();
		} else {
			$user = User::get_specific_id($conv->operator_id);
			$msg = "Sorry, this conversation already accepted by another agent ({$user->name})";
			redirect_to('chatroom.php?edit=fail&msg=' .$msg);
		}
	}
} 

if(isset($_GET['transfer']) && is_numeric($_GET['transfer']) ) {
	$conv_id = $db->escape_value($_GET['transfer']);
	if(Chat::check_id_existance($conv_id)) {
		$conv = Chat::get_specific_id($conv_id);
		if($conv->operator_id == $current_user->id && $conv->transferred_to != "0") {
			$conv->transferred_to = 0;
			$conv->update();
		}
	}
}

$ended = " AND ended = 0 AND offline = 0 ";
if(isset($_GET['archive']) && $_GET['archive'] == true ) {
	$ended = " AND ended = 1  AND offline = 0 ";
}

$conversations = Chat::find_exact($current_user->id, "operator_id" , $ended);

$department= "";
if($current_user->prvlg_group != "1") {
	$department = " AND department = '{$current_user->department_id}' ";
}

?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            
							
							<div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">headset_mic</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Your Chat Room
										<a href="archive.php" class="btn btn-sm btn-flat btn-danger pull-right" style="margin:0"><i class='material-icons'>history</i> Archive</a>
									</h4>
                                    <div class="row">
                                        <div class="col-md-9">
										<?php
										$invitation_mode = false;
										if(isset($_GET['invite']) && is_numeric($_GET['invite']) ) {
											$online_user_id = $db->escape_value($_GET['invite']);
											if(Visitor::check_id_existance($online_user_id)) {
												$online_user = Visitor::get_specific_id($online_user_id);
												$invitation_mode = true;
												if($online_user->invitation != '3' && $online_user->invited_by == '0' ) {
													$online_user->invitation = '1';
													$online_user->invited_by = $current_user->id;
													$online_user->update();
												}
												if($online_user->invitation == '3') {
													$invitation_mode = false;
												}
											}
										}
										
										if(isset($_GET['close-invite']) && is_numeric($_GET['close-invite']) ) {
											$online_user_id = $db->escape_value($_GET['close-invite']);
											if(Visitor::check_id_existance($online_user_id)) {
												$online_user = Visitor::get_specific_id($online_user_id);
												if($online_user->invited_by == $current_user->id ) {
													$online_user->delete();
													$invitation_mode = false;
												}
											}
										}
										?>
										
										
										
										<?php if($conversations) { ?>
											<div class="col-md-4">
												<ul class="nav nav-pills nav-pills-icons nav-pills-rose nav-stacked" id="names-list">
													<?php $i = 1; foreach($conversations as $c) { ?>
														
													<li class="<?php if ($i == 1) { echo "active"; } ?>" id="<?php echo $c->id; ?>">
														<?php echo "<a href='#conv-{$c->id}' data-toggle='tab' aria-expanded='true' class='conv-link' data-chat_id='{$c->id}'>{$c->name}</a>";?>
													</li>
													<?php $i++; } ?>
												</ul>
											</div>
											<div class="col-md-8">
												<div class="tab-content conversations-container">
												
													<?php $i = 1; foreach($conversations as $c) { ?>
													<div class="tab-pane box-shadow <?php if($i == 1) { echo 'active';} ?>" style='padding:20px' id="conv-<?php echo $c->id; ?>">
														<div class="chat-info" data-chat_id="<?php echo $c->id; ?>"  id="conv-<?php echo $c->id; ?>-info">
															<a href="#me" class="btn btn-sm btn-flat btn-danger pull-right end-chat" data-chat_id="<?php echo $c->id; ?>" style="margin:0">End Chat</a>
															<a href="#me" class="btn btn-sm btn-flat btn-danger pull-right transfer-chat" data-chat_id="<?php echo $c->id; ?>" style="margin:0; margin-right:10px">Transfer</a>
															<small>Chatting for: <span class="stopwatch-<?php echo $c->id; ?>">00:00:00</span></small>
															<hr>
														</div>
														<div class="slimscroll chat-container" data-chat_id="<?php echo $c->id; ?>"  id="conv-<?php echo $c->id; ?>-inner">
															<?php echo $c->question; ?>
														</div>
														<form action="chatroom.php" method="POST" class="chatForm">
															<div class="input-group">
																<span class="input-group-addon">
																	<i class="material-icons">keyboard</i>
																</span>
																<div class="form-group label-floating">
																	<label class="control-label">Type and press enter..</label>
																	<input type="text" class="form-control chat-msg" name="message" required>
																</div>
															</div>
																<?php echo "<input type=\"hidden\" name=\"hash\" value=\"".$random_hash."\" readonly/>"; ?>
																<?php echo "<input type=\"hidden\" name=\"chat_id\" value=\"".$c->id."\" readonly/>"; ?>
																<?php echo "<input type=\"hidden\" name=\"receiver\" value=\"".$c->name."\" readonly/>"; ?>
																<?php echo "<input type=\"hidden\" name=\"send-chat\" value=\"true\" readonly/>"; ?>
														</form>
													</div>
													<?php $i++; } ?>
												</div>
											</div>
										<?php } else { ?>
											<h5 class="text-muted text-center"><i class="material-icons" style="font-size:70px">info_outline</i><br>You don't have any active conversations</h5>
										<?php } ?>
                                        </div>
										<div class="col-md-3">
											<br>
											<div id="customer_details">
											</div>
											
											<div id="invite_list">
												<?php $invite_list = Visitor::get_everything(" AND invited_by = {$current_user->id} AND ( invitation = 1 OR invitation = 2 ) "); ?>
												<?php if($invite_list) { ?>
												<h5><center>Invitations List</center></h5>
												<?php foreach($invite_list as $p) {
														echo "<div class='btn-group pull-right' style='margin-top:0px'><a href='chatroom.php?close-invite={$p->id}' class='decline_invite_list btn btn-danger btn-fab btn-fab-mini' style='color:white'  data-chat_id='{$p->id}'><i class='material-icons'>close</i></a></div><div><i class='fa fa-laptop' style='font-size:16px'></i> <b>{$p->ip} ({$p->city})</b>";
														echo "<br>Waiting customer response<br style='clear:both'/><hr></div>";
													}
												} ?>
											</div>
											
											<div id="incoming_list">
												<?php $chat_list = Chat::get_pending_list($department); ?>
												<?php if($chat_list) { 
													foreach($chat_list as $p) {
														echo "<div><i class='material-icons' style='font-size:16px'>call</i> <b>{$p->name} ({$p->email})</b>";
														echo "<div class='btn-group pull-right'><a href='chatroom.php?accept={$p->id}' class='accept_chat btn btn-success btn-fab btn-fab-mini' style='color:white'  data-chat_id='{$p->id}'><i class='material-icons'>check</i></a><a href='#me' class='decline_chat_list btn btn-danger btn-fab btn-fab-mini'  style='color:white' ><i class='material-icons'>close</i></a></div><br style='clear:both'/><hr></div>";
													}
												} else { ?><h5 class="text-muted text-center"><i class="material-icons" style="font-size:70px">info_outline</i><br>You don't have any pending chat requests</h5><?php } ?>
											</div>
                                        </div>
                                    </div>
                                </div>
								<br style="clear:both"><br style="clear:both"><br style="clear:both">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			
            <?php require_once("assets/includes/footer.php"); ?>
	<?php require_once("assets/includes/preloader.php"); ?>
	
<script src="assets/js/jquery.timer.js"></script>
<script src="assets/js/jquery.timer.demo.js"></script>
<script src="assets/js/jquery.select-bootstrap.js" type="text/javascript"></script>


<div class="modal fade" id="transfer-chat" role="dialog" >
  <div class="modal-dialog " role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" style="margin:0; padding:0">Transfer chat to another agent
		<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
		  <span aria-hidden="true"><i class="material-icons">close</i></span>
		</button></h5>
	  </div>
	  
	<form class="form-horizontal" method="POST" action="chatroom.php" enctype="multipart/form-data">
    
	  <div class="modal-body">
				  
				  <div class="row">
					<label class="col-md-3 label-on-left">Department</label>
					<div class="col-md-9">
						<div class="form-group label-floating is-empty">
							<select name="department_id" class="selectpicker form-control" id="department_id">
								<option selected disabled></option>
								<?php $departments = Department::get_everything(""); ?>
								<?php if($departments) {
									foreach($departments as $d) {
										echo "<option value='{$d->id}'>{$d->name}</option>";
								}} ?>
							</select>
						<span class="material-input"></span></div>
					</div>
				</div>
                  <br>
				  <div class="row">
					<label class="col-md-3 label-on-left">Online Agents</label>
					<div class="col-md-9">
						<div class="form-group label-floating is-empty">
							<select name="agent_id" class="selectpicker form-control" id="agent_id" >
								<option selected disabled>No Online Agents!</option>
							</select>
						<span class="material-input"></span></div>
					</div>
				</div>
                  <br>
		<?php 
		echo "<input type=\"hidden\" name=\"hash\" value=\"".$random_hash."\" readonly/>"; 
		echo "<input type=\"hidden\" name=\"chat_id\" id=\"chat_id\" value=\"\" readonly/>";
		?> 
    	
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" name="transfer-chat">Submit</button>
	  </div>
	</form>
	
	</div>
  </div>
</div>
<audio id="buzzer2" src="assets/audio/msg.mp3" type="audio/mp3"></audio>

<script type="text/javascript">
$(".slimscroll").slimscroll({
	height: '500px',
	start: 'bottom',
	alwaysVisible: false,
	railVisible: true,
	size: '7px',
	scrollTo: "bottom"
}).addClass('box-shadow');


$('ul#names-list').on('click' , '.conv-link' ,function() {
	var chat_id = $(this).data('chat_id');
	var container = $("#customer_details");
	$.post("assets/includes/one_ajax.php?type=customer_details", {id:1, data: 1, cur: chat_id , hash:'<?php echo $random_hash; ?>'}, function(response){
		container.html(response);
	});
});

var cur = $("ul#names-list li.active").attr("id");
if(!cur) { cur = 0; }
var container = $("ul#names-list");
var not = $("ul#names-list li.unread").attr("id");
if(!not) { not = 0; }
var ended = '<?php echo $ended; ?>';
$.post("assets/includes/one_ajax.php?type=names-list", {id:1, data:1, cur:cur , notify: not, ended: ended, hash:'<?php echo $random_hash; ?>'}, function(response){
	container.html(response);
});

$('.chat-container').each(function(i, obj) {
	var chat_id = $(this).data('chat_id');
	//var container = $(this);
	var container = $("#conv-"+chat_id+"-inner");
	$.post("assets/includes/one_ajax.php?type=open-chat", {id:1, data: chat_id , cur: cur, hash:'<?php echo $random_hash; ?>'}, function(response){
		container.html(response);
		var scrollTo_val = $("#conv-"+chat_id+"-inner").prop('scrollHeight') + 'px';
		$("#conv-"+chat_id+"-inner").slimScroll({ scrollTo : scrollTo_val }); 
	});
});

$.post("assets/includes/one_ajax.php?type=pending-list", {id: 1 , data: 1,  hash:'<?php echo $random_hash; ?>'}, function(data){
	if(data) {
		$("#incoming_list").html(data);
	}
});
$.post("assets/includes/one_ajax.php?type=invite-list", {id: 1 , data: 1,  hash:'<?php echo $random_hash; ?>'}, function(data){
	if(data) {
		$("#invite_list").html(data);
	}
});

if(cur) {
	var container = $("#customer_details");
	$.post("assets/includes/one_ajax.php?type=customer_details", {id:1, data: 1, cur: cur , hash:'<?php echo $random_hash; ?>'}, function(response){
		container.html(response);
	});
}

setInterval(function() {

var cur = $("ul#names-list li.active").attr("id");
if(!cur) { cur = 0; }
var container = $("ul#names-list");
var not = $("ul#names-list li.unread").attr("id");
if(!not) { not = 0; }
var ended = '<?php echo $ended; ?>';
$.post("assets/includes/one_ajax.php?type=names-list", {id:1, data:1 , cur:cur, notify: not,  ended: ended, hash:'<?php echo $random_hash; ?>'}, function(response){
	if (response.indexOf('<input type="hidden" value="unread">') >= 0) {
		$('#buzzer2').get(0).play();
	}
	container.html(response);
});

$('.chat-container').each(function(i, obj) {
	var chat_id = $(this).data('chat_id');
	var container = $("#conv-"+chat_id+"-inner");
	$.post("assets/includes/one_ajax.php?type=open-chat", {id:1, data: chat_id , cur: cur, hash:'<?php echo $random_hash; ?>'}, function(response){
		container.html(response);
		var scrollTo_val = $("#conv-"+chat_id+"-inner").prop('scrollHeight') + 'px';
		$("#conv-"+chat_id+"-inner").slimScroll({ scrollTo : scrollTo_val }); 
	});
});

$.post("assets/includes/one_ajax.php?type=pending-list", {id: 1 , data: 1,  hash:'<?php echo $random_hash; ?>'}, function(data){
	if(data) {
		$("#incoming_list").html(data);
	}
});
$.post("assets/includes/one_ajax.php?type=invite-list", {id: 1 , data: 1,  hash:'<?php echo $random_hash; ?>'}, function(data){
	if(data) {
		$("#invite_list").html(data);
	}
});


}, 2500);


<?php if($conversations) {
foreach($conversations as $c) { ?>
var Example<?php echo $c->id ?> = new (function() {
    var $stopwatch, // Stopwatch element on the page
        incrementTime = 70, // Timer speed in milliseconds
        currentTime = 0, // Current time in hundredths of a second
        updateTimer = function() {
            $stopwatch.html(formatTime(currentTime));
            currentTime += incrementTime / 10;
        },
        init = function() {
            $stopwatch = $('.stopwatch-<?php echo $c->id; ?>');
            Example<?php echo $c->id ?>.Timer = $.timer(updateTimer, incrementTime, true);
        };
    this.resetStopwatch = function() {
        currentTime = 0;
        this.Timer.stop().once();
    };
    $(init);
});
<?php } } ?>



$( "div.tab-content" ).on( "click", ".end-chat", function() {

	var chat_id = $(this).data('chat_id');
	var chat_btn = $(this);
	
	swal({
		title: 'End Chat?',
		text: 'Are you sure you want to end this chat?',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Yes',
		cancelButtonText: 'No',
		confirmButtonClass: "btn btn-success",
		cancelButtonClass: "btn btn-danger",
		buttonsStyling: false
	}).then(function() {
		$.post("assets/includes/one_ajax.php?type=end-chat", {id: 1, data: chat_id, hash:'<?php echo $random_hash; ?>'}, function(response){
			eval("Example"+chat_id+".Timer.toggle()");
			chat_btn.hide();
		});
	});
	
});

$( "div.tab-content" ).on( "click", ".transfer-chat", function() {

	var chat_id = $(this).data('chat_id');
	$("#chat_id").val(chat_id);
	$("#transfer-chat").modal("toggle");
	
});


$('select#department_id').on("change", function() {
		var department_id = $(this).val();
		$.post("assets/includes/one_ajax.php?type=agents-list", {id:1, data: department_id , hash:'<?php echo $random_hash; ?>'}, function(data){
			$("select#agent_id").html(data);
			$(".selectpicker").selectpicker('refresh');
		});
});

$(document).ready(function() {
	
	$('.chatForm').ajaxForm(function() {
		$(".chat-msg").val('');
		var data = JSON.parse('{"' + decodeURI($(this)[0]['data'].replace(/&/g, "\",\"").replace(/=/g,"\":\"")) + '"}');
		var chat_id = data['chat_id'];
		var container = $("#conv-"+chat_id+"-inner");
		var cur = $("ul#names-list li.active").attr("id");
		$.post("assets/includes/one_ajax.php?type=open-chat", {id:1, data: chat_id , cur:cur, hash:'<?php echo $random_hash; ?>'}, function(response){
		container.html(response);
		var scrollTo_val = $("#conv-"+chat_id+"-inner").prop('scrollHeight') + 'px';
		$("#conv-"+chat_id+"-inner").slimScroll({ scrollTo : scrollTo_val }); 
		});
	});

});

<?php if($invitation_mode == true) { ?>

setInterval(function() {
$.post("assets/includes/one_ajax.php?type=check-invite", {id: 1 , data: <?php echo $online_user->id; ?>,  hash:'<?php echo $random_hash; ?>'}, function(data){
	if(data == 'accepted') {
		location.reload();
	}
});
}, 2500);
	
<?php } ?>

</script>

</html>