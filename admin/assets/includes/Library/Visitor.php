<?php
require_once("Loader.php");

class Visitor Extends OneClass {
	
	public static $table_name = "online_users";
	public static $db_fields = array( "id" , "session" , "time" , "ip" , "country" , "city" , "address" , "invitation" , "invited_by" , "current_page" , "chat_id");
	
	public $id;
	public $session;
	public $time;
	public $ip;
	public $country;
	public $city;
	public $address;
	public $current_page;
	public $invitation;
	public $invited_by;
	public $chat_id;
	
	public static function update_session($session,$page="") {
		global $db;
		$page = $db->escape_value($page);
		$sql = "UPDATE ". DBTP. static::$table_name ." SET ";
		$sql .= " time = ". time() . ", current_page = '{$page}' WHERE ";
		$sql .= " session = '". $session . "' ";
		$db->query($sql);
		if($db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	
	public static function delete_session($time) {
		global $db;
		$sql = "DELETE FROM ". DBTP. static::$table_name ." WHERE ";
		$sql .= " time < '". $time . "' ";
		$db->query($sql);
		if($db->affected_rows()) {
			return true;
		} else {
			return false;
		}
	}
	
}
	
?>