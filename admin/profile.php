<?php require_once('assets/includes/header.php'); ?>

<?php 
if (isset($_POST['edit_obj'])) {
	
		if(!$current_user->can_see_this("profile.update",$group)) {
			$msg = "You don't have required permissions to do this action, please contact system admins";
			redirect_to("profile.php?edit=fail&msg={$msg}");
		}
		
		if($_POST['hash'] == $_SESSION[$elhash]){
			//unset($_SESSION[$elhash]);
			$edit_id = $db->escape_value($_POST["edit_id"]);
			
			if(isset($_POST['edit_id']) && $current_user->prvlg_group == "1" ) {
				if(User::check_id_existance($db->escape_value($_POST['edit_id']))) {
					$edited_entry = User::get_specific_id($db->escape_value($_POST['edit_id']));
				} else {
					$edited_entry = $current_user;
				}
				
			} else {
				$edited_entry = $current_user;
			}
			
			if(!User::check_id_existance($edit_id)) {
				$msg = "Unable to update data, User not found! please try again";
				redirect_to("profile.php?edit=fail&msg={$msg}&id={$edit_id}");
			}
			
			$db_fields = Array('name','mobile', 'address' , 'position' ,'education' , 'about' , 'avatar');
			
			$upload_key = array_search('avatar' , $db_fields);
			if($upload_key) {
				unset($db_fields[$upload_key]);
				$upload_present = true;
			}
			
			foreach($db_fields as $field) {
				if(isset($_POST[$field])) {
					$$field = $db->escape_value($_POST[$field]);
				}
			}
			
			foreach($db_fields as $field) {
				if(isset($$field)) {
					$edited_entry->$field = $$field;
				}
			}
			
			$password = $db->escape_value($_POST['password']);
			
			if(isset($_POST['old_password']) && $_POST['old_password'] != '' ) {
				$old_password = $db->escape_value($_POST['old_password']);
				$current_password = $edited_entry->password;
				$phpass = new PasswordHash(8, true);
				
				if(!$phpass->CheckPassword($old_password, $current_password)) {
					
					$msg = "Invalid Password. Please try again";
					redirect_to("profile.php?edit=fail&msg={$msg}&id={$edit_id}");
				}
				
			} else {
				$msg = "Please enter your current password to continue editing your profile";
				redirect_to("profile.php?edit=fail&msg={$msg}&id={$edit_id}");
			}
			
			
			
			
			
			/*if($current_user->prvlg_group == "1" ) {
				$prvlg_group = $db->escape_value($_POST['prvlg_group']);
				$edited_entry->prvlg_group = $prvlg_group;
			}*/
			
			if($current_user->can_see_this('profile.change_email' , $group) ) {
			$email = $db->escape_value($_POST['email']);
			
			$current_email = $edited_entry->email;
			$email_exists = User::check_existance_except("email", $email , $edited_entry->id);
			
			if($email_exists) {
				$msg = "Email already exists in database! please try again";
				redirect_to("profile.php?edit=fail&msg={$msg}&id={$edit_id}");
			}
			
			if($email != '' && $email != $current_email) {
			$edited_entry->email = $email;
			}
			}
			
			if($current_user->can_see_this('profile.change_username' , $group) ) {
				$username = $db->escape_value($_POST['username']);
				
				$current_username = $edited_entry->username;
				$username_exists = User::check_existance_except("username", $username , $edited_entry->id);
				
				if($username_exists) {
					$msg = "Username already exists in database! please try again";
					redirect_to("profile.php?edit=fail&msg={$msg}&id={$edit_id}");
					
				}
			
				if($username != '' && $username != $current_username) {
					$edited_entry->username = $username;
				}
			}
			
			
			if($current_user->can_see_this('profile.change_password' , $group) ) {
				$current_password = $edited_entry->password;
				if($password !='' && $password != $current_password ) {
				$phpass = new PasswordHash(8, true);
				$hashedpassword = $phpass->HashPassword($password);
				
				$edited_entry->password = $hashedpassword;
				}
			}
			
			if(isset($upload_present) && $upload_present == true && isset($_FILES['avatar']) ) {
				$files = '';
				$f = 0;
				$images = array();
				$num_pics = 1;
				$target = $_FILES['avatar'];
				$crop_arr = $_POST['cropped'];
				$crop = json_decode($crop_arr , true);
				$upload_problems = 0;
				for ($f ; $f < $num_pics ; $f++) :
					$file = "file";
					$string = $$file . "{$f}";
					$$string = new File();	
						if(!empty($_FILES['avatar']['name'][$f])) {
							$$string->attach_file($_FILES['avatar'], $f);
							if ($$string->save($crop)) {
								$images[$f] = $$string->id;
							} else {
								$upl_msg = "Cannot upload profile picture, please try again";
								$upl_msg .= join("<br />" , $$string->errors);							
								$upload_problems = 1;
							}
						}
				endfor;
				
				if(!empty($images)) {
					$final_string = implode("," , $images);
					//if($edited_entry->files != NULL) {
						//$edited_entry->files .= ",". $final_string;
					//} else {
						//$edited_entry->files .= $final_string;
					//}
					$edited_entry->avatar = $final_string;
				}
			}
			
			if ($edited_entry->update()) {
				
					$msg = "Data updated successfully";
					if(isset($upload_problems) && $upload_problems == '1' ) { $msg .= $upl_msg; }
					redirect_to("profile.php?edit=success&msg={$msg}&id={$edit_id}");
					
				} else {
					$msg = 'Unable to update data, please try again';
					redirect_to("profile.php?edit=fail&msg={$msg}&id={$edit_id}");
				}
			
		
			} else {
				$msg = "Authorization failed, please try again";
				redirect_to("profile.php?edit=fail&msg={$msg}&id={$edit_id}");
			}
}






$edit_mode = false;

if (isset($_GET['type']) && isset($_GET['id']) && is_numeric($_GET['id']) && isset($_GET['hash'])) {
			$type = $db->escape_value($_GET['type']);
			$id = $db->escape_value($_GET['id']);
			$recieved_hash = $db->escape_value($_GET['hash']);
			
			if(!User::check_id_existance($id)) {
				redirect_to("profile.php");
			}
			
			$this_obj = User::get_specific_id($id);
			
			if ($_SESSION[$elhash] == $recieved_hash) {
				//unset($_SESSION[$elhash]);
				switch($type) {
					
					case 'edit' :
						if(!$current_user->can_see_this("profile.update",$group)) {
							$msg = "You don't have required permissions to do this action, please contact system admins";
							redirect_to("profile.php?edit=fail&msg={$msg}");
						}
						$edit_mode = true;
						
						$user = User::get_specific_id($id);
						if($user->avatar) {
							$img = File::get_specific_id($user->avatar);
							$quser_avatar = WEB_LINK."assets/".$img->image_path();
							$user_avatar_path = UPLOADPATH."/".$img->image_path();
							if (!file_exists($user_avatar_path)) {
								$quser_avatar = WEB_LINK.'assets/img/operator.png';
							}
						} else {
							$quser_avatar = WEB_LINK.'assets/img/operator.png';
						}

						
					break;
				
				}
			} else {
				$msg = "Authorization failed, please try again";
				redirect_to("profile.php?edit=fail&msg={$msg}");
			}
}
?>

<?php require_once('assets/includes/sidebar.php'); ?>
<?php require_once('assets/includes/navbar.php'); ?>
<?php 
	
//if(isset($_GET['id']) && $current_user->prvlg_group == "1" ) {
if(isset($_GET['id'])) {
	if(User::check_id_existance($db->escape_value($_GET['id']))) {
		$user = User::get_specific_id($db->escape_value($_GET['id'])); //Needs More Privilege Control
	} else {
		$user = $current_user;
	}	
} else {
	$user = $current_user;
}

	
	
	$prvlg_group = Group::get_specific_id($user->prvlg_group);
	$department_name = "";
	
	if($user->department_id) {
		$department = Department::get_specific_id($user->department_id);
		if($department) {
			$department_name = $department->name;
		}
	}
	
?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">perm_identity</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">View Profile -
                                        <small class="category"><?php echo $user->name; ?></small>
                                    </h4>
									
                                        <div class="row">
                                            <div class="col-md-5">
                                                <div class="form-group ">
                                                    <label class="control-label">Privileges</label>
                                                    <input type="text" class="form-control" value="<?php echo $prvlg_group->name; ?>" placeholder="N/A" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-3">
                                                <div class="form-group ">
                                                    <label class="control-label">Username</label>
													<input type="text" class="form-control" value="<?php echo $user->username; ?>" placeholder="N/A" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group ">
                                                    <label class="control-label">Email address</label>
                                                    <input type="text" class="form-control" value="<?php echo $user->email; ?>" placeholder="N/A" readonly>
                                                </div>
                                            </div>
                                        </div>
										
										
										
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group ">
                                                    <label class="control-label">Name</label>
                                                    <input type="text" class="form-control" value="<?php echo $user->name; ?>" placeholder="N/A" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group ">
                                                    <label class="control-label">Position</label>
                                                    <input type="text" class="form-control" value="<?php echo $user->position; ?>" placeholder="N/A" readonly>
                                                </div>
                                            </div>
                                        </div>
										
										
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group ">
                                                    <label class="control-label">Adress</label>
                                                    <input type="text" class="form-control" value="<?php echo $user->address; ?>" placeholder="N/A" readonly>
                                                </div>
                                            </div>
                                        </div>
										
                                        <div class="row">
											<div class="col-md-6">
                                                <div class="form-group ">
                                                    <label class="control-label">Department</label>
                                                    <input type="text" class="form-control" value="<?php echo $department_name; ?>"  placeholder="N/A" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group ">
                                                    <label class="control-label">Education</label>
                                                    <input type="text" class="form-control" value="<?php echo $user->education; ?>"  placeholder="N/A" readonly>
                                                </div>
                                            </div>
                                        </div>
										<br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <div class="form-group ">
														<label>About Me</label>
                                                        <textarea class="form-control" rows="" readonly><?php echo $user->about; ?></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
										<?php if($user->id == $current_user->id || $current_user->prvlg_group == "1") { ?>
										<a href="profile.php?id=<?php echo $user->id ?>&type=edit&hash=<?php echo $random_hash; ?>" class="btn btn-rose pull-right">Update Profile</a>
										<?php } ?>
                                        <div class="clearfix"></div>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card card-profile">
                                <div class="card-avatar">
                                    <a href="#me">
                                        <img class="img" src="<?php echo $user->get_avatar(); ?>" />
                                    </a>
                                </div>
                                <div class="card-content">
                                    <h6 class="category text-gray"><?php echo $user->position; ?></h6>
                                    <h4 class="card-title"><?php echo $user->name; ?></h4>
									<?php 
									$rating = Chat::sum_all("rating" , "operator_id" , $user->id);
									$rating_total = Chat::count_everything(" AND operator_id= '{$user->id}' AND rating != 0 ");
									if($rating_total) {
										$stars = ceil($rating/$rating_total);
									} else {
										$stars = 0;
									}
									for($i = 1 ; $i <= 5 ; $i++) {
										echo "<i class='material-icons ";
											if($i <= $stars) { echo ' text-warning'; } else { echo ' text-muted'; }
										echo "'>star</i>";
									} ?>
									<br><?php echo Chat::count_everything(" AND operator_id= '{$user->id}' "); ?> Conversations
                                    <p class="description">
                                        <?php if($user->about) {echo  "<hr>" . $user->about; } ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once("assets/includes/footer.php"); ?>
	<?php require_once("assets/includes/preloader.php"); ?>


	
<?php if($edit_mode) { ?>
<?php if($user->id == $current_user->id || $current_user->prvlg_group == "1") { ?>
<div class="modal fade" id="edit_entry" role="dialog" >
  <div class="modal-dialog " role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" style="margin:0; padding:0">Edit Agent (<?php echo $this_obj->name; ?>)
		<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
		  <span aria-hidden="true"><i class="material-icons">close</i></span>
		</button></h5>
	  </div>
	  
	<form class="form-horizontal" method="POST" action="profile.php" enctype="multipart/form-data">
		<div class="modal-body">
				<div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" value="<?php echo $user->name; ?>" required >
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPhone" name="mobile" placeholder="Phone"  value="<?php echo $user->mobile; ?>" required>
                    </div>
                  </div>
				  
				  <div class="form-group">
                    <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="inputAddress" name="address" placeholder="Address" ><?php echo $user->address; ?></textarea>
                    </div>
                  </div>
				  
				  <div class="form-group">
                    <label for="inputPosition" class="col-sm-2 control-label">Position</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPosition" name="position" placeholder="Position" value="<?php echo $user->position; ?>" >
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputEducation" class="col-sm-2 control-label">Education</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEducation" name="education" placeholder="Education" value="<?php echo $user->education; ?>" >
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputAbout" class="col-sm-2 control-label">About User</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="inputAbout" name="about" placeholder="About User" ><?php echo $user->about; ?></textarea>
                    </div>
                  </div>
				  
				  
				  <hr>
				  
				  
				  <div class="form-group">
                    <label for="inputProfilePic" class="col-sm-2 control-label">Profile Picture</label>
                    <div class="col-sm-10">
						
						<div style=""><img src="<?php echo $quser_avatar; ?>" class="" style="float:left; padding:5px; margin-right:10px; max-width:100%; height:350px" id="img2"></div>
						
						<br style='clear:both'>
						<div style="height:64px; padding-top: 12px;width:200px;float:left">
							<input class="text-input " type="file" name="avatar[]" id="img2_upl"/><br/>
						</div>
						
						
                    </div>
                  </div>
                  
				  <hr>
				  
				  <div class="form-group">
                    <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputUsername" placeholder="Username" name="username" value="<?php echo $user->username; ?>" <?php if(!$current_user->can_see_this('profile.change_username' , $group) ) {  echo "readonly"; } ?> >
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email"  value="<?php echo $user->email ?>" required <?php if(!$current_user->can_see_this('profile.change_email' , $group) ) {  echo "readonly"; } ?>>
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputPassword" class="col-sm-2 control-label">Current Password</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPassword" name="old_password" placeholder=""  value="" required>
                    </div>
                  </div>
					<?php if($current_user->can_see_this('profile.change_password' , $group) ) { ?>
					<div class="form-group">
						<label for="inputPassword" class="col-sm-2 control-label">Change Password</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="inputPassword" name="password" placeholder="Unchanged"  value="" >
						</div>
				  </div>
					<?php } ?>
		
		
		<?php echo "<input type=\"hidden\" name=\"hash\" value=\"".$random_hash."\" readonly/>"; ?> 
		<?php echo "<input type=\"hidden\" name=\"edit_id\" value=\"".$user->id."\" readonly/>"; ?> 
    	<?php echo "<input type=\"hidden\" name=\"cropped\" id=\"cropped\" value=\"\" readonly/>"; ?>
		
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" name="edit_obj">Submit</button>
	  </div>
	</form>
	
	</div>
  </div>
</div>
<?php } ?>
<?php } ?>

<script src="assets/js/cropper/cropper.min.js"></script>
<script src="assets/js/jquery.select-bootstrap.js" type="text/javascript"></script>
	
<script type="text/javascript">

function readURL(input,targetid) {
if (input.files && input.files[0]) {
	var reader = new FileReader();
	reader.onload = function (e) {
		
		var height;
		var width;
		var parent_width = $("#" + targetid).parent('div').parent('div').css('width').replace(/[^-\d\.]/g, '') - 50;
		
		var _URL = window.URL || window.webkitURL;

		$("#" + targetid).attr('src', e.target.result);
		$("#" + targetid).on('load', function () {
		
		img = new Image();
		img.src = _URL.createObjectURL(input.files[0]);
		img.onload = function () {
			height= this.height;
			if(this.width < parent_width) {
				width= this.width;
			} else {
				width= parent_width;
			}
			
		$("#" + targetid).cropper('destroy');
		$("#" + targetid).parent('div').css('width', width);
		$("#" + targetid).parent('div').css('height', height);
		
		
		$("#" + targetid).cropper({
		  minContainerHeight: height,
		  minContainerWidth: width,
		  aspectRatio: 1/1,
		  crop: function(e) {
			var croppedData = '{"x":"'+ e.x +'","y":"'+ e.y +'","width":"' + e.width + '","height":"' + e.height + '" }';
			$('#cropped').val(croppedData);
		  }
		});
		
		};
		
		});
	}
	reader.readAsDataURL(input.files[0]);
}
}

$("#img1_upl").change(function(){
	readURL(this, 'img1');
});


<?php if(isset($edit_mode) && $edit_mode == "true") { ?>
		//$(window).load(function(){
		$('#edit_entry').modal('show');
		$("#img2_upl").change(function(){
			readURL(this, 'img2');
		});

		//});
<?php } ?>
</script>

</html>