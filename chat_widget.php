<?php require_once("admin/assets/includes/Library/Loader.php");

$cur_step = 1;
$chat_id  = 0;

$session = session_id();
$currently_online = Visitor::get_everything(" AND session = '{$session}' ORDER BY time DESC LIMIT 1 ");
if($currently_online) {
	foreach($currently_online as $co) {
		if($co->chat_id) { $chat_id = $co->chat_id; $cur_step = 2; }
	}
}

if (isset($_POST['send-chat'])) {
		if($_POST['hash'] == $_SESSION[$elhash_login]){
			//unset($_SESSION[$elhash]);
			$chat_id = $db->escape_value($_POST['chat_id']);
			if(Chat::check_id_existance($chat_id)) {
				$receiver= $db->escape_value($_POST['receiver']);
				$message= $db->escape_value($_POST['message']);
				
				$chat = Chat::get_specific_id($chat_id);
				
				$str= $chat->transcript;
				//$str= str_replace('\\' , '' , $chat->transcript);
				//$str= str_replace('/' , '' , $str);
				$msgs = unserialize($str);
				
				$msg = str_replace('\\' , '' , $message);
				if($msg) {
				$msgs[] = Array("sender" => $chat->name , 
										"receiver" => $receiver, 
										"msg" => $msg,
										"date" => strftime("%Y-%m-%d %H:%M:%S", time()),
										"viewed" => "0" );
				}
				
				$chat->transcript = serialize($msgs);
				$chat->unread_operator = $chat->unread_operator + 1;
				if($chat->ended == "0") { $chat->update(); }
				$cur_step = 2;
			}
		}
}

if (isset($_POST['enter_chat']) && $chat_id == 0 ) {
	if($_POST['hash'] == $_SESSION[$elhash_login]){
		
		//if(isset($_POST['g-recaptcha-response'])) {
          //$captcha=$_POST['g-recaptcha-response'];

        /*if(!$captcha){
			$msg = "Incorrect captcha! please try again";
			redirect_to('chat_widget.php?edit=fail&msg=' .$msg);
        }
        $response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret={$captcha_info['secret']}&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
		
		if($response['success'] == false){
			$msg = "Incorrect captcha! please try again";
			redirect_to('chat_widget.php?edit=fail&msg=' .$msg);
        } else {
			//CORRECT*/
			$name= $db->escape_value($_POST['name']);
			$email= $db->escape_value($_POST['email']);
			$department= $db->escape_value($_POST['department']);
			$question= $db->escape_value($_POST['question']);
			
			$str= str_replace('\\' , '' , $question);
			//$str= str_replace('/' , '' , $str);
			
		if($str) {
			$arr = array();
			$arr['sender'] = $name;
			$arr['receiver'] = '';
			$arr['msg'] = nl2br($str);
			$arr['date'] = strftime("%Y-%m-%d %H:%M:%S" , time());
			$arr['viewed'] = '0';
			
			$arr2 = array();
			$arr2[] = $arr;

			$chat = new Chat();
			
			$chat->name = $name;
			$chat->email = $email;
			$chat->department = $department;
			$chat->question = $question;
			$chat->transcript = serialize($arr2);
			$chat->started_at = strftime("%Y-%m-%d %H:%M:%S" , time());
			$chat->unread_operator = 1;
			$chat->ip = ip_info("visitor", "ip");
			
			if(isset($_POST['invited_by']) && is_numeric($_POST['invited_by']) ) {
				$chat->operator_id = $db->escape_value($_POST['invited_by']);
			}
			
			if(isset($_POST['invite_id']) && is_numeric($_POST['invite_id']) ) {
				$sess = Visitor::get_specific_id($_POST['invite_id']);
				if($sess) { $sess->invitation = '3'; $sess->update(); $chat->invite_id = $sess->id; }
			}
			
			$on = User::get_online_agents($department);
			if(!$on) {
				$chat->offline = 1;
			}
			
			if($chat->create()) {
				$cur_step = 2;
				$chat_id  = $chat->id;
				$session = session_id();
				$currently_online = Visitor::get_everything(" AND session = '{$session}' ");
				if($currently_online) {
					foreach($currently_online as $co) {
						$co->chat_id = $chat_id;
						$co->update();
					}
				}
				
				if(!$on) {
					$cur_step = 3;
				}
			}
		}
		
		/*}
		
		} else {
			//NO CAPTCHA
			$msg = "Incorrect captcha! please try again";
			redirect_to('chat_widget.php?edit=fail&msg=' .$msg);
		}*/
		
	} else {
		$msg = "Authentication error, please try again";
		redirect_to('chat_widget.php?edit=fail&msg=' .$msg);
	}
}


if(isset($_SESSION[$elhash_login]) && $_SESSION[$elhash_login] != "") { 
	$random_hash = $_SESSION[$elhash_login];
} else {
	$random_hash = uniqid();
	$_SESSION[$elhash_login] = $random_hash;
}

?><!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="admin/assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>iChat | Live Support System</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="admin/assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="admin/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
    
    <div class="wrapper ">
        <div class="slimscroll-widget" >
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content" >
                <div class="container" style="padding:0px; margin:0">
                    <div class="row">
						
						
						
						<div class="col-md-10 col-md-offset-1">
                        <div class="card card-signup" style="margin:0">
                            
							<?php if($cur_step == 1) { 
							
							$invite_mode = false;
							$session = session_id();			
							//Current Session
							$currently_online = Visitor::get_everything(" AND session = '{$session}' ORDER BY id DESC LIMIT 1");
							//If count is 0 , then enter the values
							if($currently_online) {
								foreach($currently_online as $sess) {
									if($sess->invitation != '0' && $sess->invited_by != '0' ) {
										$invited_by = User::get_specific_id($sess->invited_by);
										$dep = Department::get_specific_id($invited_by->department_id);
										$invite_mode = true;
									}
								}
							}
							
							?>
								
								
								
								<h3 class="card-title text-center <?php if($invite_mode) { echo 'hide'; } ?>"><i class="material-icons">chat</i> Chat with us!</h3>
								
								<div class="card-content">
								
									<?php if($invite_mode) { ?>
									<div class="alert">
										<h4><img src="admin/assets/img/chat.png" style='width:56px; float:left'> <center><?php echo greeting() . '<br><b style="font-size: 1.1em">'. $invited_by->name."</b><br>invites you to chat!"; ?></center></h4>
									</div>
									<?php
									}
									if (isset($_GET['edit']) && isset($_GET['msg']) && $_GET['edit'] == "success") :
									$status_msg = $db->escape_value($_GET['msg']);				
								?>
									<div class="alert alert-success">
										<i class="fa fa-check"></i> <strong>Success!</strong>&nbsp;&nbsp;<?php echo $status_msg; ?>
									</div>
								<?php
									endif; 	
									if (isset($_GET['edit']) && isset($_GET['msg']) && $_GET['edit'] == "fail") :
									$status_msg = $db->escape_value($_GET['msg']);		
								?>
									<div class="alert alert-danger">
										<i class="fa fa-times"></i> <strong>Error!</strong>&nbsp;&nbsp;<?php echo $status_msg; ?>
									</div>
									
								<?php 
									endif;
								?>
									
									
									
                                
                                    <form class="form-horizontal" method="POST" action="chat_widget.php">
                                        <div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group label-floating is-empty">
                                                    <label class="control-label"></label>
                                                    <input type="text" class="form-control" name="name" required placeholder="Your Name">
                                                <span class="material-input"></span></div>
                                            </div>
                                        </div>
                                        
										<div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group label-floating is-empty">
                                                    <label class="control-label"></label>
                                                    <input type="email" class="form-control" name="email" required placeholder="Your Email">
                                                <span class="material-input"></span></div>
                                            </div>
                                        </div>
										<div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group label-floating is-empty">
                                                    <select name="department" class="selectpicker form-control">
														<option disabled selected>Department</option>
														<?php $departments = Department::get_everything(" AND public = 1 AND deleted = 0 "); ?>
														<?php if($departments) {
															foreach($departments as $d) {
																$on = User::get_online_agents($d->id);
																if($on) {
																	$status = "(Online)";
																} else {
																	$status = "(Offline)";
																}
																echo "<option value='{$d->id}'>{$d->name} {$status}</option>";
														}} ?>
													</select>
                                                <span class="material-input"></span></div>
                                            </div>
                                        </div>
										<div class="row">
                                            <div class="col-md-9">
                                                <div class="form-group label-floating is-empty">
                                                    <textarea class="form-control" rows="3" required name="question" placeholder="Your Question"></textarea>
                                                <span class="material-input"></span></div>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
										        <div class="form-group form-button">
                                                    <center><button type="submit" class="btn btn-fill btn-rose" name="enter_chat">Chat Now</button></center>
                                                </div>
                                            
                                        </div>
										<?php if($invite_mode) { echo "<input type=\"hidden\" name=\"invited_by\" value=\"".$invited_by->id."\" readonly/>";
										echo "<input type=\"hidden\" name=\"invite_id\" value=\"".$sess->id."\" readonly/>"; } ?>
										<?php echo "<input type=\"hidden\" name=\"hash\" value=\"".$random_hash."\" readonly/>"; ?> 
                                    </form>
								</div>
								
								<?php } elseif($cur_step == 2) { ?>
								<?php $chat = Chat::get_specific_id($chat_id); ?>
								<h5 class="card-title">
									<p class="pull-left" style="padding-left:20px">
										<i class="material-icons" style="font-size:18px">face</i> Welcome, <?php echo $chat->name; ?>! <a href="#me" class="btn btn-xs btn-flat btn-danger" id="end-chat">End</a>
									</p>
								</h5>
								<br style="clear:both">
								<div class="card-content" style="padding:0">
										<div class="box-shadow">
											<div class="slimscroll" id="conv-<?php echo $chat->id ?>-inner">
											</div>
											<?php echo '<form action="chat_widget.php" method="POST" id="chat-form" style="display:none"><div class="input-group"><span class="input-group-addon"><i class="material-icons">keyboard</i></span><div class="form-group label-floating" style="margin: 0" ><label class="control-label" style="">Type and press enter..</label><input type="text" class="form-control chat-msg" name="message" required></div></div>'; ?>
											<div id="chat-params"></div>
											<?php echo "</form>";?>
										</div>
									
									
								</div>
								
								<?php } elseif($cur_step == "3") { ?>
									
									
									<?php $chat = Chat::get_specific_id($chat_id); ?>
								<h4 class="card-title">
									<p class="pull-left" style="padding-left:20px">
										<i class="material-icons" style="font-size:18px">face</i> Welcome, <?php echo $chat->name; ?>!
									</p>
									
								</h4>
								<br style="clear:both">
								<div class="card-content" style="padding:0;">
									
										<h5><center><img src="admin/assets/img/operator_offline.png" style="width:220px"><br><br>Well, seems like all our agents are offline<br>but we've got your message! one of our support team will contact you on the next 24 hours on (<?php echo $chat->email; ?>)<br>Thanks for contacting us!
										<br><br><a href="#me" class="btn btn-sm btn-flat btn-primary close-window">Close Window</a></center></h5>
										
								</div>
									
								<?php } ?>
                        </div>
                    </div>
						
						
						
                    </div>
                </div>
            </div>
            
        </div>
    </div>
<audio id="buzzer2" src="admin/assets/audio/msg.mp3" type="audio/mp3"></audio>
</body>


<!--   Core JS Files   -->
<script src="admin/assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="admin/assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="admin/assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="admin/assets/js/material.min.js" type="text/javascript"></script>
<script src="admin/assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="admin/assets/js/jquery.slimscroll.js"></script>
<script src="admin/assets/js/jquery.sharrre.js" type="text/javascript"></script>
<script src="admin/assets/js/sweetalert2.js" type="text/javascript"></script>
<script src="admin/assets/js/jquery.select-bootstrap.js" type="text/javascript"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="admin/assets/js/jquery.form.min.js" type="text/javascript"></script>
<script src="admin/assets/js/jquery.emotions.public.js" type="text/javascript"></script>
<!-- Material Dashboard javascript methods -->
<script src="admin/assets/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="admin/assets/js/demo.js"></script>
<script type="text/javascript">
$().ready(function() {
	demo.checkFullPageBackgroundImage();
	
	$(".slimscroll-widget").slimscroll({
		height: '400px',
		alwaysVisible: false,
		railVisible: true,
		size: '7px',
		scrollTo: "bottom"
	}).addClass('box-shadow');
	
	
	$(".slimscroll").slimscroll({
		height: '300px',
		start: 'bottom',
		alwaysVisible: false,
		railVisible: true,
		size: '7px',
		scrollTo: "bottom"
	}).addClass('box-shadow');
		
		
var chat_id = '<?php echo $chat_id; ?>';
var container = $("#conv-"+chat_id+"-inner");
$.post("admin/assets/includes/public_ajax.php?type=chat", {id:1, data: chat_id, hash:'<?php echo $random_hash; ?>'}, function(response){
	if (response.indexOf('<input type="hidden" value="unread">') >= 0) {
		$('#buzzer2').get(0).play();
	}
	container.html(response);
	var scrollTo_val = $("#conv-"+chat_id+"-inner").prop('scrollHeight') + 'px';
	$("#conv-"+chat_id+"-inner").slimScroll({ scrollTo : scrollTo_val }); 
});

$.post("admin/assets/includes/public_ajax.php?type=set_online", {id:1, data: '<?php echo basename($_SERVER['HTTP_REFERER']); ?>', hash:'<?php echo $random_hash; ?>'}, function(){});

setInterval(function() {
	
var chat_id = '<?php echo $chat_id; ?>';
var container = $("#conv-"+chat_id+"-inner");
$.post("admin/assets/includes/public_ajax.php?type=chat", {id:1, data: chat_id, hash:'<?php echo $random_hash; ?>'}, function(response){
	if (response.indexOf('<input type="hidden" value="unread">') >= 0) {
		$('#buzzer2').get(0).play();
	}
	if (response.indexOf('<input type="hidden" value="answered">') >= 0 && $("#chat-params").is(':empty' )) {
		$.post("admin/assets/includes/public_ajax.php?type=reply-box", {id:1, data: chat_id, hash:'<?php echo $random_hash; ?>'}, function(response2){
			$("#chat-params").html(response2);
			$("#chat-form").show();
			$.post("admin/assets/includes/public_ajax.php?type=agent-info", {id:1, data: chat_id, hash:'<?php echo $random_hash; ?>'}, function(response3){
				$("#agent-info").html(response3);
			});
			$.post("admin/assets/includes/public_ajax.php?type=chat-timer", {id:1, data: chat_id, hash:'<?php echo $random_hash; ?>'}, function(response4){
				$("#chat-timer").html(response4);
			});
		});
	}
	
	if (response.indexOf('<input type="hidden" value="transferred">') >= 0) {
		$.post("admin/assets/includes/public_ajax.php?type=agent-info", {id:1, data: chat_id, hash:'<?php echo $random_hash; ?>'}, function(response3){
			$("#agent-info").html(response3);
		});
	}
	
	container.html(response);
	var scrollTo_val = $("#conv-"+chat_id+"-inner").prop('scrollHeight') + 'px';
	$("#conv-"+chat_id+"-inner").slimScroll({ scrollTo : scrollTo_val }); 
});

$.post("admin/assets/includes/public_ajax.php?type=set_online", {id:1, data: '<?php echo basename($_SERVER['HTTP_REFERER']); ?>', hash:'<?php echo $random_hash; ?>'}, function(){});

$.post("admin/assets/includes/public_ajax.php?type=check_invitation", {id:1, data: 1, hash:'<?php echo $random_hash; ?>'}, function(data){
	if(data == 'invited') {
		window.parent.$('.chat-btn').animate({bottom: '-250px'});
		window.parent.$('.ichat-widget').animate({bottom: '0px'});
		window.parent.$("iframe#widgetiframe").attr("src", "ichat/chat_widget.php");
		$('#buzzer2').get(0).play();
	}
});	

}, 2500);
	
});

$('#agent-info').on('click' , '.rate' , function() {
	var stars = $(this).data('stars');
	var chat_id = '<?php echo $chat_id; ?>';
	$.post("admin/assets/includes/public_ajax.php?type=rate", {id: stars, data: chat_id, hash:'<?php echo $random_hash; ?>'}, function(response){
		$("#rate_me").html(response);
	});
});

$('a#end-chat').click(function() {
	
	var chat_id = '<?php echo $chat_id; ?>';
	
	swal({
		title: 'End Chat?',
		text: 'Are you sure you want to end this chat?',
		type: 'warning',
		showCancelButton: true,
		confirmButtonText: 'Yes',
		cancelButtonText: 'No',
		confirmButtonClass: "btn btn-success",
		cancelButtonClass: "btn btn-danger",
		buttonsStyling: false
	}).then(function() {
		$.post("admin/assets/includes/public_ajax.php?type=end-chat", {id: 1, data: chat_id, hash:'<?php echo $random_hash; ?>'}, function(response){
			window.parent.$(".ichat-widget").animate({bottom: '-500px'});
			window.parent.$(".chat-btn").animate({bottom: '0px'});
			window.parent.$("iframe#widgetiframe").attr("src", "ichat/chat_widget.php");
		});		
	});
	
});

$( "div.card-signup" ).on( "click", ".close-window", function() {
		window.parent.$(".ichat-widget").animate({bottom: '-500px'});
		window.parent.$(".chat-btn").animate({bottom: '0px'});
		window.parent.$("iframe#widgetiframe").attr("src", "ichat/chat_widget.php");
});


$(document).ready(function() {
	$('#chat-form').ajaxForm(function() {
		$(".chat-msg").val('');
		var chat_id = '<?php echo $chat_id; ?>';
		var container = $("#conv-"+chat_id+"-inner");
		$.post("admin/assets/includes/public_ajax.php?type=chat", {id:1, data: chat_id, hash:'<?php echo $random_hash; ?>'}, function(response){
			container.html(response);
			var scrollTo_val = $("#conv-"+chat_id+"-inner").prop('scrollHeight') + 'px';
			$("#conv-"+chat_id+"-inner").slimScroll({ scrollTo : scrollTo_val });
		});
	});
}); 


</script>
</html>