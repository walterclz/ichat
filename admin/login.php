<?php require_once("assets/includes/Library/Loader.php");
if(filesize($parent.'/config.php') == '0') { redirect_to('install/index.php'); }
if ($session->is_logged_in() == true ) {
		redirect_to('index.php');
}
if(isset($_GET['reset_hash'])) {
	$reset_hash= $db->escape_value($_GET['reset_hash']);
	$then = base64_decode(base64_decode($reset_hash));
	if(is_numeric($then) && (time() - $then) > 3600 || !is_numeric($then) ) {
		$msg = "This link is expired! Please generate another link to continue";
		redirect_to("login.php?edit=fail&msg={$msg}");
	}
	
	$found_user = User::find($then,"reset_hash" , " AND deleted = 0 ORDER BY id DESC LIMIT 1");

	if($found_user) {
		$found_user = $found_user[0];
		
		$password = get_random(10);
		$phpass = new PasswordHash(8, true);
		$hashedpassword = $phpass->HashPassword($password);
		$found_user->password = $hashedpassword;
		
		$found_user->reset_hash = "";
		
		if($found_user->update()) {
				
				##########
				## MAILER ##
				##########
				$msg = "You've requested to reset your password on iChat Live Support (".WEB_LINK . ")<br>";
				$msg .= "Here's a temporary password generated for your account, please login and reset your password to ensure safety of your information<br>";
				$msg .= "Your new password is:<br><pre>{$password}</pre>";
				$title = 'Password Reset';
				
				Mailer::send_mail_to($found_user->email , $found_user->name , $msg , $title);
				
				$msg = "New temporary password sent to your email";
				redirect_to("login.php?edit=success&msg={$msg}");
		}
	} else {
		$msg = "This link is invalid! Please generate another link to continue";
		redirect_to("login.php?edit=fail&msg={$msg}");
	}
}

if (isset($_POST['forgot_pass'])) {
	if($_POST['loginhash'] == $_SESSION[$elhash_login]){
		$email = trim($_POST["email"]);
		
		$user = User::find($email , "email" , " AND deleted = 0 ORDER BY id DESC LIMIT 1");
		
		if($user) {
			$user = $user[0];
			
			if ($user->disabled == "1") {
				$msg = "Account disabled! please contact system administration";
				redirect_to("login.php?edit=fail&msg={$msg}");
			}
			
			if($user->reset_hash) {
				$then = base64_decode(base64_decode($user->reset_hash));
				if(is_numeric($then) && (time() - $then) > 3600 || !is_numeric($then) ) {		//expired link..
					$user->reset_hash= "";
				}
				
				if(is_numeric($then) && (time() - $then) < 300 ) {		//very early to re-generate! < 5 mins.
					$deficit = time() - $then;
					$msg = "Reset link already generated and sent to this email, Please try again after " . secondsToTime($deficit);
					redirect_to("login.php?edit=fail&msg={$msg}");
				}
			}
			
			$reset_hash =  time();
			$user->reset_hash = $reset_hash;
			$user->update();
			
			$reset_link = WEB_LINK . "login.php?reset_hash=" . base64_encode(base64_encode($reset_hash));
			
			##########
			## MAILER ##
			##########
			$msg = "You've requested to reset your password on iChat Live Support (". WEB_LINK . ")<br>";
			$msg .= "Please click this link to issue a temporary password for you:<br>";
			$msg .= "<pre>{$reset_link}</pre>";
			$msg .= "<br>This link will expire in 1 hour, If it wasn't you please ignore this mail";
			$title = 'Password Reset Request';
			
			Mailer::send_mail_to($user->email , $user->name , $msg , $title);
			
			$msg = "Please check your email account for details on how to reset your password";
			redirect_to("login.php?edit=success&msg={$msg}");
			
		} else {
			$msg = "Account not found on database! please contact system administration";
			redirect_to("login.php?edit=fail&msg={$msg}");
		}

		
	} else {
		//security fail
		$error_message = "Authorization fail, please try again";
	}
}
if (isset($_POST['enterlogin'])) {
	if($_POST['loginhash'] == $_SESSION[$elhash_login]){
		$username = trim($_POST["username"]);
		$password = trim($_POST["password"]);
		
		if (strlen($password) > 72) {
			redirect_to('login.php');
			die();
		}
			
		$found_user =User::hash_authenticate($username);
		if ($found_user) {
			
			//check if disabled ...
			if ($found_user->disabled == "1") {
				$msg = "Account disabled! please contact system administration";
				redirect_to("login.php?edit=fail&msg={$msg}");
			}
			
			//check password ...
			$saltedhash = $found_user->password;
			
				$phpass = new PasswordHash(8, true);
				if ($phpass->CheckPassword($password, $saltedhash)) {
					$session->login($found_user);
					
					Log::log_action($found_user->id , "Login" , "Login to system");
					
					if(isset($_POST['remember-me']) && $_POST['remember-me'] == '1') {
						$params = session_get_cookie_params();
						setcookie(session_name(), $_COOKIE[session_name()], time() + 60*60*24*30, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
					}
					
					redirect_to('index.php');
				} else {
					$error_message = "Invalid Password, Please try again";
				}
				
			
			
		} else {
			$error_message = "User not found , Please try again";
		}
	} else {
		//security fail
		$error_message = "Authorization fail, please try again";
	}

} 

if(isset($_SESSION[$elhash_login]) && $_SESSION[$elhash_login] != "") { 
	$random_hash = $_SESSION[$elhash_login];
} else {
	$random_hash = uniqid();
	$_SESSION[$elhash_login] = $random_hash;
}

?><!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.png" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>iChat | Live Support System</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
    
    <div class="wrapper wrapper-full-page">
        <div class="full-page login-page" filter-color="disable" data-image="assets/img/login.png">
            <!--   you can change the color of the filter page using: data-color="blue | purple | green | orange | red | rose " -->
            <div class="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3" id="login-box">
                            <form method="POST" action="login.php">
                                <div class="card card-login">
                                    <div class="card-header text-center" data-background-color="orange">
                                        <h4 class="card-title">Login</h4>
                                        
                                    </div>
									<br>
                                    <p class="category text-center">
										<?php if(isset($error_message) && $error_message != "" ) { 
											echo "<span class='text-danger'>".$error_message."</span>"; 
										} ?>
										
										<?php if(isset($_GET['edit']) && $_GET['edit'] == "success" && isset($_GET['msg']) && $_GET['msg'] != "" ) { 
											$msg = $db->escape_value($_GET['msg']);
											echo "<span class='text-success'>".$msg."</span>"; 
										} ?>
										
										<?php if(isset($_GET['edit']) && $_GET['edit'] == "fail" && isset($_GET['msg']) && $_GET['msg'] != "" ) { 
											$msg = $db->escape_value($_GET['msg']);
											echo "<span class='text-danger'>".$msg."</span>"; 
										} ?>
										
										<?php if(isset($_GET['logout']) && $_GET['logout'] == "1" ) { 
											echo "<span class='text-success'>Logged out of the system successfully</span><br><br><br>"; 
										} ?>
                                    </p>
                                    <div class="card-content">
                                        
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">face</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Username</label>
                                                <input type="text" class="form-control" name="username" required>
                                            </div>
                                        </div>
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">lock_outline</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Password</label>
                                                <input type="password" class="form-control" name="password" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-warning btn-simple btn-wd btn-lg" name="enterlogin" style="margin:0">Let's go</button>
                                        <a href="#me" id="forget-pass-btn" class="btn btn-danger btn-simple btn-wd btn-lg" style="margin:0">Forget Password?</a>
                                    </div>
                                </div>
								<?php echo "<input type=\"hidden\" name=\"loginhash\" value=\"".$random_hash."\" readonly/>"; ?>
                            </form>
                        </div>
						
						
						<div class="col-md-4 col-sm-6 col-md-offset-4 col-sm-offset-3" id="forget-pass-box" style="display:none">
                            <form method="POST" action="login.php">
                                <div class="card card-login">
                                    <div class="card-header text-center" data-background-color="orange">
                                        <h4 class="card-title">Forget Password?</h4>
                                        
                                    </div>
									<br>
                                    <p class="category text-center">
										<?php if(isset($error_message) && $error_message != "" ) { 
											echo "<span class='text-danger'>".$error_message."</span>"; 
										} ?>
										
										<?php if(isset($_GET['edit']) && $_GET['edit'] == "success" && isset($_GET['msg']) && $_GET['msg'] != "" ) { 
											$msg = $db->escape_value($_GET['msg']);
											echo "<span class='text-success'>".$msg."</span>"; 
										} ?>
										
										<?php if(isset($_GET['edit']) && $_GET['edit'] == "fail" && isset($_GET['msg']) && $_GET['msg'] != "" ) { 
											$msg = $db->escape_value($_GET['msg']);
											echo "<span class='text-danger'>".$msg."</span>"; 
										} ?>
										
										
										<?php if(isset($_GET['logout']) && $_GET['logout'] == "1" ) { 
											echo "<span class='text-success'>Logged out of the system successfully</span><br><br><br>"; 
										} ?>
                                    </p>
                                    <div class="card-content">
                                        
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="material-icons">mail</i>
                                            </span>
                                            <div class="form-group label-floating">
                                                <label class="control-label">Your Email</label>
                                                <input type="text" class="form-control" name="email" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="footer text-center">
                                        <button type="submit" class="btn btn-warning btn-simple btn-wd btn-lg" name="forgot_pass" style="margin:0">Let's go</button>
                                        <a href="#me" id="back-btn" class="btn btn-success btn-simple btn-wd btn-lg" style="margin:0">Back</a>
                                    </div>
                                </div>
								<?php echo "<input type=\"hidden\" name=\"loginhash\" value=\"".$random_hash."\" readonly/>"; ?>
                            </form>
                        </div>
						
						
						
                    </div>
                </div>
            </div>
            <?php require_once("assets/includes/footer.php"); ?>
        </div>
    </div>
</body>


<!--   Core JS Files   -->
<script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/material.min.js" type="text/javascript"></script>
<script src="assets/js/perfect-scrollbar.jquery.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.sharrre.js" type="text/javascript"></script>
<!-- Material Dashboard javascript methods -->
<script src="assets/js/material-dashboard.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>
<script type="text/javascript">
    $().ready(function() {
        demo.checkFullPageBackgroundImage();

        setTimeout(function() {
            // after 1000 ms we add the class animated to the login/register card
            $('.card').removeClass('card-hidden');
        }, 700);
		
		$("a#forget-pass-btn , a#back-btn").click(function(){
			$("div#forget-pass-box").slideToggle();
			$("div#login-box").slideToggle();
		});
    });
</script>
</html>