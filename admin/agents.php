<?php require_once('assets/includes/header.php');


if (isset($_POST['add_obj'])) {
		if(!$current_user->can_see_this("agents.read",$group)) {
			$msg = "You don't have required permissions to do this action, please contact system admins";
			redirect_to("agents.php?edit=fail&msg={$msg}");
		}
		if($_POST['hash'] == $_SESSION[$elhash]){
			//unset($_SESSION[$elhash]);
			
			$db_fields = Array('name','mobile', 'address' , 'position' ,'education', 'about' , 'prvlg_group' , 'department_id' , 'avatar' ,'username' , 'email' , 'password');
			
			$upload_key = array_search('avatar' , $db_fields);
			if($upload_key) {
				unset($db_fields[$upload_key]);
				$upload_present = true;
			}
			
			foreach($db_fields as $field) {
				if(isset($_POST[$field])) {
					$$field = $db->escape_value($_POST[$field]);
				}
			}
			
			$new_entry = new User();
			
			foreach($db_fields as $field) {
				if(isset($$field)) {
					$new_entry->$field = $$field;
				}
			}
				
			$email_exists = User::check_existance("email", $email);
			
			if($email_exists) {
				$msg = "Email already exists in database! please try again";
				redirect_to("agents.php?edit=fail&msg={$msg}");
			} 
				$new_entry->email = $email;
			
				$username_exists = User::check_existance("username", $username);
				
				if($username_exists) {
					$msg = "Username already exists in database! please try again";
					redirect_to("agents.php?edit=fail&msg={$msg}");
				}
			
				$new_entry->username = $username;
				
				$phpass = new PasswordHash(8, true);
				$hashedpassword = $phpass->HashPassword($password);
				
				$new_entry->password = $hashedpassword;
				
			if(isset($upload_present) && $upload_present == true  && isset($_FILES['avatar']) ) {
				$files = '';
				$f = 0;
				$images = array();
				$num_pics = 1;
				$target = $_FILES['avatar'];
				$crop_arr = $_POST['cropped'];
				$crop = json_decode($crop_arr , true);
				$upload_problems = 0;
				for ($f ; $f < $num_pics ; $f++) :
					$file = "file";
					$string = $$file . "{$f}";
					$$string = new File();	
						if(!empty($_FILES['avatar']['name'][$f])) {
							$$string->attach_file($_FILES['avatar'], $f);
							if ($$string->save($crop)) {
								$images[$f] = $$string->id;
							} else {
								$upl_msg = "Cannot upload profile picture, please try again";
								$upl_msg .= join("<br />" , $$string->errors);							
								$upload_problems = 1;
							}
						}
				endfor;
				
				if(!empty($images)) {
					$final_string = implode("," , $images);
					//if($new_entry->files != NULL) {
						//$new_entry->files .= ",". $final_string;
					//} else {
						//$new_entry->files .= $final_string;
					//}
					$new_entry->avatar = $final_string;
				}
			}
			
			
			
			if ($new_entry->create()) {
				
					$msg = "New agent profile created successfully";
					if(isset($upload_problems) && $upload_problems == '1' ) { $msg .= $upl_msg; }
					redirect_to("agents.php?edit=success&msg={$msg}");
				
			} else {
				$msg = "Unable to save data, please try again";
				redirect_to("agents.php?edit=fail&msg={$msg}");
			}
			
		} else {
			$msg = "Authorization failed, please try again";
			redirect_to("agents.php?edit=fail&msg={$msg}");	
		}
}


if (isset($_POST['edit_obj'])) {
	
		if(!$current_user->can_see_this("agents.update",$group)) {
			$msg = "You don't have required permissions to do this action, please contact system admins";
			redirect_to("agents.php?edit=fail&msg={$msg}");
		}
		
		if($_POST['hash'] == $_SESSION[$elhash]){
			//unset($_SESSION[$elhash]);
			$edit_id = $db->escape_value($_POST["edit_id"]);
			
			if(!User::check_id_existance($edit_id)) {
				$msg = "Unable to update data, User not found! please try again";
				redirect_to("agents.php?edit=fail&msg={$msg}");
			}
			
			$db_fields = Array('name','mobile', 'address' , 'position' ,'education' , 'about' , 'avatar' , 'department_id' , 'prvlg_group');
			
			$upload_key = array_search('avatar' , $db_fields);
			if($upload_key) {
				unset($db_fields[$upload_key]);
				$upload_present = true;
			}
			
			foreach($db_fields as $field) {
				if(isset($_POST[$field])) {
					$$field = $db->escape_value($_POST[$field]);
				}
			}
			
			$edited_entry = User::get_specific_id($edit_id);
			
			foreach($db_fields as $field) {
				if(isset($$field)) {
					$edited_entry->$field = $$field;
				}
			}
			
			$password = $db->escape_value($_POST['password']);
			
			/*if($current_user->prvlg_group == "1" ) {
				$prvlg_group = $db->escape_value($_POST['prvlg_group']);
				$edited_entry->prvlg_group = $prvlg_group;
			}*/
			
			if($current_user->prvlg_group == "1" ) {
			$email = $db->escape_value($_POST['email']);
			
			$current_email = $edited_entry->email;
			$email_exists = User::check_existance_except("email", $email , $edited_entry->id);
			
			if($email_exists) {
				$msg = "Email already exists in database! please try again";
				redirect_to("agents.php?edit=fail&msg={$msg}");
			}
			
			if($email != '' && $email != $current_email) {
			$edited_entry->email = $email;
			}
			}
			
			if($current_user->prvlg_group == "1" ) {
				$username = $db->escape_value($_POST['username']);
				
				$current_username = $edited_entry->username;
				$username_exists = User::check_existance_except("username", $username , $edited_entry->id);
				
				if($username_exists) {
					$msg = "Username already exists in database! please try again";
					redirect_to("agents.php?edit=fail&msg={$msg}");
					
				}
			
				if($username != '' && $username != $current_username) {
					$edited_entry->username = $username;
				}
			}
			
			
			if($current_user->prvlg_group == "1" ) {
				$current_password = $edited_entry->password;
				if($password !='' && $password != $current_password ) {
				$phpass = new PasswordHash(8, true);
				$hashedpassword = $phpass->HashPassword($password);
				
				$edited_entry->password = $hashedpassword;
				}
			}
			
			if(isset($upload_present) && $upload_present == true && isset($_FILES['avatar']) ) {
				$files = '';
				$f = 0;
				$images = array();
				$num_pics = 1;
				$target = $_FILES['avatar'];
				$crop_arr = $_POST['cropped'];
				$crop = json_decode($crop_arr , true);
				$upload_problems = 0;
				for ($f ; $f < $num_pics ; $f++) :
					$file = "file";
					$string = $$file . "{$f}";
					$$string = new File();	
						if(!empty($_FILES['avatar']['name'][$f])) {
							$$string->attach_file($_FILES['avatar'], $f);
							if ($$string->save($crop)) {
								$images[$f] = $$string->id;
							} else {
								$upl_msg = "Cannot upload profile picture, please try again";
								$upl_msg .= join("<br />" , $$string->errors);							
								$upload_problems = 1;
							}
						}
				endfor;
				
				if(!empty($images)) {
					$final_string = implode("," , $images);
					//if($edited_entry->files != NULL) {
						//$edited_entry->files .= ",". $final_string;
					//} else {
						//$edited_entry->files .= $final_string;
					//}
					$edited_entry->avatar = $final_string;
				}
			}
			
			if ($edited_entry->update()) {
				
					$msg = "Data updated successfully";
					if(isset($upload_problems) && $upload_problems == '1' ) { $msg .= $upl_msg; }
					redirect_to("agents.php?edit=success&msg={$msg}");
					
				} else {
					$msg = 'Unable to update data, please try again';
					redirect_to("agents.php?edit=fail&msg={$msg}");
				}
			
		
			} else {
				$msg = "Authorization failed, please try again";
				redirect_to("agents.php?edit=fail&msg={$msg}");
			}
}






$edit_mode = false;

if (isset($_GET['type']) && isset($_GET['id']) && is_numeric($_GET['id']) && isset($_GET['hash'])) {
			$type = $db->escape_value($_GET['type']);
			$id = $db->escape_value($_GET['id']);
			$recieved_hash = $db->escape_value($_GET['hash']);
			
			if(!User::check_id_existance($id)) {
				redirect_to("agents.php");
			}
			
			$this_obj = User::get_specific_id($id);
			
			if ($_SESSION[$elhash] == $recieved_hash) {
				//unset($_SESSION[$elhash]);
				switch($type) {
					case 'delete' :
						if(!$current_user->can_see_this("agents.delete",$group)) {
							$msg = "You don't have required permissions to do this action, please contact system admins";
							redirect_to("agents.php?edit=fail&msg={$msg}");
						}
						
						if($id == "1") {
							$msg = "You don't have required permissions to do this action, please contact system admins";
							redirect_to("agents.php?edit=fail&msg={$msg}");
						}
						
						$this_obj->deleted = 1;
						if($this_obj->update()) {
							$msg = "Agent deleted successfully";
							redirect_to("agents.php?edit=success&msg={$msg}");
						} else {
							$msg = 'Unable to delete data, please try again';
							redirect_to("agents.php?edit=fail&msg={$msg}");
						}
					break;
					
					case 'edit' :
						if(!$current_user->can_see_this("agents.update",$group)) {
							$msg = "You don't have required permissions to do this action, please contact system admins";
							redirect_to("agents.php?edit=fail&msg={$msg}");
						}
						$edit_mode = true;
						
						$user = User::get_specific_id($id);
						if($user->avatar) {
							$img = File::get_specific_id($user->avatar);
							$quser_avatar = WEB_LINK."assets/".$img->image_path();
							$user_avatar_path = UPLOADPATH."/".$img->image_path();
							if (!file_exists($user_avatar_path)) {
								$quser_avatar = WEB_LINK.'assets/img/operator.png';
							}
						} else {
							$quser_avatar = WEB_LINK.'assets/img/operator.png';
						}

						
					break;
				
				}
			} else {
				$msg = "Authorization failed, please try again";
				redirect_to("agents.php?edit=fail&msg={$msg}");
			}
}

require_once('assets/includes/sidebar.php'); ?>
<?php require_once('assets/includes/navbar.php'); ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="col-md-12">
                            
							
							<div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">group</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Agents
										<a href="#me" data-toggle="modal" data-target="#new_entry" class="btn btn-flat btn-sm btn-success pull-right"><i class="material-icons">add</i> Add New Agent</a>
									</h4>
                                    
									<div class="material-datatables">
                                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Privileges</th>
                                                    <th>Position</th>
                                                    <th>Phone</th>
                                                    <th>Department</th>
                                                    <th>Conversations</th>
                                                    <th>Rating</th>
                                                    <th class="disabled-sorting text-right" style="width:100px">Actions</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Privileges</th>
                                                    <th>Position</th>
                                                    <th>Phone</th>
                                                    <th>Department</th>
                                                    <th>Conversations</th>
                                                    <th>Rating</th>
                                                    <th class="text-right">Actions</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
												<?php 
													$agents = User::get_everything(" AND deleted = 0 "); 
													if($agents) {
													foreach($agents as $agent) {
														$conversations = Chat::count_everything(" AND operator_id = '{$agent->id}' ");
														$department = Department::get_specific_id($agent->department_id);
														$department_name = "";
														if($department) {
															$department_name = $department->name;
														}
														if($agent->avatar) {
															$img = File::get_specific_id($agent->avatar);
															$quser_avatar = WEB_LINK."assets/".$img->image_path();
															$user_avatar_path = UPLOADPATH."/".$img->image_path();
															if (!file_exists($user_avatar_path)) {
																$quser_avatar = WEB_LINK.'assets/img/operator.png';
															}
														} else {
															$quser_avatar = WEB_LINK.'assets/img/operator.png';
														}
														$prvlg_group = Group::get_specific_id($agent->prvlg_group);
												?>
													<tr style="cursor:pointer">
														<td onclick="document.location='profile.php?id=<?php echo $agent->id; ?>'; return false;"><a href="profile.php?id=<?php echo $agent->id; ?>" ><img src="<?php echo $quser_avatar; ?>" class="img-circle" style="width:42px; margin-right:10px"></a><?php echo $agent->name; ?></td>
														<td onclick="document.location='profile.php?id=<?php echo $agent->id; ?>'; return false;"><?php echo $prvlg_group->name; ?></td>
														<td onclick="document.location='profile.php?id=<?php echo $agent->id; ?>'; return false;"><?php echo $agent->position; ?></td>
														<td onclick="document.location='profile.php?id=<?php echo $agent->id; ?>'; return false;"><?php echo $agent->mobile; ?></td>
														<td onclick="document.location='profile.php?id=<?php echo $agent->id; ?>'; return false;"><?php echo $department_name; ?></td>
														<td onclick="document.location='profile.php?id=<?php echo $agent->id; ?>'; return false;"><?php echo $conversations; ?></td>
														<td onclick="document.location='profile.php?id=<?php echo $agent->id; ?>'; return false;"><?php 
									$rating = Chat::sum_all("rating" , "operator_id" , $agent->id);
									$rating_total = Chat::count_everything(" AND operator_id= '{$agent->id}' AND rating != 0 ");
									if($rating_total) {
										$stars = ceil($rating/$rating_total);
									} else {
										$stars = 0;
									}
									for($i = 1 ; $i <= 5 ; $i++) {
										echo "<i class='material-icons ";
											if($i <= $stars) { echo ' text-warning'; } else { echo ' text-muted'; }
										echo "'>star</i>";
									} ?></td>
														<td class="text-right">
															<a href="profile.php?id=<?php echo $agent->id; ?>" class="btn btn-simple <?php if($current_user->can_see_this("agents.delete",$group)) { echo ' btn-success'; } ?> btn-icon" ><i class="material-icons">person</i></a>
															<a href="agents.php?id=<?php echo $agent->id; ?>&type=edit&hash=<?php echo $random_hash; ?>" class="btn btn-simple <?php if($current_user->can_see_this("agents.update",$group)) { echo ' btn-warning'; } ?> btn-icon "><i class="material-icons">edit</i></a>
															<a href="agents.php?id=<?php echo $agent->id; ?>&type=delete&hash=<?php echo $random_hash; ?>" class="btn btn-simple <?php if($current_user->can_see_this("agents.delete",$group)) { echo ' btn-danger'; } ?> btn-icon" onclick="return confirm('Are you sure you want to delete this agent?');"><i class="material-icons">close</i></a>
														</td>
													</tr>
													<?php }} ?>
                                            </tbody>
                                        </table>
                                    </div>
									
									
                                </div>
								<br style="clear:both"><br style="clear:both"><br style="clear:both">
                            </div>
                        </div>
                </div>
            </div>
            <?php require_once("assets/includes/footer.php"); ?>
	<?php require_once("assets/includes/preloader.php"); ?>

<div class="modal fade" id="new_entry" role="dialog" >
  <div class="modal-dialog " role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" style="margin:0; padding:0">New Agent
		<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
		  <span aria-hidden="true"><i class="material-icons">close</i></span>
		</button></h5>
	  </div>
	  
	<form class="form-horizontal" method="POST" action="agents.php" enctype="multipart/form-data">
    
	  <div class="modal-body">
		
				<div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" value="" required>
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPhone" name="mobile" placeholder="Phone"  value="" >
                    </div>
                  </div>
				  
				  <div class="form-group">
                    <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="inputAddress" name="address" placeholder="Address" ></textarea>
                    </div>
                  </div>
				  
				  <div class="form-group">
                    <label for="inputPosition" class="col-sm-2 control-label">Position</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPosition" name="position" placeholder="Position" value="">
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputEducation" class="col-sm-2 control-label">Education</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEducation" name="education" placeholder="Education" value="" >
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputAbout" class="col-sm-2 control-label">About User</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="inputAbout" name="about" placeholder="About User" ></textarea>
                    </div>
                  </div>
				  
				  
				  <hr>
				  
				  
				  <div class="form-group">
                    <label for="inputProfilePic" class="col-sm-2 control-label">Profile Picture</label>
                    <div class="col-sm-10">
						
						<div style=""><img src="assets/img/operator.png" class="" style="float:left; padding:5px; margin-right:10px; max-width:100%; height:350px" id="img1"></div>
						
						<br style='clear:both'>
						<div style="height:64px; padding-top: 12px;width:200px;float:left">
							<input class="text-input" type="file" name="avatar[]" id="img1_upl"/><br/>
						</div>
						
						
                    </div>
                  </div>
                  
				  <hr>
				  
				  <div class="form-group">
                    <label for="inputUsername" class="col-sm-2 control-label">Privileges</label>
                    <div class="col-sm-10">
                      <select name="prvlg_group" class="selectpicker form-control">
							<?php $prvlg = Group::get_everything(""); ?>
							<?php if($prvlg) {
								$prvlg= array_reverse($prvlg);
								foreach($prvlg as $g) {
									echo "<option value='{$g->id}'>{$g->name}</option>";
							}} ?>
						</select>
                    </div>
                  </div>
                  <br>
				  <div class="form-group">
                    <label for="inputUsername" class="col-sm-2 control-label">Department</label>
                    <div class="col-sm-10">
                      <select name="department_id" class="selectpicker form-control">
							<?php $departments = Department::get_everything(""); ?>
							<?php if($departments) {
								foreach($departments as $d) {
									echo "<option value='{$d->id}'>{$d->name}</option>";
							}} ?>
						</select>
                    </div>
                  </div>
                  <br>
				  <div class="form-group">
                    <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputUsername" placeholder="Username" name="username" value="" required>
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email"  value="" required>
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPassword" name="password" placeholder="Password"  value="" required>
                    </div>
                  </div>
		
		<?php 
		echo "<input type=\"hidden\" name=\"hash\" value=\"".$random_hash."\" readonly/>"; 
		echo "<input type=\"hidden\" name=\"cropped\" id=\"cropped\" value=\"\" readonly/>";
		?> 
    	
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" name="add_obj">Submit</button>
	  </div>
	</form>
	
	</div>
  </div>
</div>

<?php if($edit_mode) { ?>
<div class="modal fade" id="edit_entry" role="dialog" >
  <div class="modal-dialog " role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" style="margin:0; padding:0">Edit Agent (<?php echo $this_obj->name; ?>)
		<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
		  <span aria-hidden="true"><i class="material-icons">close</i></span>
		</button></h5>
	  </div>
	  
	<form class="form-horizontal" method="POST" action="agents.php" enctype="multipart/form-data">
		<div class="modal-body">
				<div class="form-group">
                    <label for="inputName" class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputName" name="name" placeholder="Name" value="<?php echo $user->name; ?>" required >
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputPhone" class="col-sm-2 control-label">Phone</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPhone" name="mobile" placeholder="Phone"  value="<?php echo $user->mobile; ?>" >
                    </div>
                  </div>
				  
				  <div class="form-group">
                    <label for="inputAddress" class="col-sm-2 control-label">Address</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="inputAddress" name="address" placeholder="Address" ><?php echo $user->address; ?></textarea>
                    </div>
                  </div>
				  
				  <div class="form-group">
                    <label for="inputPosition" class="col-sm-2 control-label">Position</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPosition" name="position" placeholder="Position" value="<?php echo $user->position; ?>" >
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputEducation" class="col-sm-2 control-label">Education</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputEducation" name="education" placeholder="Education" value="<?php echo $user->education; ?>" >
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputAbout" class="col-sm-2 control-label">About User</label>
                    <div class="col-sm-10">
                      <textarea class="form-control" id="inputAbout" name="about" placeholder="About User" ><?php echo $user->about; ?></textarea>
                    </div>
                  </div>
				  
				  
				  <hr>
				  
				  
				  <div class="form-group">
                    <label for="inputProfilePic" class="col-sm-2 control-label">Profile Picture</label>
                    <div class="col-sm-10">
						
						<div style=""><img src="<?php echo $quser_avatar; ?>" class="" style="float:left; padding:5px; margin-right:10px; max-width:100%; height:350px" id="img2"></div>
						
						<br style='clear:both'>
						<div style="height:64px; padding-top: 12px;width:200px;float:left">
							<input class="text-input " type="file" name="avatar[]" id="img2_upl"/><br/>
						</div>
						
						
                    </div>
                  </div>
                  
				  <hr>
				  <div class="form-group">
                    <label for="inputUsername" class="col-sm-2 control-label">Privileges</label>
                    <div class="col-sm-10">
                      <select name="prvlg_group" class="selectpicker form-control">
							<?php $prvlg = Group::get_everything(""); ?>
							<?php if($prvlg) {
								$prvlg= array_reverse($prvlg);
								foreach($prvlg as $g) {
									echo "<option value='{$g->id}'";
										if($user->prvlg_group == $g->id) { echo ' selected'; }
									echo ">{$g->name}</option>";
							}} ?>
						</select>
                    </div>
                  </div>
                  <br>
				  <div class="form-group">
                    <label for="inputUsername" class="col-sm-2 control-label">Department</label>
                    <div class="col-sm-10">
                      <select name="department_id" class="selectpicker form-control">
							<?php $departments = Department::get_everything(""); ?>
							<?php if($departments) {
								foreach($departments as $d) {
									echo "<option value='{$d->id}'";
										if($user->department_id == $d->id) { echo ' selected'; }
									echo ">{$d->name}</option>";
							}} ?>
						</select>
                    </div>
                  </div>
                  <br>
				  
				  <div class="form-group">
                    <label for="inputUsername" class="col-sm-2 control-label">Username</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputUsername" placeholder="Username" name="username" value="<?php echo $user->username; ?>">
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputEmail" class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                      <input type="email" class="form-control" id="inputEmail" name="email" placeholder="Email"  value="<?php echo $user->email ?>" required>
                    </div>
                  </div>
                  
				  <div class="form-group">
                    <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                      <input type="text" class="form-control" id="inputPassword" name="password" placeholder="Unchanged"  value="" >
                    </div>
                  </div>
		
		
		
		<?php echo "<input type=\"hidden\" name=\"hash\" value=\"".$random_hash."\" readonly/>"; ?> 
		<?php echo "<input type=\"hidden\" name=\"edit_id\" value=\"".$this_obj->id."\" readonly/>"; ?> 
    	<?php echo "<input type=\"hidden\" name=\"cropped\" id=\"cropped\" value=\"\" readonly/>"; ?>
		
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" name="edit_obj">Submit</button>
	  </div>
	</form>
	
	</div>
  </div>
</div>
<?php } ?>

<script src="assets/js/cropper/cropper.min.js"></script>
<script src="assets/js/jquery.select-bootstrap.js" type="text/javascript"></script>
	
<script type="text/javascript">
$('#datatables').DataTable({
	"pagingType": "full_numbers",
	"lengthMenu": [
		[10, 25, 50, -1],
		[10, 25, 50, "All"]
	],
	responsive: true,
	language: {
		search: "_INPUT_",
		searchPlaceholder: "Search Users",
	}
});


function readURL(input,targetid) {
if (input.files && input.files[0]) {
	var reader = new FileReader();
	reader.onload = function (e) {
		
		var height;
		var width;
		var parent_width = $("#" + targetid).parent('div').parent('div').css('width').replace(/[^-\d\.]/g, '') - 50;
		
		var _URL = window.URL || window.webkitURL;

		$("#" + targetid).attr('src', e.target.result);
		$("#" + targetid).on('load', function () {
		
		img = new Image();
		img.src = _URL.createObjectURL(input.files[0]);
		img.onload = function () {
			height= this.height;
			if(this.width < parent_width) {
				width= this.width;
			} else {
				width= parent_width;
			}
			
		$("#" + targetid).cropper('destroy');
		$("#" + targetid).parent('div').css('width', width);
		$("#" + targetid).parent('div').css('height', height);
		
		
		$("#" + targetid).cropper({
		  minContainerHeight: height,
		  minContainerWidth: width,
		  aspectRatio: 1/1,
		  crop: function(e) {
			var croppedData = '{"x":"'+ e.x +'","y":"'+ e.y +'","width":"' + e.width + '","height":"' + e.height + '" }';
			$('#cropped').val(croppedData);
		  }
		});
		
		};
		
		});
	}
	reader.readAsDataURL(input.files[0]);
}
}

$("#img1_upl").change(function(){
	readURL(this, 'img1');
});


<?php if(isset($edit_mode) && $edit_mode == "true") { ?>
		//$(window).load(function(){
		$('#edit_entry').modal('show');
		$("#img2_upl").change(function(){
			readURL(this, 'img2');
		});

		//});
<?php } ?>
</script>


</html>