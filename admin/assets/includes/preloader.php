		</div>
	</div>
<audio id="buzzer" src="assets/audio/ring.mp3" type="audio/mp3"></audio>
</body>
<!--   Core JS Files   -->
<script src="assets/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>
<script src="assets/js/material.min.js" type="text/javascript"></script>
<script src="assets/js/arrive.min.js"></script>
<script src="assets/js/perfect-scrollbar.jquery.min.js"></script>
<script src="assets/js/jquery.slimscroll.js"></script>
<script src="assets/js/bootstrap-notify.js"></script>
<script src="assets/js/material-dashboard.js?v=1.2.0"></script>
<script src="assets/js/jquery.sharrre.js" type="text/javascript"></script>
<script src="assets/js/sweetalert2.js" type="text/javascript"></script>
<script src="assets/js/jquery.form.min.js" type="text/javascript"></script>
<script src="assets/js/jquery.datatables.js" type="text/javascript"></script>
<script src="assets/js/jquery.emotions.js" type="text/javascript"></script>

<script>

function generate(type, title, text) {
	swal({
			title: title,
			text: text,
			confirmButtonText: 'Got it',
			buttonsStyling: false,
			confirmButtonClass: "btn btn-success",
			type: type,
			timer: 3000
		});
}

setInterval(function() {
			$.post("assets/includes/one_ajax.php?type=pending-chats", {id: 1 , data: 1,  hash:'<?php echo $random_hash; ?>'}, function(data){
				if(data) {
					$("#incoming").html(data);
					$("#incoming").slideDown();
					$('#buzzer').get(0).play();
				}
			});
			
			$.post("assets/includes/one_ajax.php?type=transferred-chats", {id: 1 , data: 1,  hash:'<?php echo $random_hash; ?>'}, function(data){
				if(data) {
					$("#incoming").html(data);
					$("#incoming").slideDown();
					$('#buzzer').get(0).play();
				}
			});
}, 5000);

$('#incoming').on('click' , '.decline_chat' , function() {
	$('#incoming').slideToggle();
	audioElement.pause();
});
$('#incoming_list').on('click' , '.decline_chat_list' , function() {
	$(this).parent().parent().parent().fadeOut();
});
	
</script>

<?php
	if (isset($_GET['edit']) && isset($_GET['msg']) && $_GET['edit'] == "success") {
	$status_msg = $db->escape_value($_GET['msg']);				
?>
	<script type="text/javascript">
		generate("success","Success!" ,"<?php echo $status_msg ?>");
	</script>
<?php
	}
	if (isset($_GET['edit']) && isset($_GET['msg']) && $_GET['edit'] == "fail"  ) {
		$status_msg = $db->escape_value($_GET['msg']);
?>
	<script type="text/javascript">
		generate("error","Error!", "<?php echo $status_msg ?>");
	</script>
<?php 
	}
?>