<?php require_once('assets/includes/header.php'); ?>
<?php require_once('assets/includes/sidebar.php'); ?>
<?php require_once('assets/includes/navbar.php'); ?>
<?php $department = "";
if($current_user->prvlg_group != "1") {
	$department = " AND department = '{$current_user->department_id}' ";
} 
$all_time = Chat::sum_all("duration" , "offline" , "0");
$first_chat = Chat::get_first_chat();
$first_offline_chat = Chat::get_first_offline_chat();
?>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="green">
                                    <i class="material-icons">headset_mic</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Conversations</p>
                                    <h3 class="title"><?php echo Chat::count_everything(" AND offline = 0 {$department} "); ?>
                                        <small>chats</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons text-success">search</i>
                                        <a href="archive.php" style="color:black">View conversations...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="orange">
                                    <i class="material-icons">live_help</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Offline Msgs</p>
                                    <h3 class="title"><?php echo Chat::count_everything(" AND offline = 1 {$department} "); ?>
                                        <small>chats</small>
                                    </h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons text-success">search</i>
                                        <a href="offline_archive.php" style="color:black">View conversations...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="red">
                                    <i class="material-icons">timer</i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Time Chatting</p>
                                    <h3 class="title"><small><?php if($all_time) { 
										$arr = secondsToArray($all_time);
										if(isset($arr['days'])) {
											echo $arr['days']. ' Days';
										} elseif(isset($arr['hours'])) {
											echo $arr['hours']. ' Hours';
										} elseif(isset($arr['minutes'])) {
											echo $arr['minutes']. ' Minutes';
										} elseif(isset($arr['seconds'])) {
											echo $arr['seconds']. ' Seconds';
										}
									} else { echo " 0 Minutes"; } ?></small></h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
										<?php if($first_chat) { ?><i class="material-icons">date_range</i> Starting from <?php echo date_descriptive($first_chat[0]); ?><?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header" data-background-color="blue">
                                    <i class="fa fa-group"></i>
                                </div>
                                <div class="card-content">
                                    <p class="category">Agents</p>
                                    <h3 class="title"><?php echo User::count_everything(" AND prvlg_group = 2 AND deleted = 0 "); ?></h3>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons text-success">search</i>
                                        <a href="agents.php" style="color:black">View agents...</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="green">
                                    <div class="ct-chart" id="dailySalesChart"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Daily Conversations</h4>
                                    <p class="category">Sent during this week</p>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <?php if($first_chat) { ?><i class="material-icons">access_time</i> Last chat was 
										<?php 
											$time_diff = time() - strtotime($first_chat[1]);
											$arr = secondsToArray($time_diff);
											if(isset($arr['days'])) {
												echo $arr['days']. ' days ago';
											} elseif(isset($arr['hours'])) {
												echo $arr['hours']. ' hours ago';
											} elseif(isset($arr['minutes'])) {
												echo $arr['minutes']. ' minutes ago';
											} elseif(isset($arr['seconds'])) {
												echo $arr['seconds']. ' seconds ago';
											}
										} ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-header card-chart" data-background-color="red">
                                    <div class="ct-chart" id="completedTasksChart"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Offline Messages</h4>
                                    <p class="category">Sent during this week</p>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <?php if($first_offline_chat) { ?><i class="material-icons">access_time</i> Last message was 
										<?php 
											$time_diff = time() - strtotime($first_offline_chat[1]);
											$arr = secondsToArray($time_diff);
											if(isset($arr['days'])) {
												echo $arr['days']. ' days ago';
											} elseif(isset($arr['hours'])) {
												echo $arr['hours']. ' hours ago';
											} elseif(isset($arr['minutes'])) {
												echo $arr['minutes']. ' minutes ago';
											} elseif(isset($arr['seconds'])) {
												echo $arr['seconds']. ' seconds ago';
											}
										} ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
						
						<div class="card">
                                <div class="card-header card-chart" data-background-color="orange">
                                    <div class="ct-chart" id="emailsSubscriptionChart"></div>
                                </div>
                                <div class="card-content">
                                    <h4 class="title">Departments Activity</h4>
                                    <p class="category">Conversations based on departments</p>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons text-success">search</i>
                                        <a href="departments.php" style="color:black">View departments...</a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                       
                        <div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="orange">
                                    <h4 class="title" style="color:white">Our Featured Agents<small class="pull-right"><a href="agents.php">View All</a></small></h4>
                                </div>
                                <div class="card-content table-responsive">
								
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Department</th>
                                            <th>Conversations</th>
                                            <th>Rating</th>
                                        </thead>
                                        <tbody>
												<?php 
													$m = 1;
													$agents = User::get_everything(" AND deleted = 0 ORDER BY RAND() LIMIT 5");
													if($agents) {
													
													foreach($agents as $agent) {
														$conversations = Chat::count_everything(" AND operator_id = '{$agent->id}' ");
														$department = Department::get_specific_id($agent->department_id);
														$department_name = "";
														if($department) {
															$department_name = $department->name;
														}
														
												?>
													<tr onclick="document.location='profile.php?id=<?php echo $agent->id; ?>&type=view&hash=<?php echo $random_hash; ?>'; return false;" style="cursor:pointer" >
														<td><?php echo $m; ?></td>
														<td><?php echo $agent->name; ?></td>
														<td><?php echo $department_name; ?></td>
														<td><?php echo $conversations; ?></td>
														<td><?php 
									$rating = Chat::sum_all("rating" , "operator_id" , $agent->id);
									$rating_total = Chat::count_everything(" AND operator_id= '{$agent->id}' AND rating != 0 ");
									if($rating_total) {
										$stars = ceil($rating/$rating_total);
									} else {
										$stars = 0;
									}
									for($i = 1 ; $i <= 5 ; $i++) {
										echo "<i style='font-size:15px' class='material-icons ";
											if($i <= $stars) { echo ' text-warning'; } else { echo ' text-muted'; }
										echo "'>star</i>";
									} ?></td>
													</tr>
													<?php $m++; }} ?>
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div><div class="col-lg-6 col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title" style="color:white">Latest Conversations<small class="pull-right"><a href="archive.php">View All</a></small></h4>
                                </div>
                                <div class="card-content table-responsive">
								
                                    <table class="table table-hover">
                                        <thead class="text-warning">
                                            <th>#</th>
                                            <th>Name</th>
                                            <th>Department</th>
                                            <th>Operator</th>
                                            <th>Duration</th>
                                            <th>Rating</th>
                                        </thead>
                                        <tbody>
												<?php 
													$m = 1;
													$limiter = " AND operator_id != 0 ";
													if($current_user->prvlg_group == "2") {
														$limiter = " AND operator_id = '{$current_user->id}' ";
													}
													$msgs = Chat::get_everything(" {$limiter} AND offline = 0 AND ended = 1 ORDER BY RAND() LIMIT 5");
													if($msgs) {
													
													foreach($msgs as $msg) {
														$operator = User::get_specific_id($msg->operator_id);
														$department = Department::get_specific_id($msg->department);
														
												?>
													<tr onclick="document.location='archive.php?id=<?php echo $msg->id; ?>&type=view&hash=<?php echo $random_hash; ?>'; return false;" style="cursor:pointer" >
														<td><?php echo $m; ?></td>
														<td><?php echo $msg->name; ?></td>
														<td><?php echo $department->name; ?></td>
														<td><?php echo $operator->name; ?></td>
														<td><?php echo secondsToTime($msg->duration); ?></td>
														<td><?php 
									for($i = 1 ; $i <= 5 ; $i++) {
										echo "<i style='font-size:15px' class='material-icons ";
											if($i <= $msg->rating) { echo ' text-warning'; } else { echo ' text-muted'; }
										echo "'>star</i>";
									} ?></td>
													</tr>
													<?php $m++; }} ?>
                                            </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php require_once("assets/includes/footer.php"); ?>
        </div>
	<?php require_once("assets/includes/preloader.php"); ?>
<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>
<!-- Material Dashboard DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // Javascript method's body can be found in assets/js/demos.js
    
        /* ----------==========     Daily Sales Chart initialization    ==========---------- */

		<?php 
		$days = array();
		$numbers = array();
		for($i = 0; $i < 7 ; $i++) {
			$days[] = strftime("%a" , strtotime("+{$i} Day" , strtotime("saturday last week")));
			$customquery = ' AND offline = 0 AND DATE_FORMAT(started_at, "%Y-%m-%d") = "' . strftime("%Y-%m-%d" , strtotime("+{$i} Day" , strtotime("saturday last week"))) . '" ';
			$numbers[] = Chat::count_everything($customquery,"");
		}
		?>
		
        dataDailySalesChart = {
            labels: ['<?php echo implode("','" , $days) ?>'],
            series: [
                [<?php echo implode("," , $numbers) ?>]
            ]
        };

        optionsDailySalesChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: <?php echo max($numbers)+3; ?>, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0},
        }

        var dailySalesChart = new Chartist.Line('#dailySalesChart', dataDailySalesChart, optionsDailySalesChart);

        md.startAnimationForLineChart(dailySalesChart);



        /* ----------==========     Completed Tasks Chart initialization    ==========---------- */

		<?php 
		$days = array();
		$numbers = array();
		for($i = 0; $i < 7 ; $i++) {
			$days[] = strftime("%a" , strtotime("+{$i} Day" , strtotime("saturday last week")));
			$customquery = ' AND offline = 1 AND DATE_FORMAT(started_at, "%Y-%m-%d") = "' . strftime("%Y-%m-%d" , strtotime("+{$i} Day" , strtotime("saturday last week"))) . '" ';
			$numbers[] = Chat::count_everything($customquery,"");
		}
		?>
        dataCompletedTasksChart = {
            labels: ['<?php echo implode("','" , $days) ?>'],
            series: [
                [<?php echo implode("," , $numbers) ?>]
            ]
        };

        optionsCompletedTasksChart = {
            lineSmooth: Chartist.Interpolation.cardinal({
                tension: 0
            }),
            low: 0,
            high: <?php echo max($numbers)+3; ?>, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
            chartPadding: { top: 0, right: 0, bottom: 0, left: 0}
        }

        var completedTasksChart = new Chartist.Line('#completedTasksChart', dataCompletedTasksChart, optionsCompletedTasksChart);

        // start animation for the Completed Tasks Chart - Line Chart
        md.startAnimationForLineChart(completedTasksChart);


        /* ----------==========     Emails Subscription Chart initialization    ==========---------- */

		<?php 
		$labels = array();
		$numbers = array();
		$dep = Department::get_everything();
		if($dep) {
			foreach($dep as $d) {
				$labels[] = $d->name;
				$customquery = " AND department = '{$d->id}' ";
				$numbers[] = Chat::count_everything($customquery,"");
			}
		}
		?>
		
        var dataWebsiteViewsChart = {
          labels: ['<?php echo implode("','" , $labels) ?>'],
            series: [
                [<?php echo implode("," , $numbers) ?>]
            ]
        };
        var optionsWebsiteViewsChart = {
            axisX: {
                showGrid: false
            },
            low: 0,
            high: <?php echo max($numbers)+5; ?>,
            chartPadding: { top: 0, right: 5, bottom: 0, left: 0}
        };
        var responsiveOptions = [
          ['screen and (max-width: 640px)', {
            seriesBarDistance: 5,
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];
        var websiteViewsChart = Chartist.Bar('#emailsSubscriptionChart', dataWebsiteViewsChart, optionsWebsiteViewsChart, responsiveOptions);

        //start animation for the Emails Subscription Chart
        md.startAnimationForBarChart(websiteViewsChart);

    
    });
</script>

</html>