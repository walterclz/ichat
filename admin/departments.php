<?php require_once('assets/includes/header.php');

if (isset($_POST['add_obj'])) {
		if(!$current_user->can_see_this("departments.create",$group)) {
			$msg = "You don't have required permissions to do this action, please contact system admins";
			redirect_to("departments.php?edit=fail&msg={$msg}");
		}
		if($_POST['hash'] == $_SESSION[$elhash]){
			//unset($_SESSION[$elhash]);
				
				$name = $db->escape_value($_POST['name']);
				if(isset($_POST['public'])) {
					$public = 1;
				} else {
					$public = 0;
				}
				
				$new = New Department();
				$new->name= $name;
				$new->public= $public;
				
				if($new->create()) {
					$msg = "New department added successfully";
					redirect_to("departments.php?edit=success&msg={$msg}");
				} else {
					$msg = "Unable to save data, please try again";
					redirect_to("departments.php?edit=fail&msg={$msg}");
				}
			
		} else {
			$msg = "Authorization failed, please try again";
			redirect_to("departments.php?edit=fail&msg={$msg}");
		}
}

if (isset($_POST['edit_obj'])) {
		if(!$current_user->can_see_this("departments.update",$group)) {
			$msg = "You don't have required permissions to do this action, please contact system admins";
			redirect_to("departments.php?edit=fail&msg={$msg}");
		}
		if($_POST['hash'] == $_SESSION[$elhash]){
			//unset($_SESSION[$elhash]);
				
				$edit_id = $db->escape_value($_POST['edit_id']);
				$name = $db->escape_value($_POST['name']);
				if(isset($_POST['public'])) {
					$public = 1;
				} else {
					$public = 0;
				}
				if(!Department::check_id_existance($edit_id)) {
					redirect_to("departments.php");
				}
				$edit = Department::get_specific_id($edit_id);
				$edit->name= $name;
				$edit->public= $public;
				
				if($edit->update()) {
					$msg = "Department updated successfully";
					redirect_to("departments.php?edit=success&msg={$msg}");
				} else {
					$msg = "Unable to save data, please try again";
					redirect_to("departments.php?edit=fail&msg={$msg}");
				}
			
		} else {
			$msg = "Authorization failed, please try again";
			redirect_to("departments.php?edit=fail&msg={$msg}");
		}
}

$edit_mode = false;

if (isset($_GET['type']) && isset($_GET['id']) && is_numeric($_GET['id']) && isset($_GET['hash'])) {
			$type = $db->escape_value($_GET['type']);
			$id = $db->escape_value($_GET['id']);
			$recieved_hash = $db->escape_value($_GET['hash']);
			
			if(!Department::check_id_existance($id)) {
				redirect_to("departments.php");
			}
			
			$this_obj = Department::get_specific_id($id);
			
			if ($_SESSION[$elhash] == $recieved_hash) {
				//unset($_SESSION[$elhash]);
				switch($type) {
					case 'delete' :
						if(!$current_user->can_see_this("departments.delete",$group)) {
							$msg = "You don't have required permissions to do this action, please contact system admins";
							redirect_to("departments.php?edit=fail&msg={$msg}");
						}
						$this_obj->deleted = 1;
						if($this_obj->update()) {
							$msg = "Department deleted successfully";
							redirect_to("departments.php?edit=success&msg={$msg}");
						} else {
							$msg = 'Unable to delete data, please try again';
							redirect_to("departments.php?edit=fail&msg={$msg}");
						}
					break;
					
					case 'edit' :
						if(!$current_user->can_see_this("departments.update",$group)) {
							$msg = "You don't have required permissions to do this action, please contact system admins";
							redirect_to("departments.php?edit=fail&msg={$msg}");
						}
						$edit_mode = true;
					break;
				
				}
			} else {
				$msg = "Authorization failed, please try again";
				redirect_to("departments.php?edit=fail&msg={$msg}");
			}
}

require_once('assets/includes/sidebar.php'); ?>
<?php require_once('assets/includes/navbar.php'); ?>
            <div class="content">
                <div class="container-fluid">
                    <div class="col-md-12">
                            
							
							<div class="card">
                                <div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">business_center</i>
                                </div>
                                <div class="card-content">
                                    <h4 class="card-title">Departments
										<a href="#me" data-toggle="modal" data-target="#new_entry" class="btn btn-flat btn-sm btn-success pull-right"><i class="material-icons">add</i> Add New Department</a>
									</h4>
                                    
									<div class="material-datatables">
                                        <table id="datatables" class="table table-striped table-no-bordered table-hover" cellspacing="0" width="100%" style="width:100%">
                                            <thead>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Agents</th>
                                                    <th>Conversations</th>
                                                    <th class="disabled-sorting text-right">Actions</th>
                                                </tr>
                                            </thead>
                                            <tfoot>
                                                <tr>
                                                    <th>Name</th>
                                                    <th>Agents</th>
                                                    <th>Conversations</th>
                                                    <th class="text-right">Actions</th>
                                                </tr>
                                            </tfoot>
                                            <tbody>
											<?php 
												$departments = Department::get_everything(" AND deleted = 0 "); 
												if($departments) {
												foreach($departments as $department) {
													$agents = User::count_everything(" AND department_id = '{$department->id}' ");
													$conversations = Chat::count_everything(" AND department = '{$department->id}' ");
											?>
                                                <tr>
                                                    <td> <?php if($department->public) { echo '<i class="material-icons text-muted" title="Public Department"  style="cursor:pointer; font-size:20px; vertical-align: -5px">visibility</i>&nbsp;&nbsp;&nbsp;&nbsp;'; } else { echo '<i class="material-icons text-muted" title="Invisible to public" style="cursor:pointer; font-size:20px; vertical-align: -5px">visibility_off</i>&nbsp;&nbsp;&nbsp;&nbsp;'; } echo $department->name; ?></td>
                                                    <td><?php echo $agents; ?></td>
                                                    <td><?php echo $conversations; ?></td>
                                                    <td class="text-right">
                                                        <a href="departments.php?id=<?php echo $department->id; ?>&type=edit&hash=<?php echo $random_hash; ?>" data-obj_id="<?php echo $department->id; ?>" class="btn btn-simple <?php if($current_user->can_see_this("departments.update",$group)) { echo ' btn-warning'; } ?> btn-icon "><i class="material-icons">edit</i></a>
                                                        <a href="departments.php?id=<?php echo $department->id; ?>&type=delete&hash=<?php echo $random_hash; ?>" data-obj_id="<?php echo $department->id; ?>" class="btn btn-simple <?php if($current_user->can_see_this("departments.delete",$group)) { echo ' btn-danger'; } ?> btn-icon" onclick="return confirm('Are you sure you want to delete this item?');"><i class="material-icons">close</i></a>
                                                    </td>
                                                </tr>
												<?php }} ?>
                                            </tbody>
                                        </table>
                                    </div>
									
									
                                </div>
								<br style="clear:both"><br style="clear:both"><br style="clear:both">
                            </div>
                        </div>
                </div>
            </div>
            <?php require_once("assets/includes/footer.php"); ?>
	<?php require_once("assets/includes/preloader.php"); ?>

<div class="modal fade" id="new_entry" role="dialog" >
  <div class="modal-dialog " role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" style="margin:0; padding:0">New Department
		<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
		  <span aria-hidden="true"><i class="material-icons">close</i></span>
		</button></h5>
	  </div>
	  
	<form class="form-horizontal" method="POST" action="departments.php">
    
	  <div class="modal-body">
		<div class="row">
			<label class="col-md-3 label-on-left">Department Name</label>
			<div class="col-md-9">
				<div class="form-group label-floating is-empty">
					<label class="control-label"></label>
					<input type="text" class="form-control" name="name" required>
				<span class="material-input"></span></div>
			</div>
		</div>
		<div class="checkbox">
			<label>
				<input type="checkbox" name="public" value="1"> Visible to public visitors
			</label>
		</div>
		<?php echo "<input type=\"hidden\" name=\"hash\" value=\"".$random_hash."\" readonly/>"; ?> 
    	
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" name="add_obj">Submit</button>
	  </div>
	</form>
	
	</div>
  </div>
</div>

<?php if($edit_mode) { ?>
<div class="modal fade" id="edit_entry" role="dialog" >
  <div class="modal-dialog " role="document">
	<div class="modal-content">
	  <div class="modal-header">
		<h5 class="modal-title" style="margin:0; padding:0">Edit Department (<?php echo $this_obj->name; ?>)
		<button type="button" class="close" data-dismiss="modal" aria-label="Close" >
		  <span aria-hidden="true"><i class="material-icons">close</i></span>
		</button></h5>
	  </div>
	  
	<form class="form-horizontal" method="POST" action="departments.php">
    
	  <div class="modal-body">
		<div class="row">
			<label class="col-md-3 label-on-left">Department Name</label>
			<div class="col-md-9">
				<div class="form-group label-floating is-empty">
					<label class="control-label"></label>
					<input type="text" class="form-control" name="name" required value="<?php echo $this_obj->name; ?>">
				<span class="material-input"></span></div>
			</div>
		</div>
		<div class="checkbox">
			<label>
				<input type="checkbox" name="public" value="1" <?php if($this_obj->public) { echo 'checked'; } ?> > Visible to public visitors
			</label>
		</div>
		<?php echo "<input type=\"hidden\" name=\"hash\" value=\"".$random_hash."\" readonly/>"; ?> 
		<?php echo "<input type=\"hidden\" name=\"edit_id\" value=\"".$this_obj->id."\" readonly/>"; ?> 
    	
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		<button type="submit" class="btn btn-primary" name="edit_obj">Submit</button>
	  </div>
	</form>
	
	</div>
  </div>
</div>
<?php } ?>


<script type="text/javascript">
$('#datatables').DataTable({
	"pagingType": "full_numbers",
	"lengthMenu": [
		[10, 25, 50, -1],
		[10, 25, 50, "All"]
	],
	responsive: true,
	language: {
		search: "_INPUT_",
		searchPlaceholder: "Search departments",
	}
});
<?php if(isset($edit_mode) && $edit_mode == "true") { ?>
		//$(window).load(function(){
		$('#edit_entry').modal('show');
		//});
<?php } ?>
</script>


</html>