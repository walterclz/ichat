<?php require_once('assets/includes/header.php'); ?>
<?php require_once('assets/includes/sidebar.php'); ?>
<?php require_once('assets/includes/navbar.php'); ?>
<?php 
$time = time();
$time_check = $time - 300;     //We Have Set Time 5 Minutes
$delete_session = Visitor::delete_session($time_check);
$online_users = Visitor::get_everything();
?>
            <div class="content">
                <div class="container-fluid">
					<?php if($online_users) {
						if(!empty($gmaps_key)) {
					?>
					<div id="map_canvas" style="height:500px"></div>
					<br>Currently Online: <b><?php echo count($online_users); ?></b> Users
					<?php } else { ?>
						<h2>
						<center>
						<img src="assets/img/map_disabled.png" style="width:20%"><br>
						Please set google maps API key first!
						</center>
						</h2>
						<center>
							1) Get new Google Maps API Key, from https://developers.google.com/maps/documentation/javascript/get-api-key<br>
							2) Edit (assets/include/config.php) file, add this line:
						</center>
						<pre>
						$gmaps_key = "YOUR_API_KEY_HERE";
						</pre>
						
					<?php }
					} else { ?>
						<br><h2>
						<center>
						<img src="assets/img/map.png" style="width:20%"><br>
						No Online Users!
						</center>
						</h2>
					<?php } ?>
                </div>
            </div>
            <?php require_once("assets/includes/footer.php"); ?>
	<?php require_once("assets/includes/preloader.php"); ?>

<?php if(!empty($gmaps_key)) { ?>
<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=<?php echo $gmaps_key; ?>"></script>
	
<script type="text/javascript">

function geocodeAddress(locations, i) {
    var title = locations[i][0];
    var address = locations[i][1];
    var time = locations[i][2];
	var user_id = locations[i][3];
	var page = locations[i][4];
	
    geocoder.geocode({
        'address': locations[i][1]
    },
	
    function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
            var marker = new google.maps.Marker({
                icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                map: map,
                position: results[0].geometry.location,
                title: title,
                animation: google.maps.Animation.DROP,
                address: address,
                time: time,
				user_id: user_id
            })
            infoWindow(marker, map, title, address, time, user_id,page);
            bounds.extend(marker.getPosition());
            map.fitBounds(bounds);
			
        } else {
            alert("geocode of " + address + " failed:" + status);
        }
    });
}


var locations = [
  <?php 
	if($online_users) {
		foreach($online_users as $online_user) {
			$time_ago = secondsToSM( time() - $online_user->time );
			echo "['{$online_user->ip}','{$online_user->address}','{$time_ago} ago','{$online_user->id}','{$online_user->current_page}'],";
		} 
	}
  ?>
];

var geocoder;
var map;
var bounds = new google.maps.LatLngBounds();

function initialize() {
  map = new google.maps.Map(
    document.getElementById("map_canvas"), {
		mapTypeId: google.maps.MapTypeId.ROADMAP,
		scrollwheel: false, //we disable de scroll over the map, it is a really annoing when you scroll through page
            styles: [{
                "featureType": "water",
                "stylers": [{
                    "saturation": 43
                }, {
                    "lightness": -11
                }, {
                    "hue": "#0088ff"
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [{
                    "hue": "#ff0000"
                }, {
                    "saturation": -100
                }, {
                    "lightness": 99
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#808080"
                }, {
                    "lightness": 54
                }]
            }, {
                "featureType": "landscape.man_made",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ece2d9"
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ccdca1"
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#767676"
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "color": "#ffffff"
                }]
            }, {
                "featureType": "poi",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "landscape.natural",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "on"
                }, {
                    "color": "#b8cb93"
                }]
            }, {
                "featureType": "poi.park",
                "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi.sports_complex",
                "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi.medical",
                "stylers": [{
                    "visibility": "on"
                }]
            }, {
                "featureType": "poi.business",
                "stylers": [{
                    "visibility": "simplified"
                }]
            }]
    });
  geocoder = new google.maps.Geocoder();

  for (i = 0; i < locations.length; i++) {
    geocodeAddress(locations, i);
  }
}
google.maps.event.addDomListener(window, "load", initialize);

function geocodeAddress(locations, i) {
  var title = locations[i][0];
  var address = locations[i][1];
  var time = locations[i][2];
  var user_id = locations[i][3];
  var page = locations[i][4];
  
  geocoder.geocode({
      'address': locations[i][1]
    },

    function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        var marker = new google.maps.Marker({
          icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
          map: map,
          position: results[0].geometry.location,
          title: title,
          animation: google.maps.Animation.DROP,
          address: address,
          time: time,
		  user_id: user_id,
		  page: page
        })
        infoWindow(marker, map, title, address, time, user_id, page);
        bounds.extend(marker.getPosition());
        map.fitBounds(bounds);
        if( locations.length == 1) {
			map.setZoom(6);
		}
      } else {
        alert("geocode of " + address + " failed:" + status);
      }
    });
}

function infoWindow(marker, map, title, address, time, user_id,page) {
  google.maps.event.addListener(marker, 'click', function() {
    var html = "<div><h3><i class='fa fa-laptop'></i> " + title + "</h3><i class='fa fa-home'></i> " + address + "<br><br><i class='fa fa-desktop'></i> " + page + "<br><br><i class='fa fa-clock-o'></i> <i>Last seen: " + time + "</i><center><a href='chatroom.php?invite="+user_id+"' class='btn btn-primary btn-flat btn-sm' data-online_user_id='"+ user_id +"' target='_blank'><i class='fa fa-comments-o'></i> Invite user to chat</a></center></div>";
    iw = new google.maps.InfoWindow({
      content: html,
      maxWidth: 350
    });
    iw.open(map, marker);
  });
}

function createMarker(results) {
  var marker = new google.maps.Marker({
    icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
    map: map,
    position: results[0].geometry.location,
    title: title,
    animation: google.maps.Animation.DROP,
    address: address,
    time: time,
    user_id: user_id,
	page: page
  })
  bounds.extend(marker.getPosition());
  map.fitBounds(bounds);
  
  infoWindow(marker, map, title, address, time, user_id, page);
  return marker;
}

</script>
<?php } ?>
</html>