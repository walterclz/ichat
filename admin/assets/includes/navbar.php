<div class="main-panel">
<nav class="navbar navbar-transparent navbar-absolute">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"> Welcome, Michael! </a>
		</div>
		<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li>
					<a href="index.php" class="hidden-xs hidden-sm">
						<i class="material-icons">dashboard</i>
						<p class="hidden-lg hidden-md">Dashboard</p>
					</a>
				</li>
				<li>
					<a href="profile.php" class="hidden-xs hidden-sm">
						<i class="material-icons">person</i>
						<p class="hidden-lg hidden-md">Profile</p>
					</a>
				</li>
			</ul>
			
			
			<form class="navbar-form navbar-right" role="search" action="archive.php">
				<div class="form-group is-empty">
					<input type="text" class="form-control" placeholder="Search" name="search">
					<span class="material-input"></span>
				</div>
				<button type="submit" class="btn btn-white btn-round btn-just-icon">
					<i class="material-icons">search</i>
					<div class="ripple-container"></div>
				</button>
			</form>
			
		</div>
	</div>
	
	<div class="alert alert-default" style="display:none" id="incoming">
		
	</div>
	
	
</nav>

