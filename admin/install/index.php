<?php session_start();

$assets_location = '../';
$config_file = dirname(dirname(__FILE__)) ."/assets/includes/config.php";

if(!file_exists($config_file)) {
	@$file = fopen($config_file,"w");
	if(!$file) {
		die("Error! config.php file not found .. please create an empty file at <pre>/assets/includes/config.php</pre>");
	}
}

$rand = "RandomHash!";
require_once('functions.php');

if(isset($_SESSION[$rand]) && $_SESSION[$rand] != "") { 
	$random_hash = $_SESSION[$rand];
} else {
	$random_hash = uniqid();
	$_SESSION[$rand] = $random_hash;
}

//Sandbox!
$php_version = phpversion();
$safe_mode = ini_get('safe_mode');
/*if (function_exists('apache_get_modules')) {
  $modules = apache_get_modules();
  $mod_rewrite = in_array('mod_rewrite', $modules);
} else {
  $mod_rewrite =  getenv('HTTP_MOD_REWRITE')=='On' ? true : false ;
}*/
$privileges = is_writable($config_file);
$errors = array();
$step_errors = array();

$db_host = 'localhost';
$db_name = '';
$db_user = '';
$db_pass = '';
$db_table_prefix = 'ichat_';

$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$url = str_replace('install/' , '' , $url);
$url = str_replace('index.php' , '' , $url);

$admin_email = '';
$admin_password = '';

$step = 1;
$steps = 4;

//Builder!
require_once('../assets/includes/Library/PasswordHash.php');
require_once($config_file);

if(isset($_POST['submit'])) {
	
	//if($_POST['hash'] == $_SESSION[$rand]){
		$step = trim($_POST['step']);
		if($step == 1) {
			$db_host = trim($_POST['db_host']);
			$db_name = trim($_POST['db_name']);
			$db_user = trim($_POST['db_user']);
			$db_pass = trim($_POST['db_pass']);
			$db_table_prefix = trim($_POST['db_table_prefix']);
			
			@$connection = mysqli_connect($db_host,$db_user,$db_pass);
			if($connection) {
				if(@mysqli_select_db($connection,$db_name)) {
					
				//write to config.php
$str = "<?php //Database connection settings
defined('DBH') ? null : define ('DBH' , '{$db_host}');
defined('DBU') ? null : define ('DBU' , '{$db_user}');
defined('DBPW') ? null : define ('DBPW' , '{$db_pass}');
defined('DBN') ? null : define ('DBN' , '{$db_name}');
defined('DBTP') ? null : define ('DBTP' , '{$db_table_prefix}');\r\n\r\n";
					
					$file = fopen($config_file, "w");
					fwrite($file, $str);
					fclose($file);
					
					$step = 2;
				} else {
					$step_errors[] = "Database Selection failed! " . mysqli_error($connection);
				}
			} else {
				$step_errors[] = "MySQLi Connection failed! " . mysqli_connect_error();
			}
		} elseif($step == 2) {
			$url = trim($_POST['url']);
			$secret = trim($_POST['secret']);
			$sitekey = trim($_POST['sitekey']);
			$gmaps_key = trim($_POST['gmaps_key']);
			

			
$str = '//Define your web accessible link to this script, including http:// or https:// with TRAILING SLASH / in the end !IMPORTANT
defined("WEB_LINK") ? null : define("WEB_LINK" , "'.$url.'");
defined("ERROR_LINK") ? null : define("ERROR_LINK" , WEB_LINK );
defined("UPL_FILES") ? null : define("UPL_FILES" , WEB_LINK."assets");

//Google Captcha Info, get them from https://www.google.com/recaptcha/admin
$captcha_info = array("secret"=>"' . $secret . '", "sitekey" => "' . $sitekey. '");

//Google Maps API Key, get them from https://developers.google.com/maps/documentation/javascript/get-api-key
$gmaps_key = "' . $gmaps_key . '";
?>';
			
			$file = fopen($config_file, "a+");
			fwrite($file, $str);
			fclose($file);
			
			$new_file = file_get_contents('../.htaccess');
			$new_file = str_replace('[WEB_LINK]', $url , $new_file);
			
			$htaccessfile = fopen("../.htaccess", "w");
			fwrite($htaccessfile, $new_file);
			fclose($htaccessfile);
			
			$step = 3;
			
		} elseif($step == 3) {
			$name = trim($_POST['name']);
			$email = trim($_POST['email']);
			$username = trim($_POST['username']);
			$password = trim($_POST['password']);
			
			$phpass = new PasswordHash(8, true);
			$hashed_pass = $phpass->HashPassword($password);
			
			$new_file = file_get_contents('db.schema');
			$new_file = str_replace('[DBTP]', DBTP , $new_file);
			$new_file = str_replace('[ADMINPASS]', $hashed_pass , $new_file);
			$new_file = str_replace('[ADMINNAME]', $name, $new_file);
			$new_file = str_replace('[ADMINEMAIL]', $email, $new_file);
			$new_file = str_replace('[ADMINUSERNAME]', $username, $new_file);
			
			$dbfile = fopen("ichat.sql", "w");
			fwrite($dbfile, $new_file);
			fclose($dbfile);
			
			$con = mysqli_connect(DBH,DBU,DBPW);
			mysqli_select_db($con, DBN);
			
			$split = SplitSQL('ichat.sql' , ';' , $con);
			if($split != 'finished') {
				$step_errors[] = $split;
			}
			
			$step = 4;
			
		}		
	//} else {
		//$step_errors[] = "Authentication failed! please try again";
	//}
}

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Google Adsense Ban Checker, Check if your site is banned from AdSense or not">
    <meta name="author" content="Michael Designs">
    <link rel="icon" href="favicon.ico">

    <title>iChat Installer Script | Michael Designs</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/cover.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="assets/js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
  </head>

  <body>
	
	<div class="site-wrapper">
      <div class="site-wrapper-inner">
			<div class="window-wrapper">
				<img src="assets/images/close.png" class="img-responsive pull-left hidden-xs" >
				<img src="assets/images/tab1.png" class="img-responsive pull-left">
				<a href="https://codecanyon.net/user/michaelzj/portfolio" target="_blank"><img src="assets/images/tab2.png" class="img-responsive pull-left hidden-xs"></a>
				<a href="http://www.michael-designs.com" target="_blank"><img src="assets/images/tab3.png" class="img-responsive pull-left hidden-xs"></a>
				<div class="url-wrapper clearfix">
					<div class="buttons hidden-xs">
						<img src="assets/images/back.png" class="back-btn img-responsive pull-left" style="cursor:pointer">
						<img src="assets/images/refresh.png" class="refresh-btn img-responsive pull-left" style="cursor:pointer">
						<img src="assets/images/forward.png" class="forward-btn img-responsive pull-left" style="cursor:pointer">
					</div>
					<div class="url">
						<img src="assets/images/lock.png" style="width:20px;margin-right:4px" class="hidden-xs">
							<?php echo "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
							<img src="assets/images/star.png" class="pull-right hidden-xs"  style="width:28px;margin-top:-5px" >
					</div>
				</div>
				
				<div class="page-wrapper" style="background-color:white">
					
	
	<div class="col-md-3 hidden-sm hidden-xs">
	<i class="glyphicon glyphicon-tasks"></i>&nbsp;&nbsp;Server Requirements
	<hr>
	<ul class="feed-ul">
		<?php if($php_version && $php_version >= 5.5 ) { ?>
		<li style='color:green'><i class='glyphicon glyphicon-ok'></i> PHP Version: <?php echo phpversion(); ?></li>
		<?php } else { ?>
		<li style='color:red' ><i class='glyphicon glyphicon-remove'></i> PHP Version: <?php echo phpversion(); ?></li>
		<?php
		$errors[] = "<b>PHP Version</b> is not compatible with this script, please install PHP > 5.4 first!"; } ?>
		
		
		<?php if(!$safe_mode) { ?>
		<li style='color:green'><i class='glyphicon glyphicon-ok'></i> Safe Mode: Disabled</li>
		<?php } else { ?>
		<li style='color:red' ><i class='glyphicon glyphicon-remove'></i> Safe Mode: Enabled</li>
		<?php
		$errors[] = "<b>Safe Mode</b> is enabled, please disabled safe mode first!"; } ?>
		
		<?php /*if($mod_rewrite) { ?>
		<li style='color:green'><i class='glyphicon glyphicon-ok'></i> Mod_Rewrite: Enabled</li>
		<?php } else { ?>
		<li style='color:red' ><i class='glyphicon glyphicon-remove'></i> Mod_Rewrite: Disabled</li>
		<?php
		$errors[] = "<b>mod_rewrite</b> module is disabled, please enable mod_rewrite first!"; }*/ ?>
		
		<?php if($privileges) { ?>
		<li style='color:green'><i class='glyphicon glyphicon-ok'></i> Write Privileges: Granted</li>
		<?php } else { ?>
		<li style='color:red' ><i class='glyphicon glyphicon-remove'></i> Write Privileges: Disabled</li>
		<?php
		$errors[] = "<b>Write Privileges</b> to the config file are disabled, please correct 'assets/includes/config.php' permissions to (775) first!"; } ?>
	</ul>
	</div>
	
	
	<div class="col-md-9">
		<?php
			if (!empty($step_errors)) {
			foreach($step_errors as $error) {
		?>
			<div class="alert alert-danger">
				<i class="glyphicon glyphicon-times"></i> <strong>Error!</strong>&nbsp;&nbsp;<?php echo $error; ?>
			</div>
		<?php 
			}}
		?>
		<div class= "page-header">
			<h3>Welcome to iChat installer script<small class="pull-right">[ Step <?php echo $step; ?> of <?php echo $steps; ?> ]</small></h3>
		</div>
		
		<?php if(!empty($errors)) { ?>
		<div style="color:red; font-size:20px">Unfortunately, Script installation cannot be continued on this server! Errors found:</div>
		<br><ul style="color:black; font-size:18px">
			<?php foreach($errors as $error) {
				echo "<li>{$error}</li>";
			} ?>
		</ul>
		
		<?php
		} else {
		?>
		<form method="post" action="./index.php">
		<?php
		switch($step) {
				case '1' :
				?>
				<div style="color:black; font-size:16px">
				Thanks for purchasing iChat Online Support<br>Now let's break the ice between iChat and your server ;)
				
			<br><br>
			<div class="form-group">
				<label for="db_host">Database Host</label>
				<input type="text" class="form-control" name="db_host" id="db_host" value="<?php echo $db_host; ?>" required>
			</div>
			<div class="form-group">
				<label for="db_name">Database Name</label>
				<input type="text" class="form-control" name="db_name" id="db_name" value="<?php echo $db_name; ?>" required>
			</div>
			<div class="form-group">
				<label for="db_user">Database Username</label>
				<input type="text" class="form-control" name="db_user" id="db_user" value="<?php echo $db_user; ?>" required>
			</div>
			<div class="form-group">
				<label for="db_pass">Database Password</label>
				<input type="text" class="form-control" name="db_pass" id="db_pass" value="<?php echo $db_pass; ?>" required>
			</div>	
			<div class="form-group">
				<label for="db_table_prefix">Tables Prefix</label>
				<input type="text" class="form-control" name="db_table_prefix" id="db_table_prefix" value="<?php echo $db_table_prefix; ?>" >
			</div>
				
				</div>
				<?php
				break;
				case '2' :
			?>
			<div style="color:black; font-size:16px">
				Great! installer now can connect to MySQL Server ^_^<br>
				Now please verify your script Public URL [ <i>starting with http:// & ending with trailing slash /</i> ]
				
			<br><br>
			<div class="form-group">
				<label for="url">Script URL</label>
				<input type="text" class="form-control" name="url" id="url" value="<?php echo $url; ?>" required>
			</div>
			<br>
			<div class="form-group clearfix">
				<label for="url">Google CAPTCHA Keys (Required)</label><br>
				<input type="text" class="form-control pull-left" style="width:40%" name="sitekey" id="sitekey" value="" required placeholder="Site Key">
				<input type="text" class="form-control pull-left" style="width:40%" name="secret" id="secret" value="" required placeholder="Secret Key">
			</div>
			
			<div class="form-group clearfix">
				<label for="url">Google Maps API Key (Required)</label><br>
				<input type="text" class="form-control " name="gmaps_key" id="gmaps_key" value="" required placeholder="Google Maps API Key">
			</div>
			</div>
			
			
			<?php
				break;
				case '3' :
			?>
			<div style="color:black; font-size:16px">
				Great! Now tell us more about you!<br>
				
			<br><br>
			<div class="form-group clearfix">
				<label for="f_name">Admin Name</label><br>
				<input type="text" class="form-control" style="width:80%" name="name" id="name" value="" required placeholder="Name..">
			</div>
			<div class="form-group clearfix">
				<label for="Username">Admin Username</label><br>
				<input type="text" class="form-control" style="width:80%" name="username" id="username" value="admin" required>
			</div>
			<div class="form-group clearfix">
				<label for="email">Admin Email</label><br>
				<input type="email" class="form-control" style="width:80%" name="email" id="email" value="" required>
			</div>
			<div class="form-group clearfix">
				<label for="password">Admin Password</label><br>
				<input type="text" class="form-control" style="width:80%" name="password" id="password" value="" required>
			</div>
			
			</div>
			
			
			<?php
				break;
				case '4' :
			?>
			<div style="color:black; font-size:16px">
				<h3 style="color:green">Congratulations! You've installed (iChat) !</h3><br>
				<h4>You can now <a href="<?php echo WEB_LINK; ?>" style="color:green" >view</a> your script and <a href="<?php echo WEB_LINK; ?>login.php" style="color:green">login</a> using credentials you entered minutes ago.</h4>
				<h4>Don't forget to <b>delete (install) folder!</b></h4>
				<h4>Feel free to <a href="mailto:michael.zohney@gmail.com" style="color:green">Contact Me</a> anytime at michael.zohney@gmail.com ;)</h4>
				<br><Br><Br>
			</div>
			
			
			<?php
				break;
			}
			
			if($step != $steps) {
			?>
			
			
			<div class="modal-footer">
				<br/>
				<center>
						<input class="btn btn-success" type="submit" name="submit" value="Submit">
				</center>
				<?php 
					echo "<input type=\"hidden\" name=\"step\" value=\"".$step."\" readonly/>";
					echo "<input type=\"hidden\" name=\"hash\" value=\"".$random_hash."\" readonly/>";
				?>
			</div>
			
			<?php } ?>
		</form>
		<?php
		}
		?>
	</div>
	

					
					
					
					
					
					
				</div>
			</div>
      </div>
    </div>
	
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="assets//js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
	</script>
	
	<script type="text/javascript">
		var height= $(window).outerHeight();
		$('.page-wrapper').css("height", height - 175);
	</script>
	
  </body>
</html>